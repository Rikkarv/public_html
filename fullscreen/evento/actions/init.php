<?php
$team = [
    ["name" => "Ana Gonçalves", "initials" => "AG", "status" => "undefined"],
    ["name" => "Anabela Couteiro", "initials" => "AC", "status" => "undefined"],
    ["name" => "André Miranda", "initials" => "AM", "status" => "undefined"],
    ["name" => "André Neves", "initials" => "AN", "status" => "undefined"],
    ["name" => "Aníbal Cascais", "initials" => "AC", "status" => "undefined"],
    ["name" => "Bruna Tavares", "initials" => "BT", "status" => "undefined"],
    ["name" => "Bruno Maia", "initials" => "BM", "status" => "undefined"],
    ["name" => "Bruno Santos", "initials" => "BS", "status" => "undefined"],
    ["name" => "David Besteiro", "initials" => "DB", "status" => "undefined"],
    ["name" => "Elba Magalhães", "initials" => "EM", "status" => "undefined"],
    ["name" => "Hugo Silva", "initials" => "HS", "status" => "undefined"],
    ["name" => "João Ferreira", "initials" => "JF", "status" => "undefined"],
    ["name" => "João Mota", "initials" => "JM", "status" => "undefined"],
    ["name" => "Jorge Vasconcelos", "initials" => "JV", "status" => "undefined"],
    ["name" => "José Cardoso", "initials" => "JC", "status" => "undefined"],
    ["name" => "Katia Correia", "initials" => "KC", "status" => "undefined"],
    ["name" => "Luana Bomfim", "initials" => "LB", "status" => "undefined"],
    ["name" => "Luís Ramalho", "initials" => "LR", "status" => "undefined"],
    ["name" => "Nuno Silva", "initials" => "NS", "status" => "undefined"],
    ["name" => "Pedro Mendes", "initials" => "PM", "status" => "undefined"],
    ["name" => "Ricardo Carvalho", "initials" => "RC", "status" => "undefined"],
    ["name" => "Ricardo Godinho", "initials" => "RG", "status" => "undefined"],
    ["name" => "Tiago Couto", "initials" => "TC", "status" => "undefined"],
    ["name" => "Tiago Ferreira", "initials" => "TF", "status" => "undefined"],
    ["name" => "Vítor Reis", "initials" => "VR", "status" => "undefined"],
    ["name" => "Victor Benzi", "initials" => "VB", "status" => "undefined"],
    ["name" => "Viviana Silva", "initials" => "VS", "status" => "undefined"],
];
file_put_contents("../files/team.json", json_encode($team));
file_put_contents("../files/timestamp.txt", time());
?>