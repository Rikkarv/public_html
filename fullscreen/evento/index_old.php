<?php
    $json = file_get_contents("files/team.json");
    $team = json_decode($json, true)
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FULL EVENTO</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="icon" href="images/favicon.png" type="image/png">
</head>

<body>
    <div id="fullscreen">
        <div id="overlay">
            <div id="actions" data-index="">
                <div class="action" data-status="true">
                    <img src="images/true.png">
                </div>
                <div class="action" data-status="undefined">
                    <img src="images/undefined.png">
                </div>
                <div class="action" data-status="false">
                    <img src="images/false.png">
                </div>
            </div>
        </div>
        <div id="container">
            <!-- start loop -->
            <?php 
                $squareRoot = floor(sqrt(count($team)));
                foreach ($team as $index => $persona) { 
                    if ($index % $squareRoot == 0) {
                        echo '<div class="column">';
                    }
            ?>
        
            <div class="item" 
                title="<?php echo $persona['name']; ?>" 
                data-index="<?php echo $index; ?>" 
                data-status="<?php echo $persona['status']; ?>"
                style="background: rgba(<?php echo rand(0, 255); ?>, <?php echo rand(0, 255); ?>, <?php echo rand(0, 255); ?>, 0.5);"
            >
                <div class="button"><?php echo $persona['initials']; ?></div>
            </div>
        
            <?php
                    if ($index % $squareRoot == $squareRoot - 1) {
                        echo '</div>';
                    }
                }
            ?>
            <!-- end loop -->
        </div>
    </div>

    <script>
        let timestamp = Math.round(Date.now() / 1000);
        let overlay = document.getElementById("overlay");
        let actions = document.getElementById("actions");

        overlay.addEventListener("click", function() {
            overlay.style.display = "none";
        });
        document.querySelectorAll(".button").forEach(button => {
            button.addEventListener("click", function() {
                overlay.style.display = "flex";
                actions.dataset.index = this.parentNode.dataset.index;
            });
        });
        document.querySelectorAll(".action").forEach(action => {
            action.addEventListener("click", function() {
                let data = {
                    "index": actions.dataset.index,
                    "status": this.dataset.status,
                    "timestamp": timestamp
                }
                fetch("actions/set.php", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify(data)
                }).then(function (response) {
                    return response.json();
                }).then(function (json) {
                    alert(json.message);
                    location.reload();
                });
            });
        });
    </script>
</body>

</html>