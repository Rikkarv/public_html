<?php
    $json = file_get_contents("files/team.json");
    $team = json_decode($json, true)
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Kart</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="icon" href="images/favicon.png" type="image/png">
</head>

<body>
    <div id="container">
        <div id="team">

            <?php foreach ($team as $index => $player) { ?>

            <div class="row" data-index="<?php echo $index ?>">
                <span><?php echo $player["name"] ?></span>
                <div class="status-container">
                    <div onclick="changeStatus(this)" data-value="false" class="status <?php if ($player["status"] == "false") {echo 'active';} ?>">Não</div>
                    <div onclick="changeStatus(this)" data-value="undefined" class="status <?php if ($player["status"] == "undefined") {echo 'active';} ?>">?</div>
                    <div onclick="changeStatus(this)" data-value="true" class="status <?php if ($player["status"] == "true") {echo 'active';} ?>">Sim</div>
                </div>
            </div>

            <?php } ?>

        </div>
        <hr>
        <div class="logo">
            <object data="images/fullscreen.svg" type="image/svg+xml"></object>
        </div>
    </div>

    <script>
        let timestamp = Math.round(Date.now() / 1000);

        function changeStatus(trigger) {
            let row = trigger.closest(".row");
            let status;
            let data = {
                "index": row.dataset.index,
                "status": trigger.dataset.value,
                "timestamp": timestamp
            }
            fetch("actions/set.php", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(data)
            }).then(function (response) {
                return response.json();
            }).then(function (json) {
                if (json.result === "ok") {
                    timestamp = json.timestamp;
                    trigger.closest(".row").dataset.status = json.status;
                    row.querySelectorAll(".status").forEach(item => {
                        item.classList.toggle("active", item === trigger);
                    });
                } else {
                    alert(json.message);
                }
                //
                console.log(json);
            });
        }
    </script>
</body>

</html>