<?php

$timestamp = file_get_contents("../files/timestamp.txt");

$content = trim(file_get_contents("php://input"));
$decoded = json_decode($content, true);

if (!in_array($decoded["status"], ["true", "false", "undefined"])) {
    echo json_encode([
        "result" => "ko",
        "message" => "Este valor não é permitido. <br><br>Estás a tentar corromper o sistema.",
        "extra" => [
            "status_posted" => $decoded["status"]
        ]
    ]);
} elseif ((int) $timestamp > (int) $decoded["timestamp"]) {
    echo json_encode([
        "result" => "ko",
        "message" => "Lamentamos mas entretanto alguém alterou o seu estado. <br><br>Recarrega a página por favor.",
        "extra" => [
            "timestamp_posted" => $decoded["timestamp"],
            "timestamp_file" => $timestamp
        ]
    ]);
} else {
    $newTimestamp = time();
    $myArray = json_decode(file_get_contents("../files/team.json"), true);
    $index = (int) $decoded["index"];
    $myArray[$index]["status"] = $decoded["status"];
    file_put_contents("../files/team.json", json_encode($myArray));
    file_put_contents("../files/timestamp.txt", $newTimestamp);
    $message = "Gravado.";
    switch ($decoded["status"]) {
        case "false":
            $message .= "<br><br>És um falhado.<br><br>";
            $message .= "<div class='icon'></div>";
            break; 
        case "true":
            $message .= "<br><br>És um Campeão.<br><br>";
            $message .= "<div class='icon'></div>";
            break; 
        default:
            $message .= "<br><br>És um indeciso...<br><br>";
            $message .= "<div class='icon'></div>";
    }
    echo json_encode([
        "result" => "ok",
        "status" => $decoded["status"],
        "message" => $message,
        "timestamp" => $newTimestamp
    ]);
}

?>