<?php
$team = [
    ["name" => "Anabela Couteiro", "status" => "undefined"],
    ["name" => "Aníbal Cascais", "status" => "undefined"],
    ["name" => "Bruno Maia", "status" => "undefined"],
    ["name" => "David Besteiro", "status" => "undefined"],
    ["name" => "Elba Magalhães", "status" => "undefined"],
    ["name" => "Hélder Moura", "status" => "undefined"],
    ["name" => "Hugo Silva", "status" => "undefined"],
    ["name" => "João Ferreira", "status" => "undefined"],
    ["name" => "João Marcos", "status" => "undefined"],
    ["name" => "João Mota", "status" => "undefined"],
    ["name" => "João Quesado", "status" => "undefined"],
    ["name" => "João Santos", "status" => "undefined"],
    ["name" => "João Teixeira", "status" => "undefined"],
    ["name" => "Jorge Castro", "status" => "undefined"],
    ["name" => "Jorge Vasconcelos", "status" => "undefined"],
    ["name" => "José Cardoso", "status" => "undefined"],
    ["name" => "Luís Ramalho", "status" => "undefined"],
    ["name" => "Nuno Silva", "status" => "undefined"],
    ["name" => "Pedro Mendes", "status" => "undefined"],
    ["name" => "Rafael Sá", "status" => "undefined"],
    ["name" => "Ricardo Carvalho", "status" => "undefined"],
    ["name" => "Ricardo Godinho", "status" => "undefined"],
    ["name" => "Tiago Couto", "status" => "undefined"],
    ["name" => "Tiago Ferreira", "status" => "undefined"],
    ["name" => "Victor Benzi", "status" => "undefined"],
    ["name" => "Vítor Reis", "status" => "undefined"],
];
file_put_contents("../files/team.json", json_encode($team));
file_put_contents("../files/timestamp.txt", time());
?>