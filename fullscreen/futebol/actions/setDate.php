<?php

$content = trim(file_get_contents("php://input"));
$decoded = json_decode($content, true);

file_put_contents("../files/date.txt", $decoded["date"]);

echo json_encode([
    "date" => $decoded["date"]
]);

?>