<?php
    $json = file_get_contents("files/team.json");
    $team = json_decode($json, true)
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FULLBOL</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="icon" href="images/favicon.png" type="image/png">
</head>

<body>
    <div id="container">
        <div id="title-container">
            <span id="title"><?php echo file_get_contents("files/date.txt") ?></span>
            <div id="counters">
                <div id="counter-true" class="counter">
                <?php
                    echo count(array_filter($team, function($player) {
                        return $player["status"] === "true";
                    }));
                ?>
                </div>
                <div id="counter-false" class="counter">
                <?php
                    echo count(array_filter($team, function($player) {
                        return $player["status"] === "false";
                    }));
                ?>
                </div>
            </div>
        </div>
        <hr>
        <div id="team">

            <?php foreach ($team as $index => $player) { ?>

            <div class="row" data-index="<?php echo $index ?>" data-status="<?php echo $player["status"] ?>" onclick="this.classList.toggle('clicked')">
                <span><?php echo $player["name"] ?></span>
                <div class="status-container">
                    <div class="status" data-value="false" onclick="pickStatus(this);"></div>
                    <div class="status" data-value="undefined" onclick="pickStatus(this);"></div>
                    <div class="status" data-value="true" onclick="pickStatus(this);"></div>
                </div>
            </div>

            <?php } ?>

        </div>
        <hr>
        <div class="logo">
            <object data="images/fullscreen.svg" type="image/svg+xml"></object>
        </div>
    </div>

    <script>
        let timestamp = Math.round(Date.now() / 1000);

        function init() {
            fetch("actions/init.php");
            location.reload();
        }

        function setDate(string) {
            fetch("actions/setDate.php", {
                method: "POST",
                body: JSON.stringify({
                    "date": string
                })
            }).then(function (response) {
                return response.json();
            }).then(function (json) {
                document.querySelector("#title").innerHTML = json.date;
            });
        }

        function pickStatus(trigger) {
            if (trigger.dataset.value === trigger.closest(".row").dataset.status) {
                return;
            }
            let data = {
                "index": trigger.closest(".row").dataset.index,
                "status": trigger.dataset.value,
                "timestamp": timestamp
            }
            fetch("actions/set.php", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(data)
            }).then(function (response) {
                return response.json();
            }).then(function (json) {
                let alert = document.createElement("div");
                alert.classList.add("alert");
                alert.innerHTML = json.message;
                document.body.appendChild(alert);
                if (json.result === "ok") {
                    timestamp = json.timestamp;
                    trigger.closest(".row").dataset.status = json.status;
                    alert.classList.add(json.status);
                    document.getElementById("counter-false").innerHTML = document.querySelectorAll(".row[data-status='false']").length;
                    document.getElementById("counter-true").innerHTML = document.querySelectorAll(".row[data-status='true']").length;
                };
                //
                console.log(json);
            });
        }

        document.body.addEventListener("click", function() {
            let alert = document.querySelector(".alert");
            if (alert) {
                alert.remove();
            }
        });

        let imageBackground = document.createElement("img");
        imageBackground.onload = function() {
            document.body.style.backgroundImage = "url(" + imageBackground.src + ")";
            document.body.classList.add("backgroundImage-loaded");
        };
        if (window.matchMedia("(max-width: 600px)").matches) {
            imageBackground.src = "images/fundo_mobile.png";
        } else {
            imageBackground.src = "images/fundo.jpg";
        }
    </script>
</body>

</html>