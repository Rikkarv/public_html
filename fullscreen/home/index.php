<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FULL HOME</title>
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <div id="fullscreen">
        <div id="container">
            <div id="cube">
                <div id="front" class="cube-face">
                    <img src="images/fullscreen.svg" style="display: block;">
                </div>
                <div id="back" class="cube-face">
                    <div class="text-container">
                        <div class="date">5 de Agosto</div>
                        <div>Feira medieval</div>
                        <div style="color: yellow;">Em construção</div>
                    </div>
                </div>
                <div id="top" class="cube-face"></div>
                <div id="bottom" class="cube-face"></div>
                <div id="left" class="cube-face">
                    <div class="text-container">
                        <div></div>
                        <div class="date">17 de Maio</div>
                        <div>Futebol</div>
                    </div>
                </div>
                <div id="right" class="cube-face">
                    <div class="text-container">
                        <div></div>
                        <div class="date">14 de Maio</div>
                        <div>Evento</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        document.getElementById('cube').addEventListener('mouseenter', function (event) {
            this.classList.add('paused');
        });
        document.getElementById('cube').addEventListener('mouseleave', function (event) {
            this.classList.remove('paused');
        });
        document.getElementById('left').addEventListener('click', function (event) {
            window.location = '../futebol/index.php';
        });
        document.getElementById('right').addEventListener('click', function (event) {
            window.location = '../evento/index.php';
        });
    </script>
</body>

</html>