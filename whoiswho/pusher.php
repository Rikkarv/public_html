<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/../rsc/pusher.php';
require $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';
$pusher = new Pusher\Pusher($app_key, $app_secret, $app_id, array('cluster' => 'eu', 'useTLS' => true));
switch ($_POST['action']) {
    case 'question':
        $data = [
            'type' => 'question',
            'by' => $_POST['player'],
            'question' => $_POST['question']
        ];
        break;
    case 'answer':
        $data = [
            'type' => 'answer',
            'by' => $_POST['player'],
            'question' => $_POST['question'],
            'answer' => $_POST['answer']
        ];
        break;
    case 'endTurn':
        $data = [
            'type' => 'endTurn',
            'by' => $_POST['player'],
            'options' => $_POST['options']
        ];
        break;
    case 'guess':
        $content = file_get_contents('games/' . $_POST['channel'] . '.json');
        $cards = json_decode($content);
        foreach ($cards as $key => $value) {
            if ((string)$key !== (string)$_POST['player']) {
                $result = (int)$value === (int)$_POST['card'];
            }
        }
        $data = [
            'type' => 'guess',
            'by' => $_POST['player'],
            'guess' => $_POST['card'],
            'result' => $result
        ];
        break;
}
$pusher->trigger($_POST['channel'], 'my-event', json_encode($data));