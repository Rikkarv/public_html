<?php
$fileSystemIterator = new FilesystemIterator('games');
$now = time();
foreach($fileSystemIterator as $file) {
    if ($now - $file->getCTime() >= 60 * 60 * 24) {
        unlink('games/' . $file->getFilename());
    }
}
if ($_POST['login-type'] === 'create-channel') {
    $channel = date('His');
    $filename = 'games/' . $channel . '.json';
    $card_id = mt_rand(1, 24);
    file_put_contents($filename, json_encode([$card_id]));
    echo json_encode([
        'logged' => true,
        'channel' => $channel,
        'id' => '0',
        'card' => $card_id
    ]);
} elseif ($_POST['login-type'] === 'join-channel') {
    $channel_file = file_get_contents('games/' . $_POST['channel'] . '.json');
    $cards = json_decode($channel_file);
    if (count($cards) === 1) {
        $cards[] = mt_rand(1, 24);
        file_put_contents('games/' . $_POST['channel'] . '.json', json_encode($cards));
        echo json_encode([
            'logged' => true,
            'channel' => $_POST['channel'],
            'id' => '1',
            'card' => $cards[1]
        ]);
    } else {
        echo json_encode([
            'logged' => false
        ]);
    }
}
?>