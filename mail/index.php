<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require_once $_SERVER['DOCUMENT_ROOT'] . '/../rsc/mail.php';
require $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';

$mail = new PHPMailer(true);

//Enable SMTP debugging.
//$mail->SMTPDebug = 3;                               
//Set PHPMailer to use SMTP.
$mail->isSMTP();            
//Set SMTP host name                          
$mail->Host = "mail.w3rsc.pt";
//Set this to true if SMTP host requires authentication to send email
$mail->SMTPAuth = true;                          
//Provide username and password     
$mail->Username = "rsc@w3rsc.pt";                 
$mail->Password = $mail_secret;                           
//If SMTP requires TLS encryption then set it
//$mail->SMTPSecure = "tls";                           
$mail->SMTPSecure = "ssl";                           
//Set TCP port to connect to
$mail->Port = 465;                                   

$mail->From = "rsc@w3rsc.pt";
$mail->FromName = "Ricardo Carvalho";

$mail->addAddress("rikardokarvalho@hotmail.com");

$mail->isHTML(true);

$mail->Subject = "HTML email";
$mail->Body = file_get_contents("template.html");
$mail->AltBody = "This is the plain text version of the email content";

try {
    $mail->send();
    echo "Message has been sent successfully";
} catch (Exception $e) {
    echo "Mailer Error: " . $mail->ErrorInfo;
}
?>