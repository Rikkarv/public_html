<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>RSC - Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
    <style>
        html, body {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
            width: 100vw;
            height: 100vh;
            overflow: hidden;
            background:
                linear-gradient(steelblue, transparent),
                linear-gradient(to top left, seagreen, transparent),
                linear-gradient(to top right, firebrick, transparent);
            background-blend-mode: screen;
            font-family: Raleway;
            color: white;
        }
        .flexCenter {
            display: flex;
            justify-content: center;
            align-items: center;
        }
        #screen {
            width: 100%;
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
        }
        #loginContainer {
            width: 300px;
            height: 300px;
            box-sizing: border-box;
            background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAEsCAYAAAB5fY51AAAZPUlEQVR4Xu2de9B/ezXH14o5JF2VW0IjKvqDCYVMbmVQmiJ1OqkO1QmnVFTISRfddL8dusc5Jyp0lSnjzoQKY5LLIHKbhKioRMu8n1nfZ/Z3P/t7/T3Pfn7rWa/9z7k83733Wq/3d7+/n8v6fLYbBwQgAIEiBLxInIQJAQhAwDAsvgQQgEAZAhhWGakIFAIQwLD4DkAAAmUIYFhlpCJQCEAAw+I7AAEIlCGAYZWRikAhAAEMi+8ABCBQhgCGdR5JFRFfaGbfaWZfY2bXMrP/NrMLMsTI/9Z//pOZXeXurz6PwicUCJw4AQzrxBFvvkFE3NrMfjRN6mfN7Ep3/8CqM9PYHmxmX2BmV7j7izffhU9AoD4BDOuUNYyIx2aL6vHu/qZdwomIzzKzJ5mZzO3h60xul+vyWQicrwQwrFNUJiIeY2a3MLP7nIvZRMSLlIa73/cU0+HWEDhxAhjWiSOevkFEfJeZ3cXMLjwXs9LVI+KaZvYKM3u1u7/wlFLithA4cQIY1okjXmlYrzKzNx+XwWTX8mbu/h2nlBK3hcCJE8CwThzx0RtExB3N7PvM7K7n2rpaXD2v+VAzu8Td//IU0uKWEDhxAhjWiSOeNCyVLnyzu1+46vY5oP6gHJC/gZn9rQbYVw3MZ7dQM4xPd/dfO4W0uCUETpwAhnXiiCcN6+Fmdmt31xjWkSMi7mlml+aY1JNznEr/7xIz+7FVhhQRrzOzZ2JYpyAqt5yFAIa1AXO2XG7s7n9yXIrkeNMXu/u3jq8ZEd9kZt9tZk91998b/j0inmNmH3V3df2mjG42w8pasNuY2ZdncevVzexDg6D+x8zeZmZvd/e3bssuIr7OzL7ezG6U51zXzD7bzD5mZu8ysw+a2b+b2esw5m2pnp3PtTGsiPgRM/s2M/vTCfk+w8z+yN3V8lk6IuKH8wFSdbkOffZTzezjzew57v5TE+c8Lu/10dHfPk6GY2Y/nWNYesiHx/+a2d+b2Uvd/SUT11XN1rXcXUWjaw0rItRCu8zMhvdQ5fx78v9daWa6xz3M7HZpENfOi/5L5vfn7i5mB8egm3qHzOMdaUq65h+7+zvT4G9vZp9kZrc0sy81s+ub2W+b2WPc/R8m8lLh7A+ame4vff5gcb2Jz35ZXve2Zva52Qr9ibPzSJLJOgKdDEsD3RoTust4oDtLDNTi0d/HhvU8M3ufu6sS/fCIiOea2YfdXQ/a+JyfN7MXu/svj86RidzK3TWGtfOxSwsrW3GT5hYRzzKzb0zzUoxqrbxzFOvSOFtEyCTVhf0lM7t8l8mCNLHHm9lXmNmjhlwi4gFmdrd143OrQOV1ZeKqZVPh7W/sDJUTShFoY1jZQvgVM3uBu6ukYGg+MrOLp8aUIkID2W909ytG5+hBuaG7q/s2vtbd3f2iCSP7GTN7h7vv3CKICBnmXc3sIduMYUXEj5vZNd39+yfiUN3WtadaO4vPZgvttu5+14i4X7bCHj02tl2+7RGhHwS1zi7VTGYuSVJrVBMFS+a+43Vlet+uH6RziW+Xe/LZ0yHQzbC05u4f3f1RI5PRAzw5wxYRai09y93VpRkak1ogKktYGoeKiB8ys0+eaJGpKyOTUwtj5ZhOROi69zezzzSzTzCzj5jZe3P8RnEsjWuNYjocwzrX6vdsod3QzDTor1amWlXnvNg6In7SzP7V3S+LiEeb2XWnTHXXxyEi1L19t7vrmhxnlEA3w5psFWXr65Vm9vphSyprmx7g7t8y0UrR4PAjzOyBw7qniHiKmf2du6vLODQ4dQdv6e4Xr/surevKbfoODmcJj8mwtGPEX5nZQUtr1f0jQhzuLaPOnSRe5e5Pm/p8ROg6Kum4OCJemoPyS6w25bniuuJ7c3dXfRvHGSXQzbDU9VtlQJqBe6+7y9QOjojQIPwt3P1eKx4SLVZ+rru/fnCOHsKXu7u6n0PD0riRxsLWtgDOQ8PS5MLHpsb3kpEmMz5Hg+Ya18qB+adrJs/d1VVbOnLc6QUqzzAzdQdfNGa1z7OWPy53cHeVfnCcUQLdDEtdv180s2cPTSYfPI0RXd3dHzkwH5nMR6ZmD/McjYX95qI1FRHq9j1sanlMROi+mv07NLcVJqiH/b/UZdr1OzdqYT3fzK7m7hp/2vlYGGee+P5xN3rA6MgYX3JY2f1dxGlmGshfWTeWLTfNYqq0QbO0L1y1lU6WQ2hsbLK2bWcAnHBeEmhlWGkykwPfOXakLtthyUCOX2nA/UiJQV5LhvaBxXjVsLszal2tHNSfaIE808xWGsS6b9HIsDaVQGiW8IFmduMsJ3hf1je928w0AH6z3J9L35HDHCfi1aLrd7m7xu4Oj4hQ6+lr89oqV5DhfNjM/i9LId6Ys7aTO1VExLPN7PPN7H6aHMgBerVO3zpl5mlYD56qbTsvnzyC2otAR8PSDN01xmMd+YW/aDHrN9gB4Rmruizj7lt2Ia8/bpFFhFpLn7Kqfmr0oJ+4YWWu2trmZeMWS7aO7p5mI/NSweyR2dBBC0u7TqjrrLouTQqs3HhwlOfKSYg0p2eY2RNG3W2NGz7RzDRbOS4Z0d8wrL1soM5JHQ3rPlmLNZ7dU3fxYGwlp9w3toqyVXb7RV1VlhJ80N21qd6wtaHu2V9vU84QET+gEosszNQawv80sz/L7tDaDf62bWHlPlwal5JprT0GLRuZxOQMZUSotaZ6NNVDqeh0UVD6OxtmRCcr89P4NTCvraKXjpxM+JC7q3U4ZIxhbRLzDPy9o2Gt+2XXRniv1a96RKj04UbrxoCypaIuy8Ei5ohQBfub3P3lo4dJ3aa9B5cjQg+uluO8bTgpMPEwD8saVnYJI0I1ZdraZqm2bNX3Obt3MqN7bWpBJZNbmZk4q0t3PTP73ay1Gi81WmVYB8WgK+ritO+9WnTqssrQVfqhQ6sE3rJuQfkZeF7bp3BqhhUReqnCsR3uvnUuEfEaM3vlhLGoG/LPagllJbtmupZqtiZMQuUQT1FLIotMVQR5WGcVEVr2cl93VzX33keu3XtCThhM7sawQwtrshh2XXAR8VQzu04Wrm7V7UsT1zbOGt9SwagmHQ5bdasWa5/LTOm2gI/7+7ftfc/S53Z55o4r760f8uO64eI6+sKcRsL5EK1abqOBYo1vPTQiNAMoU1uqip8wLJUxaAD5P6aMKbs3n3cc0+3Z0tFWyJMziCdpWMlN41UyHrXOjqyh3GB4WtStrvIbFjOxawxLLagjY4HH/R3kevsTOK3nt6thTXb3IkJT6NqxUzNUWtJyz0UXKAfh9ZBqjeBhCycLRd+vGqupwsWIOFKmkDsyqLBShZnjQ4ujnzYuPE3D2DTzt22XcOcW1uCHRmN9qsRXy1F5v8XM1o5VDc7VYL7G6B4hhhjW/oZx2mdiWDMqkOUH93Z3tRYOj0H9kB7CT3f37x08bBqEl/loJuywMjvX3N08t1ZRd3KpwnuqpZZFjqoZ0mD10hEReqh1XxW4jhckX5Vd1qkF1xozUstLBZxap7duDGtvwxrxknlpFcBXm9lNzewTtctCLuNZin3A8bXakcLdL11jWA/R591dXfSVR65xvFO+7kw7Rmj3DHXROU6YAIZ1woBHD5re56fygSdOrBHUQLS2kHnNyJjUTdHyE+1soC1nDo40ORmFBn+fPGp9LZnI4Jy1M1oR8T3Z0tMSl8vzPlo4rBlOGdKRMaxxHdIchjUlWUTIbNVS1e4JRxY0Zxf5YPPCNYalpT4XuLu66GNDF2sVw2qrHJni87ULa87Y3sPd1fXkOGECGNYJA5744mtR85Gi0Fy8rAHyu43WCKp+SzNlah0sLf+IiF/Q/lHurl/7wyO7mHcaD7inuRxZhzg6V4PVD8t7anmMyhuet2aLZC2avuOiyj5nOa8ztdnfqh0oJhjJ2DXY/kWD2bjFxzQr9zc54TCuiVKBpxaAT7UEFaf2nb9NxqHq9SUDzti1aeLSusuIOHhxrPYTWxj54Edg7S6uM3+9zvztMKyZJV63n9WKloN2etCOmtpd4MjWMSvOUZGjjEzGMzQytbDUerjsuHbNHJdhZIvjJu6uNXvjVorKLFR+MVnBPzCBTS1B7XGlmihtZTPMb1vD0ivJNIA/3u5HpQtqWWps7HAfsojQzhHqfqvAd2mmMluU11NXc+avUsvbYVgzy75NndXoIdS4j1oCGst63Dbb/q7ajSBbCjILjYetnYXcFsu4DGODYW1VTZ9xqoU1mW+WWmis7LClmkWkKmNQV+3nJsxShrLYZ0tm9J6pV53lImqZ7ZfkDq9q0f1+xnJkfGyOUohttejwOQxrZpWzu6aB9yMD3ytaSwc7M6h4cstyBw1IL5arTI056Xq/uk31+yY0OYMpc9DOEQfds6m1kYOW01aGldeRUas19rIVXNR11ayrilu1LbK2eFYt2qQRZz2XNha8JHcbVXHu4YLzTbmu+nsatlqzR3aI2PeanLeaAIY187dj044Co9aVZsH0a6/Bdi0J2bhraM4ETu5imkag0gmNMU1uXbMLjtwI76bDKu80rMlXiUXELoal1pLqyPba9WGihfWGbJFdni04cdVe7+f0LsVtC3134cpnMazhOMepFY4OWhp6eDSAu6k4VJvO3d/db5d1VZpyn3xzzeDaanXcYLzmbfB3vXhBA/laXL33Tp5Z06UB/MeOZihXvvtwR8NSS1E7XGisSbuF7n3kFsl6QcWFg/o2jfNpb/yN6xrX3Tgi3pz7kE22BPcOmhMnCdDCOoUvRkSorukv1q3Py9aQzEctmIs2beo3MKSNC56zS7QoAVi7sHlFd0yzkuoCXeXu6n4OfxCOxbAyf3X3xEClHmoZ7nxEhGJV0ahKSQ5nFXMcTCsPXrFr9fyAtUo+tJj9zpvWOu4cOCdgWPkQnA8tLD182uRu7cxSRMh8tJHfg1YVnY5V3WHDPr3EQq01zXpp8fSVmx667M6q3kmFmpP7vB9Xl3BgCpq509Kaq+U7E7d6Q83gzTY6X2Z15Lx8a5G62zKyR27Kf2TMesGsTHuy7gu/ORkCtLBOhuvaq2ZrSTVVejnnuuMauTfTS9IsNOV+wZoTtEmd9nW/87ZpZdW23vyi7Ya1ZEpLffTyCf1Tx6dpnWP+u/ZZ115WK00jK+ZVx6SN8/Q+xMWhfde1ad8V7q7K/Z2O3DlCzPSiU21781tm9uvDN/AkI71k9atyAz/NIm6qWlcrTt3Dm5iZxrO0gPztZqZ3Iy69tCMiVF3/lZptNLN/SyNc+XKOnRLkw1sRwLC2wnS8H8qpc3XJVBCqY/EwqxUxfNOwlpuowHHrXQqON9Lz82ppTN+QtVEKcvH2Zxn2H+ZLTo+8OHVdNtlF1HY9Mu7FW58Xp2jtot7+rLdKa/mUZi93uv75SbJeVBhWPc2IGAJtCWBYbaUncQjUI4Bh1dOMiCHQlgCG1VZ6EodAPQIYVj3NiBgCbQlgWG2lJ3EI1COAYdXTjIgh0JYAhtVWehKHQD0CGFY9zYgYAm0JYFhtpSdxCNQjgGHV04yIIdCWAIbVVnoSh0A9AhhWPc2IGAJtCWBYbaUncQjUI4Bh1dOMiCHQlgCG1VZ6EodAPQIYVj3NiBgCbQlgWG2lJ3EI1COAYdXTjIgh0JYAhtVWehKHQD0CGFY9zYgYAm0JYFhtpSdxCNQjgGHV04yIIdCWAIbVVnoSh0A9AhhWPc2IGAJtCWBYbaUncQjUI4Bh1dOMiCHQlgCG1VZ6EodAPQIYVj3NiBgCbQlgWG2lJ3EI1COAYdXTjIgh0JYAhtVWehKHQD0CGFY9zYgYAm0JYFhtpSdxCNQjgGHV04yIIdCWAIbVVnoSh0A9AhhWPc2IGAJtCWBYbaUncQjUI4Bh1dOMiCHQlgCG1VZ6EodAPQIYVj3NiBgCbQlgWG2lJ3EI1COAYdXTjIgh0JYAhtVWehKHQD0CGFY9zYgYAm0JYFhtpSdxCNQjgGHV04yIIdCWAIbVVnoSh0A9AhhWPc2IGAJtCWBYbaUncQjUI4Bh1dOMiCHQlgCG1VZ6EodAPQIYVj3NiBgCbQlgWG2lJ3EI1COAYdXTjIgh0JYAhtVWehKHQD0CGFY9zYgYAm0JYFhtpSdxCNQjgGHV04yIIdCWAIbVVnoSh0A9AhhWPc2IGAJtCWBYbaUncQjUI4Bh1dOMiCHQlgCG1VZ6EodAPQIYVj3NiBgCbQlgWG2lJ3EI1COAYdXTjIgh0JYAhtVWehKHQD0CGFY9zYgYAm0JYFhtpSdxCNQjgGHV04yIIdCWAIbVVnoSh0A9AhhWPc2IGAJtCWBYbaUncQjUI4Bh1dOMiCHQlgCG1VZ6EodAPQIYVj3NiBgCbQlgWG2lJ3EI1COAYdXTjIgh0JYAhtVWehKHQD0CGFY9zYgYAm0JYFhtpSdxCNQjgGHV04yIIdCWAIbVVnoSh0A9AhhWPc2IGAJtCWBYbaUncQjUI4Bh1dOMiCHQlgCG1VZ6EodAPQIYVj3NiBgCbQlgWG2lJ3EI1COAYdXTjIgh0JYAhtVWehKHQD0CGFY9zYgYAm0JYFhtpSdxCNQjgGHV04yIIdCWAIbVVnoSh0A9AhhWPc2IGAJtCWBYbaUncQjUI4Bh1dOMiCHQlgCG1VZ6EodAPQIYVj3NiBgCbQlgWG2lJ3EI1COAYdXTjIgh0JYAhtVWehKHQD0CGFY9zYgYAm0JYFhtpSdxCNQjgGHV04yIIdCWAIbVVnoSh0A9AhhWPc2IGAJtCWBYbaUncQjUI4Bh1dOMiCHQlgCG1VZ6EodAPQIYVj3NiBgCbQlgWG2lJ3EI1COAYdXTjIgh0JYAhtVWehKHQD0CGFY9zYgYAm0JYFhtpSdxCNQjgGHV04yIIdCWAIbVVnoSh0A9AhhWPc2IGAJtCWBYbaUncQjUI4Bh1dOMiCHQlgCG1VZ6EodAPQIYVj3NiBgCbQlgWG2lJ3EI1COAYdXTjIgh0JYAhtVWehKHQD0CGFY9zYgYAm0JYFhtpSdxCNQjgGHV04yIIdCWAIbVVnoSh0A9AhhWPc2IGAJtCWBYbaUncQjUI4Bh1dOMiCHQlgCG1VZ6EodAPQIYVj3NiBgCbQlgWG2lJ3EI1COAYdXTjIgh0JYAhtVWehKHQD0CGFY9zYgYAm0JYFhtpSdxCNQjgGHV04yIIdCWAIbVVnoSh0A9AhhWPc2IGAJtCWBYbaUncQjUI4Bh1dOMiCHQlgCG1VZ6EodAPQIYVj3NiBgCbQlgWG2lJ3EI1COAYdXTjIgh0JYAhtVWehKHQD0CGFY9zYgYAm0JYFhtpSdxCNQjgGHV04yIIdCWAIbVVnoSh0A9AhhWPc2IGAJtCWBYbaUncQjUI4Bh1dOMiCHQlgCG1VZ6EodAPQIYVj3NiBgCbQlgWG2lJ3EI1COAYdXTjIgh0JYAhtVWehKHQD0CGFY9zYgYAm0JYFhtpSdxCNQjgGHV04yIIdCWAIbVVnoSh0A9AhhWPc2IGAJtCWBYbaUncQjUI4Bh1dOMiCHQlgCG1VZ6EodAPQIYVj3NiBgCbQlgWG2lJ3EI1COAYdXTjIgh0JYAhtVWehKHQD0CGFY9zYgYAm0JYFhtpSdxCNQjgGHV04yIIdCWAIbVVnoSh0A9AhhWPc2IGAJtCWBYbaUncQjUI4Bh1dOMiCHQlgCG1VZ6EodAPQIYVj3NiBgCbQlgWG2lJ3EI1COAYdXTjIgh0JYAhtVWehKHQD0CGFY9zYgYAm0JYFhtpSdxCNQjgGHV04yIIdCWAIbVVnoSh0A9AhhWPc2IGAJtCWBYbaUncQjUI4Bh1dOMiCHQlgCG1VZ6EodAPQIYVj3NiBgCbQlgWG2lJ3EI1COAYdXTjIgh0JYAhtVWehKHQD0CGFY9zYgYAm0JYFhtpSdxCNQjgGHV04yIIdCWAIbVVnoSh0A9AhhWPc2IGAJtCWBYbaUncQjUI4Bh1dOMiCHQlgCG1VZ6EodAPQIYVj3NiBgCbQlgWG2lJ3EI1COAYdXTjIgh0JYAhtVWehKHQD0CGFY9zYgYAm0JYFhtpSdxCNQjgGHV04yIIdCWAIbVVnoSh0A9AhhWPc2IGAJtCWBYbaUncQjUI4Bh1dOMiCHQlgCG1VZ6EodAPQIYVj3NiBgCbQlgWG2lJ3EI1COAYdXTjIgh0JYAhtVWehKHQD0CGFY9zYgYAm0JtDSstmqTOATOAAF397nTmP2GiwQj4tTuPTdk7geBs0jA3WPuvDCNuYlzPwhAYG8CGNbe6DgRAhCYmwCGNTdx7gcBCOxNAMPaGx0nQgACcxPAsOYmzv0gAIG9CWBYe6PjRAhAYG4CGNbcxLkfBCCwNwEMa290nAgBCMxNAMOamzj3gwAE9iaAYe2NjhMhAIG5CWBYcxPnfhCAwN4EMKy90XEiBCAwNwEMa27i3A8CENibAIa1NzpOhAAE5iaAYc1NnPtBAAJ7E8Cw9kbHiRCAwNwEMKy5iXM/CEBgbwIY1t7oOBECEJibAIY1N3HuBwEI7E0Aw9obHSdCAAJzE8Cw5ibO/SAAgb0JYFh7o+NECEBgbgIY1tzEuR8EILA3AQxrb3ScCAEIzE0Aw5qbOPeDAAT2JoBh7Y2OEyEAgbkJYFhzE+d+EIDA3gQwrL3RcSIEIDA3gf8Hg8/S/6CIkY8AAAAASUVORK5CYII=);
            padding: 60px;
            display: flex;
            flex-direction: column;
            justify-content: space-around;
            align-items: center;
        }
        .loginRow {
            width: 180px;
            height: 30px;
            box-sizing: border-box;
            border: 1px solid white;
            display: flex;
        }
        .svgContainer {
            width: 29px;
            height: 28px;
        }
        .inputContainer {
            width: 149px;
            height: 28px;
            box-sizing: border-box;
            border-left: 1px solid white;
        }
        input {
            width: calc(100% - 5px);
            height: 100%;
            border: none;
            padding: 0;
            user-select: none;
            outline: none;
            background: transparent;
            font-family: Raleway;
            padding-left: 5px;
            color: white;
        }
        input:focus {
            background: white;
            color: gray;
        }
        #buttonSubmit:hover {
            cursor: pointer;
            background: white;
            color: gray;
        }
    </style>
</head>
<body>
<div id="screen">
    <div id="loginContainer">
        <div class="loginRow">
            <div class="svgContainer flexCenter">
                <svg enable-background="new 0 0 63 58" version="1.1" viewBox="0 0 63 58" width="20px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <g>
                        <circle fill="white" cx="31.5" cy="13.5" r="13.5"/>
                        <path fill="white" d="M46.005,31h-29.01C9.182,36.436,3.059,46.132,0.048,58h62.904C59.941,46.132,53.818,36.436,46.005,31z"/>
                    </g>
                </svg>
            </div>
            <div class="inputContainer">
                <input id="formEmail" type="text">
            </div>
        </div>
        <div class="loginRow">
            <div class="svgContainer flexCenter">
                <svg enable-background="new 0 0 44 43" version="1.1" viewBox="0 0 44 43" width="20px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <path fill="white" d="M30.256,21.173c1.835-5.235,1.049-12.14-3.431-16.62C20.771-1.5,10.984-1.527,4.932,4.526  c-6.054,6.053-6.094,15.908-0.041,21.961c4.615,4.615,11.168,5.945,17.026,3.318l2.902,2.902l0.109,0.109l4.084-0.714l-0.018,4.271  l4.244,0.008l-0.532,4.213l2.111,2.111c0.006,0.006,0.006,0.006,0.01,0.01l0.157-0.156c0.741,0.673,0.504,0.366,0.615,0.366  l7.272,0.22c0.6,0,1.126-0.443,1.127-1.044l-0.283-7.333c0-0.112,0.327,0.146-0.346-0.596l-0.058,0.059  c-0.003-0.004-0.003-0.003-0.006-0.007L30.256,21.173z M13.483,13.702c-1.563,1.563-4.095,1.563-5.657,0s-1.563-4.094,0-5.657  c1.563-1.563,4.095-1.563,5.657,0S15.046,12.14,13.483,13.702z"/>
                </svg>
            </div>
            <div class="inputContainer">
                <input id="formPassword" type="password">
            </div>
        </div>
        <div id="buttonSubmit" class="loginRow flexCenter">submit</div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
<script>
    document.getElementById('buttonSubmit').addEventListener('click', function() {
        jQuery.ajax({
            url: 'handleLogin.php',
            method: 'post',
            data: {
                'email': document.getElementById('formEmail').value,
                'password': document.getElementById('formPassword').value
            },
            success: function(response) {
                console.log(response);
            },
            error: function() {
                console.log('error');
            }
        });
    });
</script>
</body>
</html>