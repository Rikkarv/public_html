<?php
session_start();
if (!$_SESSION['isAuthenticated']) {
    header("Location: login.php");
    die();
}