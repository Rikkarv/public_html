<?php
session_start();
require_once '../rsc/db_connection.php';
$sql = 'SELECT password FROM users WHERE email = :email LIMIT 1;';
$query = $conn->prepare($sql);
$query->execute([
    ':email' => $_POST['email']
]);
$user = $query->fetch();
if ($user) {
    if (password_verify($_POST['password'], $user['password'])) {
        $_SESSION['isAuthenticated'] = true;
        $message = 'Authenticated';
    } else {
        $_SESSION['isAuthenticated'] = false;
        $message = 'Password mismatch';
    }
} else {
    $_SESSION['isAuthenticated'] = false;
    $message = 'User not found';
}
echo $message;