<?php
session_start();
$fonts = glob('fonts/*');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Editor</title>
    <link rel="stylesheet" href="libs/jquery.perfect-scrollbar/1.4.0/css/perfect-scrollbar.min.css" />
    <link rel="stylesheet" href="libs/jqueryui/1.12.1/jquery-ui.min.css" />
    <link rel="stylesheet" href="css/spectrum.css"/>
    <link rel="stylesheet" href="css/perfectScroll.css"/>
    <link rel="stylesheet" href="css/editor.css">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <style>
        <?php
        for ($counter = 0; $counter < count($fonts); $counter++) {
            $path = $fonts[$counter];
            $file = str_replace('fonts/', '', $path);
            $fontFamily = substr($file, 0, -4);
            echo '@font-face {' ."\r\n";
            echo 'font-family: "'.$fontFamily.'";' ."\r\n";
            echo 'src: url("'.$path.'");' ."\r\n";
            echo '}' ."\r\n";
        }
        ?>
    </style>
</head>

<body>

<div id="blockOverlay" class="overlay">Please wait</div>
<div id="alertOverlay" class="overlay">
    <div class="overlayCard">
        <div class="overlayCardContent">
            <p id="alertTitle" class="overlayCardRow"></p>
            <p id="alertDescription" class="overlayCardRow"></p>
        </div>
        <div class="overlayCardButtons">
            <div id="button_closeAlert" class="textButton">close</div>
        </div>
    </div>
</div>
<div id="settingsOverlay" class="overlay">
    <div class="overlayCard">
        <div class="overlayCardContent">
            <div class="optionEditableContainer">
                <div class="optionEditableTitle flexCenter">width</div>
                <input id="input_canvasWidth" type="text" class="optionEditableInput" autocomplete="off">
            </div>
            <div class="optionEditableContainer">
                <div class="optionEditableTitle flexCenter">height</div>
                <input id="input_canvasHeight" type="text" class="optionEditableInput" autocomplete="off">
            </div>
        </div>
        <div class="overlayCardButtons">
            <div id="button_cancelSettings" class="textButton">cancel</div>
            <div id="button_applySettings" class="textButton">apply</div>
        </div>
    </div>
</div>
<div id="iconfinderOverlay" class="overlay">
    <div id="iconfinderContainer" class="flexCenter">
        <div id="iconfinderSearchContainer" class="flexCenter">
            <input id="iconfinderSearchQuery" type="text">
            <div id="button_iconfinderSearch" class="svgButton flexCenter">
                <svg width="20" height="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                    <path d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z"></path>
                </svg>
            </div>
        </div>
        <div id="iconfinderList" class="flexCenter"></div>
        <div id="button_closeIconfinder" class="textButton flexCenter">close</div>
    </div>
</div>
<?php
if ($_SESSION['isAuthenticated']) {
    include ('private/drawings_html.php');
}
?>

<input id="hiddenFilePicker" type="file" style="display: none;">
<input id="hiddenFilePicker2" type="file" style="display: none;">

<div id="mainMenu" class="flexCenter">
    <div id="button_undo" title="undo" class="svgButton disabled flexCenter mlr5">
        <svg width="20" height="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
            <path d="M212.333 224.333H12c-6.627 0-12-5.373-12-12V12C0 5.373 5.373 0 12 0h48c6.627 0 12 5.373 12 12v78.112C117.773 39.279 184.26 7.47 258.175 8.007c136.906.994 246.448 111.623 246.157 248.532C504.041 393.258 393.12 504 256.333 504c-64.089 0-122.496-24.313-166.51-64.215-5.099-4.622-5.334-12.554-.467-17.42l33.967-33.967c4.474-4.474 11.662-4.717 16.401-.525C170.76 415.336 211.58 432 256.333 432c97.268 0 176-78.716 176-176 0-97.267-78.716-176-176-176-58.496 0-110.28 28.476-142.274 72.333h98.274c6.627 0 12 5.373 12 12v48c0 6.627-5.373 12-12 12z"></path>
        </svg>
    </div>
    <div id="button_redo" title="redo" class="svgButton disabled flexCenter mlr5">
        <svg width="20" height="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
            <path d="M500.33 0h-47.41a12 12 0 0 0-12 12.57l4 82.76A247.42 247.42 0 0 0 256 8C119.34 8 7.9 119.53 8 256.19 8.1 393.07 119.1 504 256 504a247.1 247.1 0 0 0 166.18-63.91 12 12 0 0 0 .48-17.43l-34-34a12 12 0 0 0-16.38-.55A176 176 0 1 1 402.1 157.8l-101.53-4.87a12 12 0 0 0-12.57 12v47.41a12 12 0 0 0 12 12h200.33a12 12 0 0 0 12-12V12a12 12 0 0 0-12-12z"></path>
        </svg>
    </div>
    <div id="button_grab" title="grab" class="svgButton flexCenter mlr5">
        <svg width="20" height="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
            <path d="M372.57 112.641v-10.825c0-43.612-40.52-76.691-83.039-65.546-25.629-49.5-94.09-47.45-117.982.747C130.269 26.456 89.144 57.945 89.144 102v126.13c-19.953-7.427-43.308-5.068-62.083 8.871-29.355 21.796-35.794 63.333-14.55 93.153L132.48 498.569a32 32 0 0 0 26.062 13.432h222.897c14.904 0 27.835-10.289 31.182-24.813l30.184-130.958A203.637 203.637 0 0 0 448 310.564V179c0-40.62-35.523-71.992-75.43-66.359zm27.427 197.922c0 11.731-1.334 23.469-3.965 34.886L368.707 464h-201.92L51.591 302.303c-14.439-20.27 15.023-42.776 29.394-22.605l27.128 38.079c8.995 12.626 29.031 6.287 29.031-9.283V102c0-25.645 36.571-24.81 36.571.691V256c0 8.837 7.163 16 16 16h6.856c8.837 0 16-7.163 16-16V67c0-25.663 36.571-24.81 36.571.691V256c0 8.837 7.163 16 16 16h6.856c8.837 0 16-7.163 16-16V101.125c0-25.672 36.57-24.81 36.57.691V256c0 8.837 7.163 16 16 16h6.857c8.837 0 16-7.163 16-16v-76.309c0-26.242 36.57-25.64 36.57-.691v131.563z"></path>
        </svg>
    </div>
    <div id="button_zoomIn" title="zoom in" class="svgButton flexCenter mlr5">
        <svg width="20" height="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
            <path d="M304 192v32c0 6.6-5.4 12-12 12h-56v56c0 6.6-5.4 12-12 12h-32c-6.6 0-12-5.4-12-12v-56h-56c-6.6 0-12-5.4-12-12v-32c0-6.6 5.4-12 12-12h56v-56c0-6.6 5.4-12 12-12h32c6.6 0 12 5.4 12 12v56h56c6.6 0 12 5.4 12 12zm201 284.7L476.7 505c-9.4 9.4-24.6 9.4-33.9 0L343 405.3c-4.5-4.5-7-10.6-7-17V372c-35.3 27.6-79.7 44-128 44C93.1 416 0 322.9 0 208S93.1 0 208 0s208 93.1 208 208c0 48.3-16.4 92.7-44 128h16.3c6.4 0 12.5 2.5 17 7l99.7 99.7c9.3 9.4 9.3 24.6 0 34zM344 208c0-75.2-60.8-136-136-136S72 132.8 72 208s60.8 136 136 136 136-60.8 136-136z"></path>
        </svg>
    </div>
    <div id="zoom_info" style="width: 40px; text-align: center;">100%</div>
    <div id="button_zoomOut" title="zoom out" class="svgButton flexCenter mlr5">
        <svg width="20" height="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
            <path d="M304 192v32c0 6.6-5.4 12-12 12H124c-6.6 0-12-5.4-12-12v-32c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12zm201 284.7L476.7 505c-9.4 9.4-24.6 9.4-33.9 0L343 405.3c-4.5-4.5-7-10.6-7-17V372c-35.3 27.6-79.7 44-128 44C93.1 416 0 322.9 0 208S93.1 0 208 0s208 93.1 208 208c0 48.3-16.4 92.7-44 128h16.3c6.4 0 12.5 2.5 17 7l99.7 99.7c9.3 9.4 9.3 24.6 0 34zM344 208c0-75.2-60.8-136-136-136S72 132.8 72 208s60.8 136 136 136 136-60.8 136-136z"></path>
        </svg>
    </div>
    <div id="button_toggleLayers" title="toggle layers" class="svgButton flexCenter mlr5 active">
        <svg width="20" height="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
            <path d="M12.41 148.02l232.94 105.67c6.8 3.09 14.49 3.09 21.29 0l232.94-105.67c16.55-7.51 16.55-32.52 0-40.03L266.65 2.31a25.607 25.607 0 0 0-21.29 0L12.41 107.98c-16.55 7.51-16.55 32.53 0 40.04zm487.18 88.28l-58.09-26.33-161.64 73.27c-7.56 3.43-15.59 5.17-23.86 5.17s-16.29-1.74-23.86-5.17L70.51 209.97l-58.1 26.33c-16.55 7.5-16.55 32.5 0 40l232.94 105.59c6.8 3.08 14.49 3.08 21.29 0L499.59 276.3c16.55-7.5 16.55-32.5 0-40zm0 127.8l-57.87-26.23-161.86 73.37c-7.56 3.43-15.59 5.17-23.86 5.17s-16.29-1.74-23.86-5.17L70.29 337.87 12.41 364.1c-16.55 7.5-16.55 32.5 0 40l232.94 105.59c6.8 3.08 14.49 3.08 21.29 0L499.59 404.1c16.55-7.5 16.55-32.5 0-40z"></path>
        </svg>
    </div>
    <div id="button_settings" title="settings" class="svgButton flexCenter mlr5">
        <svg width="20" height="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
            <path d="M487.4 315.7l-42.6-24.6c4.3-23.2 4.3-47 0-70.2l42.6-24.6c4.9-2.8 7.1-8.6 5.5-14-11.1-35.6-30-67.8-54.7-94.6-3.8-4.1-10-5.1-14.8-2.3L380.8 110c-17.9-15.4-38.5-27.3-60.8-35.1V25.8c0-5.6-3.9-10.5-9.4-11.7-36.7-8.2-74.3-7.8-109.2 0-5.5 1.2-9.4 6.1-9.4 11.7V75c-22.2 7.9-42.8 19.8-60.8 35.1L88.7 85.5c-4.9-2.8-11-1.9-14.8 2.3-24.7 26.7-43.6 58.9-54.7 94.6-1.7 5.4.6 11.2 5.5 14L67.3 221c-4.3 23.2-4.3 47 0 70.2l-42.6 24.6c-4.9 2.8-7.1 8.6-5.5 14 11.1 35.6 30 67.8 54.7 94.6 3.8 4.1 10 5.1 14.8 2.3l42.6-24.6c17.9 15.4 38.5 27.3 60.8 35.1v49.2c0 5.6 3.9 10.5 9.4 11.7 36.7 8.2 74.3 7.8 109.2 0 5.5-1.2 9.4-6.1 9.4-11.7v-49.2c22.2-7.9 42.8-19.8 60.8-35.1l42.6 24.6c4.9 2.8 11 1.9 14.8-2.3 24.7-26.7 43.6-58.9 54.7-94.6 1.5-5.5-.7-11.3-5.6-14.1zM256 336c-44.1 0-80-35.9-80-80s35.9-80 80-80 80 35.9 80 80-35.9 80-80 80z"></path>
        </svg>
    </div>
    <div id="button_fileExport" title="file export" class="svgButton flexCenter mlr5">
        <svg width="20" height="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
            <path d="M216 0h80c13.3 0 24 10.7 24 24v168h87.7c17.8 0 26.7 21.5 14.1 34.1L269.7 378.3c-7.5 7.5-19.8 7.5-27.3 0L90.1 226.1c-12.6-12.6-3.7-34.1 14.1-34.1H192V24c0-13.3 10.7-24 24-24zm296 376v112c0 13.3-10.7 24-24 24H24c-13.3 0-24-10.7-24-24V376c0-13.3 10.7-24 24-24h146.7l49 49c20.1 20.1 52.5 20.1 72.6 0l49-49H488c13.3 0 24 10.7 24 24zm-124 88c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20zm64 0c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20z"></path>
        </svg>
    </div>
    <div id="button_fileImport" title="file import" class="svgButton flexCenter mlr5">
        <svg width="20" height="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
            <path d="M296 384h-80c-13.3 0-24-10.7-24-24V192h-87.7c-17.8 0-26.7-21.5-14.1-34.1L242.3 5.7c7.5-7.5 19.8-7.5 27.3 0l152.2 152.2c12.6 12.6 3.7 34.1-14.1 34.1H320v168c0 13.3-10.7 24-24 24zm216-8v112c0 13.3-10.7 24-24 24H24c-13.3 0-24-10.7-24-24V376c0-13.3 10.7-24 24-24h136v8c0 30.9 25.1 56 56 56h80c30.9 0 56-25.1 56-56v-8h136c13.3 0 24 10.7 24 24zm-124 88c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20zm64 0c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20z"></path>
        </svg>
    </div>
</div>

<div id="secondaryMenu" class="flexCenter">
    <div id="spectrumTrigger_backgroundColor" class="clickable mlr10" title="background"></div>
    <div id="button_addImage" class="textButton flexCenter mlr10">Image</div>
    <div id="button_addRectangle" class="textButton flexCenter mlr10">Rectangle</div>
    <div id="button_addCircle" class="textButton flexCenter mlr10">Circle</div>
    <div id="button_addTriangle" class="textButton flexCenter mlr10">Triangle</div>
    <div id="button_addText" class="textButton flexCenter mlr10">Text</div>
    <div id="button_openIconfinder" class="textButton flexCenter mlr10">Iconfinder</div>
    <?php
    if ($_SESSION['isAuthenticated']) {
        echo '<div id="button_openDrawings" class="textButton flexCenter mlr10">Drawings</div>';
    }
    ?>
</div>

<div id="objectsOptionsPanel" class="topPanel">
    <!-- image fx -->
    <div id="button_imageFx" class="imageOption mlr5 dropdown" title="Image Effects">
        <!-- dropdown trigger -->
        <div class="dropdownTrigger clickable" onclick="jQuery(this.parentElement).toggleClass('dropdownOpen');">
            <svg height="20" style="enable-background:new 0 0 512 512;" version="1.1" viewBox="0 0 512 512" width="20" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <path d="M368,224c26.5,0,48-21.5,48-48c0-26.5-21.5-48-48-48c-26.5,0-48,21.5-48,48C320,202.5,341.5,224,368,224z"/>
                <path d="M452,64H60c-15.6,0-28,12.7-28,28.3v327.4c0,15.6,12.4,28.3,28,28.3h392c15.6,0,28-12.7,28-28.3V92.3 C480,76.7,467.6,64,452,64z M348.9,261.7c-3-3.5-7.6-6.2-12.8-6.2c-5.1,0-8.7,2.4-12.8,5.7l-18.7,15.8c-3.9,2.8-7,4.7-11.5,4.7 c-4.3,0-8.2-1.6-11-4.1c-1-0.9-2.8-2.6-4.3-4.1L224,215.3c-4-4.6-10-7.5-16.7-7.5c-6.7,0-12.9,3.3-16.8,7.8L64,368.2V107.7 c1-6.8,6.3-11.7,13.1-11.7h357.7c6.9,0,12.5,5.1,12.9,12l0.3,260.4L348.9,261.7z"/>
            </svg>
        </div>
        <!-- dropdown -->
        <div id="imageFxMenu" class="dropdownHidden" style="top: 160%;">
            <!-- Image Filters -->
            <div id="imageFx_section_imageFilters">
                <div class="imageFx_sectionTitle">
                    <div>Image Filters</div>
                </div>
                <div class="imageFx_sectionOptions">
                    <!-- Invert -->
                    <div class="imageFx_option imageFx_checkboxContainer" data-filter-name="Invert">
                        <div class="imageFx_checkbox flexCenter">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792" width="14" height="14">
                                <path d="M1671 566q0 40-28 68l-724 724-136 136q-28 28-68 28t-68-28l-136-136-362-362q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 295 656-657q28-28 68-28t68 28l136 136q28 28 28 68z"/>
                            </svg>
                        </div>
                        <div class="imageFx_type">Invert</div>
                    </div>
                    <!-- Sepia -->
                    <div class="imageFx_option imageFx_checkboxContainer" data-filter-name="Sepia">
                        <div class="imageFx_checkbox flexCenter">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792" width="14" height="14">
                                <path d="M1671 566q0 40-28 68l-724 724-136 136q-28 28-68 28t-68-28l-136-136-362-362q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 295 656-657q28-28 68-28t68 28l136 136q28 28 28 68z"/>
                            </svg>
                        </div>
                        <div class="imageFx_type">Sepia</div>
                    </div>
                    <!-- Black & White -->
                    <div class="imageFx_option imageFx_checkboxContainer" data-filter-name="BlackWhite">
                        <div class="imageFx_checkbox flexCenter">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792" width="14" height="14">
                                <path d="M1671 566q0 40-28 68l-724 724-136 136q-28 28-68 28t-68-28l-136-136-362-362q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 295 656-657q28-28 68-28t68 28l136 136q28 28 28 68z"/>
                            </svg>
                        </div>
                        <div class="imageFx_type">Black & White</div>
                    </div>
                    <!-- Brownie -->
                    <div class="imageFx_option imageFx_checkboxContainer" data-filter-name="Brownie">
                        <div class="imageFx_checkbox flexCenter">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792" width="14" height="14">
                                <path d="M1671 566q0 40-28 68l-724 724-136 136q-28 28-68 28t-68-28l-136-136-362-362q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 295 656-657q28-28 68-28t68 28l136 136q28 28 28 68z"/>
                            </svg>
                        </div>
                        <div class="imageFx_type">Brownie</div>
                    </div>
                    <!-- Vintage -->
                    <div class="imageFx_option imageFx_checkboxContainer" data-filter-name="Vintage">
                        <div class="imageFx_checkbox flexCenter">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792" width="14" height="14">
                                <path d="M1671 566q0 40-28 68l-724 724-136 136q-28 28-68 28t-68-28l-136-136-362-362q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 295 656-657q28-28 68-28t68 28l136 136q28 28 28 68z"/>
                            </svg>
                        </div>
                        <div class="imageFx_type">Vintage</div>
                    </div>
                    <!-- Kodachrome -->
                    <div class="imageFx_option imageFx_checkboxContainer" data-filter-name="Kodachrome">
                        <div class="imageFx_checkbox flexCenter">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792" width="14" height="14">
                                <path d="M1671 566q0 40-28 68l-724 724-136 136q-28 28-68 28t-68-28l-136-136-362-362q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 295 656-657q28-28 68-28t68 28l136 136q28 28 28 68z"/>
                            </svg>
                        </div>
                        <div class="imageFx_type">Kodachrome</div>
                    </div>
                    <!-- Technicolor -->
                    <div class="imageFx_option imageFx_checkboxContainer" data-filter-name="Technicolor">
                        <div class="imageFx_checkbox flexCenter">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792" width="14" height="14">
                                <path d="M1671 566q0 40-28 68l-724 724-136 136q-28 28-68 28t-68-28l-136-136-362-362q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 295 656-657q28-28 68-28t68 28l136 136q28 28 28 68z"/>
                            </svg>
                        </div>
                        <div class="imageFx_type">Technicolor</div>
                    </div>
                    <!-- Polaroid -->
                    <div class="imageFx_option imageFx_checkboxContainer" data-filter-name="Polaroid">
                        <div class="imageFx_checkbox flexCenter">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792" width="14" height="14">
                                <path d="M1671 566q0 40-28 68l-724 724-136 136q-28 28-68 28t-68-28l-136-136-362-362q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 295 656-657q28-28 68-28t68 28l136 136q28 28 28 68z"/>
                            </svg>
                        </div>
                        <div class="imageFx_type">Polaroid</div>
                    </div>
                </div>
            </div>
            <!-- Grayscale -->
            <div id="imageFx_section_grayscale">
                <div class="imageFx_sectionTitle">
                    <div>Grayscale</div>
                    <div id="imageFx_grayscaleSwitch" class="imageFx_switchContainer">
                        <div class="imageFx_switch flexCenter">
                            <div class="imageFx_innerSwitch"></div>
                        </div>
                    </div>
                </div>
                <div class="imageFx_sectionOptions">
                    <!-- Average -->
                    <div class="imageFx_option imageFx_radioContainer" data-filter-mode="average">
                        <div class="imageFx_radio flexCenter">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10 10" width="10" height="10">
                                <circle cx="5" cy="5" r="5"></circle>
                            </svg>
                        </div>
                        <div class="imageFx_type">Average</div>
                    </div>
                    <!-- Luminosity -->
                    <div class="imageFx_option imageFx_radioContainer" data-filter-mode="luminosity">
                        <div class="imageFx_radio flexCenter">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10 10" width="10" height="10">
                                <circle cx="5" cy="5" r="5"></circle>
                            </svg>
                        </div>
                        <div class="imageFx_type">Luminosity</div>
                    </div>
                    <!-- Lightness -->
                    <div class="imageFx_option imageFx_radioContainer" data-filter-mode="lightness">
                        <div class="imageFx_radio flexCenter">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10 10" width="10" height="10">
                                <circle cx="5" cy="5" r="5"></circle>
                            </svg>
                        </div>
                        <div class="imageFx_type">Lightness</div>
                    </div>
                </div>
            </div>
            <!-- Fx Effects -->
            <div id="imageFx_section_fxEffects">
                <div class="imageFx_sectionTitle">Fx Effects</div>
                <div class="imageFx_sectionOptions">
                    <!-- Blur -->
                    <div class="imageFx_option imageFx_checkboxContainer" data-filter-name="Blur">
                        <div class="imageFx_checkbox flexCenter">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792" width="14" height="14">
                                <path d="M1671 566q0 40-28 68l-724 724-136 136q-28 28-68 28t-68-28l-136-136-362-362q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 295 656-657q28-28 68-28t68 28l136 136q28 28 28 68z"/>
                            </svg>
                        </div>
                        <div class="imageFx_type">Blur</div>
                        <input type="text" class="inputValue" title="0 to 1">
                        <input type="range" class="inputRange" min="0" max="1" step="0.01" value="0">
                    </div>
                    <!-- Separator -->
                    <div class="separator"></div>
                    <!-- Pixelate -->
                    <div class="imageFx_option imageFx_checkboxContainer" data-filter-name="Pixelate">
                        <div class="imageFx_checkbox flexCenter">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792" width="14" height="14">
                                <path d="M1671 566q0 40-28 68l-724 724-136 136q-28 28-68 28t-68-28l-136-136-362-362q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 295 656-657q28-28 68-28t68 28l136 136q28 28 28 68z"/>
                            </svg>
                        </div>
                        <div class="imageFx_type">Pixelate</div>
                        <input type="text" class="inputValue" title="1 to 100">
                        <input type="range" class="inputRange" min="1" max="100" value="1">
                    </div>
                    <!-- Noise -->
                    <div class="imageFx_option imageFx_checkboxContainer" data-filter-name="Noise">
                        <div class="imageFx_checkbox flexCenter">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792" width="14" height="14">
                                <path d="M1671 566q0 40-28 68l-724 724-136 136q-28 28-68 28t-68-28l-136-136-362-362q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 295 656-657q28-28 68-28t68 28l136 136q28 28 28 68z"/>
                            </svg>
                        </div>
                        <div class="imageFx_type">Noise</div>
                        <input type="text" class="inputValue" title="0 to 1000">
                        <input type="range" class="inputRange" min="0" max="1000" value="0">
                    </div>
                    <!-- Separator -->
                    <div class="separator"></div>
                    <!-- Hue -->
                    <div class="imageFx_option imageFx_checkboxContainer" data-filter-name="HueRotation">
                        <div class="imageFx_checkbox flexCenter">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792" width="14" height="14">
                                <path d="M1671 566q0 40-28 68l-724 724-136 136q-28 28-68 28t-68-28l-136-136-362-362q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 295 656-657q28-28 68-28t68 28l136 136q28 28 28 68z"/>
                            </svg>
                        </div>
                        <div class="imageFx_type">Hue</div>
                        <input type="text" class="inputValue" title="-1 to 1">
                        <input type="range" class="inputRange" min="-1" max="1" step="0.01" value="0">
                    </div>
                    <!-- Saturation -->
                    <div class="imageFx_option imageFx_checkboxContainer" data-filter-name="Saturation">
                        <div class="imageFx_checkbox flexCenter">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792" width="14" height="14">
                                <path d="M1671 566q0 40-28 68l-724 724-136 136q-28 28-68 28t-68-28l-136-136-362-362q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 295 656-657q28-28 68-28t68 28l136 136q28 28 28 68z"/>
                            </svg>
                        </div>
                        <div class="imageFx_type">Saturation</div>
                        <input type="text" class="inputValue" title="-1 to 1">
                        <input type="range" class="inputRange" min="-1" max="1" step="0.01" value="0">
                    </div>
                    <!-- Separator -->
                    <div class="separator"></div>
                    <!-- Contrast -->
                    <div class="imageFx_option imageFx_checkboxContainer" data-filter-name="Contrast">
                        <div class="imageFx_checkbox flexCenter">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792" width="14" height="14">
                                <path d="M1671 566q0 40-28 68l-724 724-136 136q-28 28-68 28t-68-28l-136-136-362-362q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 295 656-657q28-28 68-28t68 28l136 136q28 28 28 68z"/>
                            </svg>
                        </div>
                        <div class="imageFx_type">Contrast</div>
                        <input type="text" class="inputValue" title="-1 to 1">
                        <input type="range" class="inputRange" min="-1" max="1" step="0.01" value="0">
                    </div>
                    <!-- Brightness -->
                    <div class="imageFx_option imageFx_checkboxContainer" data-filter-name="Brightness">
                        <div class="imageFx_checkbox flexCenter">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792" width="14" height="14">
                                <path d="M1671 566q0 40-28 68l-724 724-136 136q-28 28-68 28t-68-28l-136-136-362-362q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 295 656-657q28-28 68-28t68 28l136 136q28 28 28 68z"/>
                            </svg>
                        </div>
                        <div class="imageFx_type">Brightness</div>
                        <input type="text" class="inputValue" title="-1 to 1">
                        <input type="range" class="inputRange" min="-1" max="1" step="0.01" value="0">
                    </div>
                </div>
            </div>
            <!-- Gamma -->
            <div id="imageFx_section_gamma">
                <div class="imageFx_sectionTitle">
                    <div>Gamma</div>
                    <div id="imageFx_gammaSwitch" class="imageFx_switchContainer">
                        <div class="imageFx_switch flexCenter">
                            <div class="imageFx_innerSwitch"></div>
                        </div>
                    </div>
                </div>
                <div class="imageFx_sectionOptions">
                    <!-- Red -->
                    <div class="imageFx_option" data-filter-channel="red">
                        <div class="imageFx_type">Red</div>
                        <input type="text" class="inputValue" title="0.1 to 2.2">
                        <input type="range" id="gammaRedRange" class="inputRange" min="0.1" max="2.2" step="0.1" value="1">
                    </div>
                    <!-- Separator -->
                    <div class="separator"></div>
                    <!-- Green -->
                    <div class="imageFx_option" data-filter-channel="green">
                        <div class="imageFx_type">Green</div>
                        <input type="text" class="inputValue" title="0.1 to 2.2">
                        <input type="range" id="gammaGreenRange" class="inputRange" min="0.1" max="2.2" step="0.1" value="1">
                    </div>
                    <!-- Blue -->
                    <div class="imageFx_option" data-filter-channel="blue">
                        <div class="imageFx_type">Blue</div>
                        <input type="text" class="inputValue" title="0.1 to 2.2">
                        <input type="range" id="gammaBlueRange" class="inputRange" min="0.1" max="2.2" step="0.1" value="1">
                    </div>
                </div>
            </div>
            <!-- reset -->
            <div id="button_resetImageFx">
                <span>RESET</span>
            </div>
        </div>
    </div>
    <!-- clip image -->
    <div id="button_clip" class="imageOption clickable flexCenter mlr5" title="Clip Image">
        <svg width="20" height="20" style="enable-background:new 0 0 50 50;" version="1.1" viewBox="0 0 50 50" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <path d="M43,8.414l5.707-5.707l-1.414-1.414L41.586,7H9V1H7v6H1v2h6v34h34v6h2v-6h6v-2h-6V8.414z M39.586,9L9,39.586V9H39.586z M10.414,41L41,10.414V41H10.414z"/>
        </svg>
    </div>
    <!-- border radius -->
    <div class="dropdown rectOption mlr5" title="Border Radius" style="position: relative;">
        <div class="dropdownTrigger clickable flexCenter" onclick="jQuery(this.parentElement).toggleClass('dropdownOpen');">
            <svg id="borderRadiusSvg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="20" width="20">
                <rect x="0" y="0" width="512" height="50"/>
                <rect x="0" y="0" width="50" height="512"/>
                <path stroke-width="10" fill="none" d="M 50,512 A 462,462 0 0,1 512,50"></path>
            </svg>
        </div>
        <div class="dropdownHidden" style="top: 175%; left: -44px;">
            <input id="range_rectangleBorderRadius" type="range" min="0">
        </div>
    </div>
    <!-- color -->
    <div id="spectrumTrigger_singleColor" class="singleObjectOption clickable flexCenter mlr5"></div>
    <!-- font family -->
    <div class="textOption dropdown mlr5">
        <div id="fontFamilyContainer" class="dropdownTrigger clickable">
            <div id="div_fontSelected"></div>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 7.38 4.06" height="6">
                <path d="M3.69,4.06A.36.36,0,0,1,3.42,4L.11.64a.37.37,0,0,1,0-.53.37.37,0,0,1,.53,0L4,3.42A.37.37,0,0,1,4,4,.34.34,0,0,1,3.69,4.06Z"/>
                <path d="M3.69,4.06A.36.36,0,0,1,3.42,4a.39.39,0,0,1,0-.53L6.74.11a.37.37,0,0,1,.53,0,.39.39,0,0,1,0,.53L4,4A.34.34,0,0,1,3.69,4.06Z"/>
            </svg>
        </div>
        <div id="fontsDropdown" class="dropdownHidden" style="top: 150%;">
            <div id="fontsDropdownTitle">Fonts Repository</div>
            <div id="fontsContainer">
                <?php
                for ($counter = 0; $counter < count($fonts); $counter++) {
                    $path = $fonts[$counter];
                    $file = str_replace('fonts/', '', $path);
                    $fontFamily = substr($file, 0, -4);
                    //echo '<div class="fontOption" data-family="' . $fontFamily . '" style="font-family:' . $fontFamily . '">' . $fontFamily . '</div>';
                    echo '<img src="images/fonts/' . $fontFamily . '.png" class="fontOption" data-family="' . $fontFamily . '">';
                }
                ?>
            </div>
        </div>
    </div>
    <!-- font size -->
    <input id="input_fontSize" class="textOption mlr5" type="text" autocomplete="off" title="Font Size">
    <!-- characters spacing -->
    <div class="textOption optionEditableContainer mlr5" title="Characters spacing" style="position: relative;">
        <div class="optionEditableTitle flexCenter">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10.66 13.16" width="15" height="15">
                <path d="M8.26,0V1.19H6.41V8.35h-2V1.19H2.57V0Z"/>
                <path d="M10,11.66H.63a.63.63,0,1,1,0-1.25H10a.63.63,0,1,1,0,1.25Z"/>
                <path d="M10,11.66a.63.63,0,0,1-.53-.3l-.93-1.5A.63.63,0,0,1,8.77,9a.62.62,0,0,1,.86.21l.93,1.49a.63.63,0,0,1-.2.86A.58.58,0,0,1,10,11.66Z"/>
                <path d="M9.1,13.16a.6.6,0,0,1-.33-.1.63.63,0,0,1-.2-.86l.93-1.5a.62.62,0,1,1,1.06.66l-.93,1.5A.62.62,0,0,1,9.1,13.16Z"/>
                <path d="M10,11.66H.63a.63.63,0,1,1,0-1.25H10a.63.63,0,1,1,0,1.25Z"/>
                <path d="M1.55,13.16a.63.63,0,0,1-.53-.3l-.93-1.5A.62.62,0,0,1,.3,10.5a.63.63,0,0,1,.86.2l.92,1.5a.63.63,0,0,1-.2.86A.58.58,0,0,1,1.55,13.16Z"/>
                <path d="M.62,11.66a.55.55,0,0,1-.32-.1.62.62,0,0,1-.21-.86L1,9.21a.62.62,0,1,1,1.06.65l-.92,1.5A.65.65,0,0,1,.62,11.66Z"/>
            </svg>
        </div>
        <input id="input_charSpacing" class="optionEditableInput" type="text" autocomplete="off" style="width: 40px;">
    </div>
    <!-- line height -->
    <div class="textOption optionEditableContainer mlr5" title="Line height" style="position: relative;">
        <div class="optionEditableTitle flexCenter">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 11.96 10.66" width="15" height="15">
                <path d="M5.68.41V1.74H3.83V9.8h-2V1.74H0V.41Z"/>
                <path d="M9.84,10.66A.63.63,0,0,1,9.21,10V.63a.63.63,0,1,1,1.25,0V10A.63.63,0,0,1,9.84,10.66Z"/>
                <path d="M9.84,10.66a.65.65,0,0,1-.54-.3.62.62,0,0,1,.21-.86L11,8.57a.63.63,0,1,1,.65,1.07l-1.5.92A.55.55,0,0,1,9.84,10.66Z"/>
                <path d="M9.83,10.66a.55.55,0,0,1-.32-.1L8,9.64a.62.62,0,0,1-.21-.86.63.63,0,0,1,.87-.21l1.49.93a.62.62,0,0,1,.21.86A.65.65,0,0,1,9.83,10.66Z"/>
                <path d="M9.84,10.66A.63.63,0,0,1,9.21,10V.63a.63.63,0,1,1,1.25,0V10A.63.63,0,0,1,9.84,10.66Z"/>
                <path d="M8.34,2.18a.62.62,0,0,1-.53-.3A.61.61,0,0,1,8,1L9.51.09a.63.63,0,0,1,.65,1.07l-1.49.92A.6.6,0,0,1,8.34,2.18Z"/>
                <path d="M11.33,2.18a.6.6,0,0,1-.33-.1L9.51,1.16A.62.62,0,0,1,9.3.3a.63.63,0,0,1,.87-.21L11.66,1a.62.62,0,0,1,.21.86A.65.65,0,0,1,11.33,2.18Z"/>
            </svg>
        </div>
        <input id="input_lineHeight" class="optionEditableInput" type="text" autocomplete="off" style="width: 40px;">
    </div>
    <!-- align text -->
    <div class="textOption alignText clickable flexCenter ml5 mr2" data-textAlign="left" title="Align Left">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13.63 11.25" height="20" width="20">
            <path d="M13,1.25H.63A.63.63,0,1,1,.63,0H13a.63.63,0,0,1,0,1.25Z"/>
            <path d="M9.43,4.58H.63a.63.63,0,1,1,0-1.25h8.8a.63.63,0,1,1,0,1.25Z"/>
            <path d="M13,7.92H.63a.63.63,0,1,1,0-1.25H13a.63.63,0,0,1,0,1.25Z"/>
            <path d="M9.43,11.25H.63A.63.63,0,1,1,.63,10h8.8a.63.63,0,1,1,0,1.25Z"/>
        </svg>
    </div>
    <div class="textOption alignText clickable flexCenter mlr2" data-textAlign="center" title="Align Center">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13.63 11.25" height="20" width="20">
            <path d="M13,1.25H.63A.63.63,0,1,1,.63,0H13a.63.63,0,0,1,0,1.25Z"/>
            <path d="M11.22,4.58H2.41a.63.63,0,0,1,0-1.25h8.81a.63.63,0,0,1,0,1.25Z"/>
            <path d="M13,7.92H.63a.63.63,0,1,1,0-1.25H13a.63.63,0,0,1,0,1.25Z"/>
            <path d="M11.22,11.25H2.41a.63.63,0,0,1,0-1.25h8.81a.63.63,0,0,1,0,1.25Z"/>
        </svg>
    </div>
    <div class="textOption alignText clickable flexCenter mlr2" data-textAlign="right" title="Align Right">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13.63 11.25" height="20" width="20">
            <path d="M13,1.25H.63A.63.63,0,1,1,.63,0H13a.63.63,0,0,1,0,1.25Z"/>
            <path d="M13,4.58H4.2a.63.63,0,1,1,0-1.25H13a.63.63,0,0,1,0,1.25Z"/>
            <path d="M13,7.92H.63a.63.63,0,1,1,0-1.25H13a.63.63,0,0,1,0,1.25Z"/>
            <path d="M13,11.25H4.2A.63.63,0,1,1,4.2,10H13a.63.63,0,0,1,0,1.25Z"/>
        </svg>
    </div>
    <div class="textOption alignText clickable flexCenter ml2 mr5" data-textAlign="justify" title="Justify">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13.63 11.25" height="20" width="20">
            <path d="M13,1.25H.63A.63.63,0,1,1,.63,0H13a.63.63,0,0,1,0,1.25Z"/>
            <path d="M13,4.58H.63a.63.63,0,1,1,0-1.25H13a.63.63,0,0,1,0,1.25Z"/>
            <path d="M13,7.92H.63a.63.63,0,1,1,0-1.25H13a.63.63,0,0,1,0,1.25Z"/>
            <path d="M13,11.25H.63A.63.63,0,1,1,.63,10H13a.63.63,0,0,1,0,1.25Z"/>
        </svg>
    </div>
    <!-- opacity -->
    <div class="alwaysOption optionEditableContainer mlr5" title="Opacity" style="position: relative;">
        <div class="optionEditableTitle">opacity</div>
        <input id="input_opacity" class="optionEditableInput" type="text" autocomplete="off" style="width: 40px;">
    </div>
    <!-- align object -->
    <div class="alwaysOption alignObject clickable flexCenter ml5 mr2" data-objectAlign="left" title="Horizontal Align Left">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15.07 15.06" height="20" width="20">
            <path d="M11.13,15.06H8.76a.63.63,0,0,1,0-1.25h2.37a2.62,2.62,0,0,0,1.65-.57.63.63,0,1,1,.77,1A3.94,3.94,0,0,1,11.13,15.06Zm-4.74,0H4a.63.63,0,1,1,0-1.25H6.39a.63.63,0,1,1,0,1.25ZM1.84,14.3a.62.62,0,0,1-.4-.15A4,4,0,0,1,.06,11.77a.62.62,0,0,1,.51-.72.61.61,0,0,1,.72.51,2.71,2.71,0,0,0,1,1.63.63.63,0,0,1,.08.88A.62.62,0,0,1,1.84,14.3Zm12.54-1.91h-.12a.63.63,0,0,1-.49-.74,3,3,0,0,0,.05-.52V9.4a.63.63,0,0,1,1.25,0v1.72a3.94,3.94,0,0,1-.07.77A.63.63,0,0,1,14.38,12.39ZM.63,9.93A.63.63,0,0,1,0,9.3V6.93a.63.63,0,0,1,1.25,0V9.3A.63.63,0,0,1,.63,9.93ZM14.45,7.66A.63.63,0,0,1,13.82,7V4.67a.63.63,0,1,1,1.25,0V7A.63.63,0,0,1,14.45,7.66ZM.63,5.19A.63.63,0,0,1,0,4.56V3.94A3.94,3.94,0,0,1,.54,2a.63.63,0,0,1,.86-.22.61.61,0,0,1,.22.85,2.68,2.68,0,0,0-.36,1.35v.62A.63.63,0,0,1,.63,5.19ZM14,3a.64.64,0,0,1-.55-.33,2.68,2.68,0,0,0-1.41-1.24.61.61,0,0,1-.36-.8.62.62,0,0,1,.8-.37,3.93,3.93,0,0,1,2.07,1.82.62.62,0,0,1-.25.85A.6.6,0,0,1,14,3ZM2.88,1.43A.63.63,0,0,1,2.28,1a.63.63,0,0,1,.39-.8A4,4,0,0,1,4,0H5.22a.63.63,0,1,1,0,1.25H4a2.57,2.57,0,0,0-.87.15ZM10,1.25H7.59A.63.63,0,0,1,7.59,0H10a.63.63,0,0,1,0,1.25Z"/>
            <path d="M4.42.63h.42a0,0,0,0,1,0,0V14.44a0,0,0,0,1,0,0H4.42A3.79,3.79,0,0,1,.63,10.65V4.41A3.79,3.79,0,0,1,4.42.63Z"/>
            <path d="M4.79,0H3.6A3.61,3.61,0,0,0,0,3.63v7.8A3.61,3.61,0,0,0,3.6,15H4.79Z"/>
        </svg>
    </div>
    <div class="alwaysOption alignObject clickable flexCenter mlr2" data-objectAlign="centerH" title="Horizontal Align Center">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15.06 15.07" height="20" width="20">
            <path d="M11.12,15.07H8.75a.63.63,0,0,1,0-1.25h2.37a2.62,2.62,0,0,0,1.65-.57.63.63,0,1,1,.77,1A3.94,3.94,0,0,1,11.12,15.07Zm-4.73,0H4a.63.63,0,1,1,0-1.25H6.39a.63.63,0,0,1,0,1.25Zm-4.56-.76a.62.62,0,0,1-.4-.15A4,4,0,0,1,.05,11.78a.63.63,0,0,1,.52-.72.62.62,0,0,1,.72.51,2.64,2.64,0,0,0,.94,1.63.63.63,0,0,1,.08.88A.62.62,0,0,1,1.83,14.31ZM14.38,12.4h-.13a.63.63,0,0,1-.49-.74,3,3,0,0,0,.05-.52V9.41a.63.63,0,0,1,1.25,0v1.72a3.94,3.94,0,0,1-.07.77A.62.62,0,0,1,14.38,12.4ZM.63,9.94A.63.63,0,0,1,0,9.31V6.94a.63.63,0,0,1,1.25,0V9.31A.63.63,0,0,1,.63,9.94ZM14.44,7.67A.63.63,0,0,1,13.81,7V4.68a.63.63,0,1,1,1.25,0V7A.63.63,0,0,1,14.44,7.67ZM.63,5.2A.63.63,0,0,1,0,4.58V4A4,4,0,0,1,.54,2a.62.62,0,0,1,.85-.22.61.61,0,0,1,.22.85A2.78,2.78,0,0,0,1.25,4v.63A.62.62,0,0,1,.63,5.2ZM14,3a.61.61,0,0,1-.55-.33,2.73,2.73,0,0,0-1.42-1.24.61.61,0,0,1-.36-.8.61.61,0,0,1,.8-.36,3.91,3.91,0,0,1,2.07,1.81.62.62,0,0,1-.25.85A.59.59,0,0,1,14,3ZM2.87,1.44A.62.62,0,0,1,2.28,1a.63.63,0,0,1,.38-.8A4,4,0,0,1,3.94,0H5.21a.63.63,0,1,1,0,1.25H3.94a2.57,2.57,0,0,0-.87.15ZM10,1.26H7.58A.63.63,0,0,1,7.58,0H10a.63.63,0,0,1,.63.63A.63.63,0,0,1,10,1.26Z"/>
            <rect x="5.43" width="4.21" height="15.07"/>
        </svg>
    </div>
    <div class="alwaysOption alignObject clickable flexCenter mlr2" data-objectAlign="right" title="Horizontal Align Right">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15.06 15.19" height="20" width="20">
            <path d="M11.12,15.13H9.85a.63.63,0,1,1,0-1.25h1.27a2.57,2.57,0,0,0,.87-.15.63.63,0,0,1,.8.39.62.62,0,0,1-.39.79A4,4,0,0,1,11.12,15.13Zm-3.64,0H5.11a.63.63,0,0,1,0-1.25H7.48a.63.63,0,1,1,0,1.25Zm-4.71-.22a.57.57,0,0,1-.22,0A4,4,0,0,1,.48,13.06a.62.62,0,1,1,1.09-.6A2.73,2.73,0,0,0,3,13.7a.62.62,0,0,1,.36.81A.61.61,0,0,1,2.77,14.91ZM14,13.47a.61.61,0,0,1-.32-.08.62.62,0,0,1-.22-.86,2.61,2.61,0,0,0,.36-1.34v-.63a.63.63,0,0,1,1.25,0v.63a3.88,3.88,0,0,1-.53,2A.63.63,0,0,1,14,13.47ZM.63,11.09A.63.63,0,0,1,0,10.46V8.09a.63.63,0,0,1,1.25,0v2.37A.63.63,0,0,1,.63,11.09ZM14.44,8.82a.63.63,0,0,1-.63-.63V5.83a.63.63,0,1,1,1.25,0V8.19A.63.63,0,0,1,14.44,8.82ZM.63,6.35A.63.63,0,0,1,0,5.72V4a3.84,3.84,0,0,1,.08-.76.62.62,0,1,1,1.22.24A3.05,3.05,0,0,0,1.25,4V5.72A.63.63,0,0,1,.63,6.35ZM14.39,4.08a.61.61,0,0,1-.61-.52,2.76,2.76,0,0,0-.95-1.63.62.62,0,0,1,.8-1A4,4,0,0,1,15,3.36a.64.64,0,0,1-.52.72ZM1.91,2a.6.6,0,0,1-.49-.24A.61.61,0,0,1,1.52.9,3.89,3.89,0,0,1,3.94.06H6.31a.63.63,0,0,1,0,1.25H3.94a2.68,2.68,0,0,0-1.65.57A.6.6,0,0,1,1.91,2ZM11,1.31H8.68a.63.63,0,1,1,0-1.25H11a.63.63,0,1,1,0,1.25Z"/>
            <path d="M14,.69h.42a0,0,0,0,1,0,0V14.5a0,0,0,0,1,0,0H14a3.79,3.79,0,0,1-3.79-3.79V4.48A3.79,3.79,0,0,1,14,.69Z" transform="translate(24.66 15.19) rotate(-180)"/>
            <path d="M10.21,15.19h1.21a3.65,3.65,0,0,0,3.64-3.64V3.65A3.66,3.66,0,0,0,11.42,0H10.21Z"/>
        </svg>
    </div>
    <div class="alwaysOption alignObject clickable flexCenter mlr2" data-objectAlign="top" title="Vertical Align Top">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15.21 15.07" height="20" width="20">
            <path d="M10.47,15.07H8.1a.63.63,0,0,1,0-1.25h2.37a.63.63,0,0,1,0,1.25Zm-4.74,0H4A3.92,3.92,0,0,1,3.25,15a.62.62,0,1,1,.24-1.22,3.05,3.05,0,0,0,.52.05H5.73a.63.63,0,1,1,0,1.25Zm7-.4a.62.62,0,0,1-.3-1.17,2.73,2.73,0,0,0,1.24-1.42.63.63,0,0,1,1.17.44,4,4,0,0,1-1.81,2.07A.56.56,0,0,1,12.77,14.67ZM1.4,13.79a.67.67,0,0,1-.5-.24,3.94,3.94,0,0,1-.83-2.42V8.76a.63.63,0,0,1,1.25,0v2.37a2.68,2.68,0,0,0,.57,1.65.63.63,0,0,1-.49,1Zm13.11-3.21a.62.62,0,0,1-.62-.62V7.59A.63.63,0,0,1,14.51,7a.63.63,0,0,1,.63.63V10A.63.63,0,0,1,14.51,10.58ZM.7,7a.63.63,0,0,1-.63-.63V4A.63.63,0,1,1,1.32,4V6.39A.63.63,0,0,1,.7,7ZM14.51,5.85a.63.63,0,0,1-.62-.63V4a2.57,2.57,0,0,0-.15-.87.63.63,0,0,1,.39-.8.62.62,0,0,1,.79.39A4,4,0,0,1,15.14,4V5.22A.63.63,0,0,1,14.51,5.85Zm-13-3.39a.61.61,0,0,1-.4-.14A.63.63,0,0,1,1,1.44,4,4,0,0,1,3.37.06a.63.63,0,0,1,.72.52.62.62,0,0,1-.52.71,2.71,2.71,0,0,0-1.63,1A.61.61,0,0,1,1.46,2.46Zm11.4-.75a.62.62,0,0,1-.32-.09,2.64,2.64,0,0,0-1.34-.36h-.63a.63.63,0,0,1,0-1.25h.63a3.91,3.91,0,0,1,2,.53.63.63,0,0,1-.31,1.17ZM8.2,1.26H5.83A.63.63,0,0,1,5.83,0H8.2a.63.63,0,1,1,0,1.25Z"/>
            <path d="M9.29-4.17h.42a0,0,0,0,1,0,0V9.64a0,0,0,0,1,0,0H9.29A3.79,3.79,0,0,1,5.5,5.85V-.38A3.79,3.79,0,0,1,9.29-4.17Z" transform="translate(10.34 -4.87) rotate(90)"/>
            <path d="M15.21,4.86V3.65A3.66,3.66,0,0,0,11.56,0H3.65A3.66,3.66,0,0,0,0,3.65V4.86Z"/>
        </svg>
    </div>
    <div class="alwaysOption alignObject clickable flexCenter mlr2" data-objectAlign="centerV" title="Vertical Align Center">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15.06 15.06" height="20" width="20">
            <path d="M9.3,15.06H6.93a.63.63,0,0,1,0-1.25H9.3a.63.63,0,1,1,0,1.25Zm-4.74,0H3.94a3.94,3.94,0,0,1-2-.53.62.62,0,0,1-.22-.86.61.61,0,0,1,.85-.22,2.68,2.68,0,0,0,1.35.36h.62a.63.63,0,1,1,0,1.25Zm7.1,0a.62.62,0,0,1-.61-.53.61.61,0,0,1,.51-.71,2.66,2.66,0,0,0,1.63-.95.63.63,0,0,1,.88-.08.62.62,0,0,1,.08.88A4,4,0,0,1,11.77,15ZM.8,12.82a.62.62,0,0,1-.59-.42A4,4,0,0,1,0,11.12V9.85a.63.63,0,1,1,1.25,0v1.27a2.83,2.83,0,0,0,.14.87.63.63,0,0,1-.38.8A.76.76,0,0,1,.8,12.82Zm13.64-1.15a.63.63,0,0,1-.63-.63V8.68a.63.63,0,1,1,1.25,0V11A.63.63,0,0,1,14.44,11.67ZM.63,8.1A.62.62,0,0,1,0,7.48V5.11a.63.63,0,0,1,1.25,0V7.48A.62.62,0,0,1,.63,8.1ZM14.44,6.93a.63.63,0,0,1-.63-.62V3.94a2.62,2.62,0,0,0-.57-1.65.63.63,0,0,1,1-.77,3.94,3.94,0,0,1,.83,2.42V6.31A.62.62,0,0,1,14.44,6.93ZM.84,3.4a.54.54,0,0,1-.22,0,.62.62,0,0,1-.37-.8A4,4,0,0,1,2.07.48a.61.61,0,0,1,.84.25.61.61,0,0,1-.25.84A2.73,2.73,0,0,0,1.42,3,.62.62,0,0,1,.84,3.4ZM11.77,1.31h-.13a2.87,2.87,0,0,0-.52-.05H9.4A.63.63,0,0,1,9.4,0h1.72a3.94,3.94,0,0,1,.77.08.61.61,0,0,1,.49.73A.62.62,0,0,1,11.77,1.31ZM7,1.25H4.67A.63.63,0,1,1,4.67,0H7A.63.63,0,0,1,7,1.25Z"/>
            <rect x="5.42" width="4.21" height="15.06" transform="translate(0 15.06) rotate(-90)"/>
        </svg>
    </div>
    <div class="alwaysOption alignObject clickable flexCenter ml2 mr5" data-objectAlign="bottom" title="Vertical Align Bottom">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15.19 15.06" height="20" width="20">
            <path d="M9.37,15.06H7a.63.63,0,0,1,0-1.25H9.37a.63.63,0,0,1,0,1.25Zm-4.74,0H4a3.92,3.92,0,0,1-2-.53.64.64,0,0,1-.23-.86.62.62,0,0,1,.86-.22A2.68,2.68,0,0,0,4,13.81h.62a.63.63,0,1,1,0,1.25Zm7.1,0a.63.63,0,0,1-.1-1.25,2.67,2.67,0,0,0,1.63-.94.63.63,0,0,1,.88-.08.62.62,0,0,1,.08.88A4,4,0,0,1,11.84,15ZM.87,12.82a.62.62,0,0,1-.59-.42,4,4,0,0,1-.21-1.28V9.85a.63.63,0,1,1,1.25,0v1.27a2.83,2.83,0,0,0,.14.87.62.62,0,0,1-.38.8A.76.76,0,0,1,.87,12.82Zm13.64-1.15a.63.63,0,0,1-.63-.63V8.68a.63.63,0,1,1,1.25,0V11A.63.63,0,0,1,14.51,11.67ZM.69,8.11a.63.63,0,0,1-.62-.63V5.11a.63.63,0,0,1,1.25,0V7.48A.63.63,0,0,1,.69,8.11ZM14.51,6.93a.63.63,0,0,1-.63-.62V3.94a2.68,2.68,0,0,0-.57-1.65.63.63,0,0,1,1-.77,3.94,3.94,0,0,1,.83,2.42V6.31A.62.62,0,0,1,14.51,6.93ZM.91,3.4a.64.64,0,0,1-.23,0,.62.62,0,0,1-.36-.8A4,4,0,0,1,2.13.48.61.61,0,0,1,3,.73a.61.61,0,0,1-.25.84A2.73,2.73,0,0,0,1.49,3,.62.62,0,0,1,.91,3.4ZM11.83,1.31h-.12a3,3,0,0,0-.52-.05H9.47A.63.63,0,0,1,8.84.63.63.63,0,0,1,9.47,0h1.72A4,4,0,0,1,12,.08a.62.62,0,0,1-.13,1.23ZM7.1,1.25H4.73A.63.63,0,0,1,4.73,0H7.1a.63.63,0,1,1,0,1.25Z"/>
            <path d="M9.28,5.43H9.7a0,0,0,0,1,0,0V19.24a0,0,0,0,1,0,0H9.28a3.79,3.79,0,0,1-3.79-3.79V9.22A3.79,3.79,0,0,1,9.28,5.43Z" transform="translate(-4.73 19.93) rotate(-90)"/>
            <path d="M0,10.21v1.21a3.66,3.66,0,0,0,3.65,3.64h7.9a3.65,3.65,0,0,0,3.64-3.64V10.21Z"/>
        </svg>
    </div>
    <!-- top / left -->
    <div class="alwaysOption optionEditableContainer ml5 mr2" title="Top">
        <div class="optionEditableTitle">top</div>
        <input id="input_top" class="optionEditableInput" type="text" autocomplete="off" style="width: 55px;">
    </div>
    <div class="alwaysOption optionEditableContainer ml2 mr5" title="Left">
        <div class="optionEditableTitle">left</div>
        <input id="input_left" class="optionEditableInput" type="text" autocomplete="off" style="width: 55px;">
    </div>
    <!-- width / height -->
    <div class="alwaysOption optionEditableContainer ml5 mr2" title="Width">
        <div class="optionEditableTitle">width</div>
        <input id="input_width" class="optionEditableInput" type="text" autocomplete="off" style="width: 55px;">
    </div>
    <div id="button_linkDimensions" class="alwaysOption clickable flexCenter mlr2" title="Link" onclick="jQuery(this).toggleClass('linked');">
        <svg height="20" viewBox="0 0 32 32" width="20" xmlns="http://www.w3.org/2000/svg">
            <path d=" M0 16 A8 8 0 0 1 8 8 L14 8 A8 8 0 0 1 22 16 L18 16 A4 4 0 0 0 14 12 L8 12 A4 4 0 0 0 4 16 A4 4 0 0 0 8 20 L10 24 L8 24 A8 8 0 0 1 0 16z M22 8 L24 8 A8 8 0 0 1 32 16 A8 8 0 0 1 24 24 L18 24 A8 8 0 0 1 10 16 L14 16 A4 4 0 0 0 18 20 L24 20 A4 4 0 0 0 28 16 A4 4 0 0 0 24 12z "/>
        </svg>
    </div>
    <div class="alwaysOption optionEditableContainer ml2 mr5" title="Height">
        <div class="optionEditableTitle">height</div>
        <input id="input_height" class="optionEditableInput" type="text" autocomplete="off" style="width: 55px;">
    </div>
    <!-- flip / rotate -->
    <div class="alwaysOption rotateObject clickable flexCenter ml5 mr2" data-objectRotate="-45" title="Rotate Left">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.13 16.25" height="20" width="20">
            <path d="M17.48,16.25h-8A1.65,1.65,0,0,1,7.78,14.6v-8A1.65,1.65,0,0,1,9.43,4.9h8.05a1.65,1.65,0,0,1,1.65,1.65V14.6A1.65,1.65,0,0,1,17.48,16.25Zm-8-10.1a.4.4,0,0,0-.4.4V14.6a.4.4,0,0,0,.4.4h8.05a.4.4,0,0,0,.4-.4v-8a.4.4,0,0,0-.4-.4Z"/>
            <path d="M2.55,9.29a.59.59,0,0,1-.48-.23L.14,6.69a.63.63,0,1,1,1-.79L3,8.28A.62.62,0,0,1,3,9.15.61.61,0,0,1,2.55,9.29Z"/>
            <path d="M2.55,9.29a.59.59,0,0,1-.48-.23.62.62,0,0,1,.09-.88L4.53,6.26a.63.63,0,1,1,.79,1L3,9.15A.61.61,0,0,1,2.55,9.29Z"/>
            <path d="M2.68,8.43a.62.62,0,0,1-.62-.62A7.74,7.74,0,0,1,9.72,0h0a.63.63,0,0,1,0,1.25A6.49,6.49,0,0,0,3.31,7.8a.63.63,0,0,1-.62.63Z"/>
        </svg>
    </div>
    <div class="alwaysOption rotateObject clickable flexCenter ml2 mr5" data-objectRotate="45" title="Rotate Right">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.13 16.25" height="20" width="20">
            <path d="M9.7,16.25h-8A1.65,1.65,0,0,1,0,14.6v-8A1.65,1.65,0,0,1,1.65,4.9h8a1.65,1.65,0,0,1,1.65,1.65V14.6A1.65,1.65,0,0,1,9.7,16.25Zm-8-10.1a.4.4,0,0,0-.4.4V14.6a.4.4,0,0,0,.4.4h8a.4.4,0,0,0,.4-.4v-8a.4.4,0,0,0-.4-.4Z"/>
            <path d="M16.58,9.29a.57.57,0,0,1-.39-.14.62.62,0,0,1-.1-.87L18,5.9a.63.63,0,0,1,1,.79L17.06,9.06A.59.59,0,0,1,16.58,9.29Z"/>
            <path d="M16.58,9.29a.57.57,0,0,1-.39-.14L13.81,7.23a.63.63,0,0,1,.79-1L17,8.18a.62.62,0,0,1,.09.88A.59.59,0,0,1,16.58,9.29Z"/>
            <path d="M16.45,8.43h0a.63.63,0,0,1-.62-.63A6.47,6.47,0,0,0,9.4,1.25.63.63,0,0,1,8.78.62.65.65,0,0,1,9.41,0a7.73,7.73,0,0,1,7.66,7.81A.62.62,0,0,1,16.45,8.43Z"/>
        </svg>
    </div>
    <div class="singleObjectOption optionEditableContainer mlr5" title="Rotate">
        <div class="optionEditableTitle">rotation</div>
        <input id="input_rotate" class="rotate optionEditableInput" type="text" autocomplete="off" style="width: 35px;">
    </div>
    <div class="alwaysOption flipObject clickable flexCenter ml5 mr2" data-objectFlip="flipX" title="Flip Horizontal">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16.25 16.25" height="20" width="20">
            <path d="M8.12.63H4.22a3.61,3.61,0,0,0-3.6,3.6V12a3.61,3.61,0,0,0,3.6,3.6h3.9"/>
            <path d="M9.41,1.25H8.13A.63.63,0,1,1,8.13,0H9.41a.63.63,0,1,1,0,1.25Z"/>
            <path d="M12.13,16.25a.63.63,0,0,1,0-1.25,3,3,0,0,0,2-.86.64.64,0,0,1,.89,0A.63.63,0,0,1,15,15a4.23,4.23,0,0,1-2.84,1.22Zm3.49-3.45h0a.63.63,0,0,1-.6-.65V9.47a.63.63,0,0,1,1.25,0V12.2A.64.64,0,0,1,15.62,12.8Zm0-5.41A.63.63,0,0,1,15,6.77V4.09a.63.63,0,0,1,1.25,0V6.77A.62.62,0,0,1,15.63,7.39ZM14.54,2.28a.63.63,0,0,1-.44-.17,2.92,2.92,0,0,0-2-.86.63.63,0,0,1-.61-.64A.64.64,0,0,1,12.13,0,4.24,4.24,0,0,1,15,1.21a.62.62,0,0,1-.44,1.07Z"/>
            <path d="M9.41,16.25H8.13a.63.63,0,1,1,0-1.25H9.41a.63.63,0,1,1,0,1.25Z"/>
            <path d="M9.41,16.25H4.22A4.24,4.24,0,0,1,0,12V4.23A4.24,4.24,0,0,1,4.22,0H9.41a.63.63,0,0,1,0,1.25H4.22a3,3,0,0,0-3,3V12a3,3,0,0,0,3,3H9.41a.63.63,0,0,1,0,1.25Z"/>
            <path d="M3.2,8.73a.66.66,0,0,1-.45-.18.63.63,0,0,1,0-.88L5.08,5.34A.63.63,0,1,1,6,6.22L3.64,8.55A.62.62,0,0,1,3.2,8.73Z" style="fill: white"/>
            <path d="M5.55,11.09a.62.62,0,0,1-.44-.18L2.78,8.58a.63.63,0,0,1,.89-.88L6,10a.63.63,0,0,1,0,.88A.64.64,0,0,1,5.55,11.09Z" style="fill: white"/>
        </svg>
    </div>
    <div class="alwaysOption flipObject clickable flexCenter ml2 mr5" data-objectFlip="flipY" title="Flip Vertical">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16.25 16.25" height="20" width="20">
            <path d="M15.63,10A.63.63,0,0,1,15,9.41V8.13a.63.63,0,1,1,1.25,0V9.41A.63.63,0,0,1,15.63,10Z"/>
            <path d="M12,16.25H9.48a.63.63,0,0,1,0-1.25h2.67a.62.62,0,0,1,.66.59.63.63,0,0,1-.6.66Zm-5.25,0H4.05a.62.62,0,0,1-.59-.65A.59.59,0,0,1,4.1,15H6.78a.63.63,0,1,1,0,1.25ZM1.67,15.18A.61.61,0,0,1,1.22,15,4.21,4.21,0,0,1,0,12.15a.63.63,0,0,1,.61-.65.61.61,0,0,1,.64.61,3,3,0,0,0,.86,2,.63.63,0,0,1-.44,1.07Zm12.92,0a.62.62,0,0,1-.44-.18.63.63,0,0,1,0-.88,3,3,0,0,0,.86-2,.63.63,0,1,1,1.25,0A4.24,4.24,0,0,1,15,15,.63.63,0,0,1,14.59,15.17Z"/>
            <path d="M.63,10A.63.63,0,0,1,0,9.41V8.13a.63.63,0,1,1,1.25,0V9.41A.63.63,0,0,1,.63,10Z"/>
            <path d="M15.63,10A.63.63,0,0,1,15,9.39V4.23a3,3,0,0,0-3-3H4.22a3,3,0,0,0-3,3V9.39A.63.63,0,0,1,0,9.39V4.23A4.24,4.24,0,0,1,4.22,0H12a4.24,4.24,0,0,1,4.22,4.23V9.39A.62.62,0,0,1,15.63,10Z"/>
            <path d="M15.63,8.13V4.23A3.62,3.62,0,0,0,12,.63H4.23a3.61,3.61,0,0,0-3.6,3.6v3.9"/>
            <path d="M8.11,6.18A.62.62,0,0,1,7.67,6L5.34,3.67a.63.63,0,1,1,.88-.89L8.55,5.11a.63.63,0,0,1,0,.89A.62.62,0,0,1,8.11,6.18Z" style="fill: white"/>
            <path d="M8.14,6.15A.62.62,0,0,1,7.7,6a.63.63,0,0,1,0-.89L10,2.75a.63.63,0,0,1,.88.89L8.58,6A.62.62,0,0,1,8.14,6.15Z" style="fill: white"/>
        </svg>
    </div>
    <!-- front / back -->
    <div id="button_bringToFront" class="alwaysOption stack clickable flexCenter ml5 mr2" title="Bring to Front">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15.49 16.25" height="20" width="20">
            <path d="M3.53,8.63H1.37A1.38,1.38,0,0,1,0,7.25V1.37A1.38,1.38,0,0,1,1.37,0H7.25A1.38,1.38,0,0,1,8.63,1.37V5A.63.63,0,0,1,7.38,5V1.37a.12.12,0,0,0-.13-.12H1.37a.12.12,0,0,0-.12.12V7.25a.12.12,0,0,0,.12.13H3.53a.63.63,0,1,1,0,1.25Z"/>
            <path d="M12.13,13a.62.62,0,0,1-.44-.18L2.81,3.92A.62.62,0,0,1,3.69,3l8.88,8.88a.63.63,0,0,1,0,.88A.62.62,0,0,1,12.13,13Z"/>
            <path d="M4.92,4.11H3.25a.63.63,0,0,1,0-1.25H4.92a.63.63,0,0,1,0,1.25Z"/>
            <path d="M3.25,5.77a.62.62,0,0,1-.62-.62V3.48a.62.62,0,0,1,.62-.62.63.63,0,0,1,.63.62V5.15A.63.63,0,0,1,3.25,5.77Z"/>
            <path d="M13.53,16.25H3.08A2,2,0,0,1,1.13,14.3V8A.63.63,0,0,1,2.38,8v6.3a.7.7,0,0,0,.7.7H13.53a.7.7,0,0,0,.71-.7V3.84a.71.71,0,0,0-.71-.7H8A.63.63,0,0,1,8,1.89h5.53a2,2,0,0,1,2,2V14.3A2,2,0,0,1,13.53,16.25Z"/>
        </svg>
    </div>
    <div id="button_sendToBack" class="alwaysOption stack clickable flexCenter ml2 mr5" title="Send to Back">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15.48 16.25" height="20" width="20">
            <path d="M14.11,16.25H8.23a1.37,1.37,0,0,1-1.37-1.37V11.23a.63.63,0,0,1,1.25,0v3.65a.12.12,0,0,0,.12.12h5.88a.12.12,0,0,0,.12-.12V9a.12.12,0,0,0-.12-.13H12a.63.63,0,0,1,0-1.25h2.16A1.37,1.37,0,0,1,15.48,9v5.88A1.37,1.37,0,0,1,14.11,16.25Z"/>
            <path d="M12.23,13.4a.63.63,0,0,1-.44-.19L2.91,4.33a.63.63,0,0,1,0-.88.64.64,0,0,1,.89,0l8.88,8.88a.63.63,0,0,1,0,.88A.67.67,0,0,1,12.23,13.4Z"/>
            <path d="M12.23,13.4H10.57a.63.63,0,0,1-.63-.63.62.62,0,0,1,.63-.62h1.66a.62.62,0,0,1,.63.62A.63.63,0,0,1,12.23,13.4Z"/>
            <path d="M12.23,13.4a.63.63,0,0,1-.62-.63V11.1a.63.63,0,0,1,1.25,0v1.67A.63.63,0,0,1,12.23,13.4Z"/>
            <path d="M7.48,14.36H2A2,2,0,0,1,0,12.41V2A2,2,0,0,1,2,0H12.41a2,2,0,0,1,1.95,2v6.3a.63.63,0,0,1-1.25,0V2a.71.71,0,0,0-.7-.7H2a.71.71,0,0,0-.7.7V12.41a.71.71,0,0,0,.7.7H7.48a.63.63,0,1,1,0,1.25Z"/>
        </svg>
    </div>
    <!-- fit to canvas -->
    <div id="button_fitToCanvas" class="alwaysOption clickable flexCenter mlr5" title="Fit To Artboard">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16.25 16.25" height="20" width="20">
            <path d="M15.63,16.25a.62.62,0,0,1-.44-.18l-4.78-4.73a.63.63,0,1,1,.88-.89l4.77,4.73a.62.62,0,0,1,0,.88A.59.59,0,0,1,15.63,16.25Z"/>
            <path d="M10,15.26l-.15,0a.63.63,0,0,1-.46-.75l.89-3.74a.63.63,0,0,1,.75-.47.64.64,0,0,1,.47.76l-.9,3.74A.62.62,0,0,1,10,15.26Z"/>
            <path d="M10.85,11.52a.63.63,0,0,1-.15-1.24l3.78-.88a.63.63,0,0,1,.29,1.22L11,11.5Z"/>
            <path d="M.63,16.25a.6.6,0,0,1-.45-.19.62.62,0,0,1,0-.88L5,10.45a.63.63,0,1,1,.88.89L1.06,16.07A.61.61,0,0,1,.63,16.25Z"/>
            <path d="M5.4,11.52l-.14,0-3.78-.88A.63.63,0,0,1,1.77,9.4l3.77.88a.63.63,0,0,1-.14,1.24Z"/>
            <path d="M6.29,15.26a.62.62,0,0,1-.6-.48L4.79,11a.63.63,0,0,1,.47-.76.64.64,0,0,1,.75.47l.89,3.74a.63.63,0,0,1-.46.75Z"/>
            <path d="M5.4,6A.62.62,0,0,1,5,5.8L.19,1.07A.62.62,0,1,1,1.06.18L5.84,4.91a.63.63,0,0,1,0,.89A.66.66,0,0,1,5.4,6Z"/>
            <path d="M5.4,6H5.26a.63.63,0,0,1-.47-.76l.9-3.74a.62.62,0,1,1,1.21.29L6,5.5A.62.62,0,0,1,5.4,6Z"/>
            <path d="M1.63,6.87a.63.63,0,0,1-.15-1.24l3.78-.88A.63.63,0,1,1,5.54,6l-3.77.88Z"/>
            <path d="M10.85,6a.64.64,0,0,1-.45-.18.63.63,0,0,1,0-.89L15.19.18a.62.62,0,0,1,.87.89L11.29,5.8A.62.62,0,0,1,10.85,6Z"/>
            <path d="M14.62,6.87l-.14,0L10.7,6A.63.63,0,0,1,11,4.75l3.78.88a.63.63,0,0,1-.15,1.24Z"/>
            <path d="M10.85,6a.62.62,0,0,1-.61-.48L9.35,1.76a.62.62,0,0,1,1.21-.29l.9,3.74A.64.64,0,0,1,11,6Z"/>
        </svg>
    </div>
    <!-- clone -->
    <div id="button_clone" class="singleObjectOption clickable flexCenter mlr5" title="Clone">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15 15" height="20" width="20">
            <path d="M10.84,8.12H4.16a.62.62,0,0,1-.62-.62.63.63,0,0,1,.62-.63h6.68a.63.63,0,0,1,.62.63A.62.62,0,0,1,10.84,8.12Z"/>
            <path d="M7.5,11.46a.62.62,0,0,1-.62-.62V4.16a.62.62,0,0,1,.62-.62.63.63,0,0,1,.63.62v6.68A.63.63,0,0,1,7.5,11.46Z"/>
            <path d="M11.4,1.25A2.35,2.35,0,0,1,13.75,3.6v7.8a2.35,2.35,0,0,1-2.35,2.35H3.6A2.35,2.35,0,0,1,1.25,11.4V3.6A2.35,2.35,0,0,1,3.6,1.25h7.8M11.4,0H3.6A3.61,3.61,0,0,0,0,3.6v7.8A3.61,3.61,0,0,0,3.6,15h7.8A3.61,3.61,0,0,0,15,11.4V3.6A3.61,3.61,0,0,0,11.4,0Z"/>
        </svg>
    </div>
    <!-- delete -->
    <div id="button_delete" class="alwaysOption clickable flexCenter mlr5" title="Delete">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13.83 16.25" height="20" width="20">
            <path d="M13.2,3.32H.63a.63.63,0,1,1,0-1.25H13.2a.63.63,0,1,1,0,1.25Z"/>
            <path d="M9.94,16.25H3.87a2.61,2.61,0,0,1-2.6-2.61V2.7a.63.63,0,0,1,.62-.63h10a.63.63,0,0,1,.63.63V13.64A2.61,2.61,0,0,1,9.94,16.25ZM2.52,3.32V13.64A1.36,1.36,0,0,0,3.87,15H9.94a1.36,1.36,0,0,0,1.36-1.36V3.32Z"/>
            <path d="M5.3,13.9a.63.63,0,0,1-.62-.63V5.16a.63.63,0,1,1,1.25,0v8.11A.63.63,0,0,1,5.3,13.9Z"/>
            <path d="M8.52,13.9a.63.63,0,0,1-.62-.63V5.16a.63.63,0,1,1,1.25,0v8.11A.63.63,0,0,1,8.52,13.9Z"/>
            <path d="M8.52,1.25H5.3A.63.63,0,0,1,5.3,0H8.52a.63.63,0,1,1,0,1.25Z"/>
        </svg>
    </div>
</div>
<div id="clipPanel" class="topPanel">
    <div id="startClippingContainer">
        <div id="button_startClipEllipse" class="clickable flexCenter mlr10">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50" width="20">
                <path d="M25,1C11.767,1,1,11.767,1,25s10.767,24,24,24s24-10.767,24-24S38.233,1,25,1z M25,47C12.869,47,3,37.131,3,25 S12.869,3,25,3s22,9.869,22,22S37.131,47,25,47z"/>
            </svg>
            <div class="ml5">Ellipse</div>
        </div>
        <div id="button_startClipRectangle" class="clickable flexCenter mlr10">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50" width="20">
                <path d="M1,49h48V1H1V49z M3,3h44v44H3V3z"/>
            </svg>
            <div class="ml5">Rectangle</div>
        </div>
    </div>
    <div id="finishClippingContainer">
        <div id="button_cancelClipping" title="cancel" class="clickable flexCenter mlr10">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" height="25">
                <path style="fill:#E2574C;" d="M9.4,8l1.4-1.4C11,6.4,11,6,10.8,5.8l-0.7-0.7c-0.2-0.2-0.5-0.2-0.7,0L8,6.6L6.6,5.1 C6.4,4.9,6,4.9,5.8,5.1L5.1,5.8C4.9,6,4.9,6.4,5.1,6.6L6.6,8L5.1,9.4c-0.2,0.2-0.2,0.5,0,0.7l0.7,0.7C6,11,6.4,11,6.6,10.8L8,9.4 l1.4,1.4c0.2,0.2,0.5,0.2,0.7,0l0.7-0.7c0.2-0.2,0.2-0.5,0-0.7L9.4,8z"/>
            </svg>
        </div>
        <div id="clipDimensions" class="flexCenter">
            <div class="optionEditableContainer mr2" title="Width">
                <div class="optionEditableTitle">Width</div>
                <input id="input_clipWidth" class="optionEditableInput" type="text" autocomplete="off" style="width: 50px;">
            </div>
            <div id="button_linkClipDimensions" tabindex="-1" class="linked clickable flexCenter" title="Link" onclick="jQuery(this).toggleClass('linked');">
                <svg height="20" viewBox="0 0 32 32" width="20" xmlns="http://www.w3.org/2000/svg">
                    <path d=" M0 16 A8 8 0 0 1 8 8 L14 8 A8 8 0 0 1 22 16 L18 16 A4 4 0 0 0 14 12 L8 12 A4 4 0 0 0 4 16 A4 4 0 0 0 8 20 L10 24 L8 24 A8 8 0 0 1 0 16z M22 8 L24 8 A8 8 0 0 1 32 16 A8 8 0 0 1 24 24 L18 24 A8 8 0 0 1 10 16 L14 16 A4 4 0 0 0 18 20 L24 20 A4 4 0 0 0 28 16 A4 4 0 0 0 24 12z "/>
                </svg>
            </div>
            <div class="optionEditableContainer ml2" tabindex="-1" title="Height">
                <div class="optionEditableTitle">Height</div>
                <input id="input_clipHeight" class="optionEditableInput" type="text" autocomplete="off" style="width: 50px;">
            </div>
        </div>
        <div id="button_applyClipping" title="apply" class="clickable flexCenter mlr10">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" height="25">
                <path style="fill:#1BD2A1;" d="M8.6,9.8l3.3-3.3c0.2-0.2,0.2-0.5,0-0.7l-0.7-0.7c-0.2-0.2-0.5-0.2-0.7,0L7.1,8.4L5.6,6.9 c-0.2-0.2-0.5-0.2-0.7,0L4.2,7.7C4,7.9,4,8.2,4.2,8.4l2.4,2.4c0.3,0.3,0.9,0.3,1.2,0L8.6,9.8z"></path>
            </svg>
        </div>
    </div>
</div>

<div id="layersPanel">
    <div id="previewPanel" class="flexCenter">
        <div id="previewContainer" class="flexCenter">
            <img id="previewImage" class="clickable">
        </div>
    </div>
    <div id="layersListTitle" class="flexCenter">Layers</div>
    <div id="layersScroll">
        <div id="layersList"></div>
    </div>
</div>
<div id="artboard" class="flexCenter">
    <canvas id="myCanvas"></canvas>
</div>

<script src="libs/jquery/3.4.1/jquery.min.js"></script>
<script src="libs/jquery.perfect-scrollbar/1.4.0/perfect-scrollbar.min.js"></script>
<script src="libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="libs/fabric.js/3.4.0/fabric.js"></script>
<script src="libs/fontfaceobserver/2.1.0/fontfaceobserver.js"></script>
<script src="libs/js-cookie/2.2.1/js.cookie.min.js"></script>
<script src="js/spectrum.js"></script>
<script src="js/editor.js"></script>
<?php
if ($_SESSION['isAuthenticated']) {
    echo '<script src="private/drawings.js"></script>';
}
?>

</body>
</html>