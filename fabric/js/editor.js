const HTML = {
    hiddenFilePicker: document.getElementById('hiddenFilePicker'),
    hiddenFilePicker2: document.getElementById('hiddenFilePicker2'),
    // popups
    blockOverlay: document.getElementById('blockOverlay'),
    alertOverlay: document.getElementById('alertOverlay'),
    alertTitle: document.getElementById('alertTitle'),
    alertDescription: document.getElementById('alertDescription'),
    settingsOverlay: document.getElementById('settingsOverlay'),
    input_canvasWidth: document.getElementById('input_canvasWidth'),
    input_canvasHeight: document.getElementById('input_canvasHeight'),
    spectrumTrigger_backgroundColor: document.getElementById('spectrumTrigger_backgroundColor'),
    iconfinderOverlay: document.getElementById('iconfinderOverlay'),
    iconfinderList: document.getElementById('iconfinderList'),
    iconfinderSearchQuery: document.getElementById('iconfinderSearchQuery'),
    // top menu
    button_undo: document.getElementById('button_undo'),
    button_redo: document.getElementById('button_redo'),
    zoom_info: document.getElementById('zoom_info'),
    button_toggleLayers: document.getElementById('button_toggleLayers'),
    button_settings: document.getElementById('button_settings'),
    // top panels
    objectsOptionsPanel: document.getElementById('objectsOptionsPanel'),
    clipPanel: document.getElementById('clipPanel'),
    // clip panel
    startClippingContainer: document.getElementById('startClippingContainer'),
    finishClippingContainer: document.getElementById('finishClippingContainer'),
    input_clipWidth: document.getElementById('input_clipWidth'),
    button_linkClipDimensions: document.getElementById('button_linkClipDimensions'),
    input_clipHeight: document.getElementById('input_clipHeight'),
    button_startClipEllipse: document.getElementById('button_startClipEllipse'),
    button_startClipRectangle: document.getElementById('button_startClipRectangle'),
    button_applyClipping: document.getElementById('button_applyClipping'),
    button_cancelClipping: document.getElementById('button_cancelClipping'),
    // objects options panel - IMAGE FILTERS
    button_imageFx: document.getElementById('button_imageFx'),
    button_resetImageFx: document.getElementById('button_resetImageFx'),
    imageFx_section_imageFilters: document.getElementById('imageFx_section_imageFilters'),
    imageFx_section_grayscale: document.getElementById('imageFx_section_grayscale'),
    imageFx_grayscaleSwitch: document.getElementById('imageFx_grayscaleSwitch'),
    imageFx_section_fxEffects: document.getElementById('imageFx_section_fxEffects'),
    imageFx_section_gamma: document.getElementById('imageFx_section_gamma'),
    imageFx_gammaSwitch: document.getElementById('imageFx_gammaSwitch'),
    gammaRedRange: document.getElementById('gammaRedRange'),
    gammaGreenRange: document.getElementById('gammaGreenRange'),
    gammaBlueRange: document.getElementById('gammaBlueRange'),
    // objects options panel - IMAGE CLIP
    button_clip: document.getElementById('button_clip'),
    button_clipRemove: document.getElementById('button_clipRemove'),
    // objects options panel - BORDER RADIUS
    range_rectangleBorderRadius: document.getElementById('range_rectangleBorderRadius'),
    // objects options panel - COLOR
    spectrumTrigger_singleColor: document.getElementById('spectrumTrigger_singleColor'),
    // objects options panel - FONT
    fontFamilyContainer: document.getElementById('fontFamilyContainer'),
    div_fontSelected: document.getElementById('div_fontSelected'),
    fontsContainer: document.getElementById('fontsContainer'),
    input_fontSize: document.getElementById('input_fontSize'),
    input_charSpacing: document.getElementById('input_charSpacing'),
    input_lineHeight: document.getElementById('input_lineHeight'),
    // objects options panel - OTHERS
    input_opacity: document.getElementById('input_opacity'),
    input_top: document.getElementById('input_top'),
    input_left: document.getElementById('input_left'),
    input_width: document.getElementById('input_width'),
    button_linkDimensions: document.getElementById('button_linkDimensions'),
    input_height: document.getElementById('input_height'),
    input_rotate: document.getElementById('input_rotate'),
    // layers panel
    layersPanel: document.getElementById('layersPanel'),
    previewPanel: document.getElementById('previewPanel'),
    previewImage: document.getElementById('previewImage'),
    layersList: document.getElementById('layersList'),
    layersScroll: document.getElementById('layersScroll'),
    // methods
    setDisplay: (_element, _string) => {_element.style.display = _string;},
    setValue: (_element, _value) => {_element.value = _value;},
};
let canvasWidth = 1123;
let canvasHeight = 794;
let myCanvas = new fabric.Canvas('myCanvas', {
    preserveObjectStacking: true,
    perPixelTargetFind: true,
    targetFindTolerance: 10,
    width: canvasWidth,
    height: canvasHeight,
});
let snapshots = {
    currentPosition: 0,
    data: [],
};
let clipboard;
let imageToClip;
let idsArray = [];
let isGrabMode = false;
let isGrabbing = false;
let isEyedropperMode = false;
let isClippingEllipseMode = false;
let isClippingRectangleMode = false;
let paletteColors = [
    ['#FFFFFF', '#808080', '#000000'],
    ['#99b433', '#00a300', '#1e7145', '#ff0097'],
    ['#9f00a7', '#7e3878', '#603cba', '#00aba9'],
    ['#eff4ff', '#A11BEF', '#2d89ef', '#2b5797'],
    ['#ffc40d', '#FFF931', '#e3a21a', '#da532c'],
    ['#ee1111', '#b91d47', '#66FF66', '#FF6EFF'],
    ['#955251', '#92A8D1', '#F7CAC9', '#F3E0BE'],
];
let clipZone;
let clipEllipse = new fabric.Ellipse({
    fill: 'rgba(255,255,255,0.7)',
    borderColor: 'black',
    borderDashArray: [2, 10],
    cornerStyle: 'circle',
    cornerColor: 'white',
    cornerStrokeColor: 'black',
    stroke: 'black',
    strokeWidth: 1,
    originX: 'center',
    originY: 'center',
    name: 'clipEllipse',
    hasRotatingPoint: false,
});
let clipRectangle = new fabric.Rect({
    fill: 'rgba(255,255,255,0.7)',
    borderColor: 'black',
    borderDashArray: [2, 10],
    cornerStyle: 'circle',
    cornerColor: 'white',
    cornerStrokeColor: 'black',
    stroke: 'black',
    strokeWidth: 1,
    originX: 'center',
    originY: 'center',
    name: 'clipRectangle',
    hasRotatingPoint: false,
});

let perfectScrollbar_fontsContainer = new PerfectScrollbar('#fontsContainer', {});
let perfectScrollbar_layers = new PerfectScrollbar(HTML.layersScroll, {});

let sortableLayerList = jQuery(HTML.layersList).sortable({
    axis: 'y',
    containment: HTML.layersList,
    handle: '.layerHandle',
    opacity: 0.7,
    tolerance: 'pointer',
    start: function(event, ui) {
        (ui.item[0]).querySelector('.layerHandle').style.cursor = 'grabbing';
    },
    stop: function(event, ui) {
        (ui.item[0]).querySelector('.layerHandle').style.cursor = 'grab';
    },
    update: function() {
        let newOrder = sortableLayerList.sortable('toArray');
        newOrder.forEach(id => {
            if (id !== '') {
                getObjectById(id).sendToBack();
            }
        });
        myCanvas.renderAll();
        takeSnapshot();
    },
});

function getIconfinderToken() {
    return new Promise(function(resolve, reject) {
        jQuery.getJSON('php/iconfinder.php', function(response) {
            if (response['access_token']) {
                let token = response['access_token'];
                let data = token.split('.');
                let payload = JSON.parse(atob(data[1]));
                // subtract 2 seconds to take slow reponse times into account
                let ttl = (payload['exp'] - payload['iat']) * 1000 - 2 * 1000;
                let expires_unix_time = Date.now() + ttl;
                let expires = new Date(expires_unix_time);
                Cookies.set('iconfinderToken', token, {expires: expires});
                resolve();
            } else {
                reject();
            }
        }).fail(function() {
            reject();
        });
    });
}

function updatePreviewPanel() {
    HTML.previewImage.src = myCanvas.toDataURL();
}

function exportImage() {
    let link = document.createElement('a');
    let blob;
    let actualZoom = myCanvas.getZoom() * 100;
    canvasResize(100);
    blob = dataURLtoBlob(myCanvas.toDataURL());
    link.download = 'rsc.png';
    link.href = URL.createObjectURL(blob);
    link.click();
    canvasResize(actualZoom);
    myCanvas.renderAll();
}

function canvasResize(_zoom) {
    let zoom = Math.round(_zoom);
    if (zoom <= 0) {
        return;
    }
    myCanvas.setZoom(zoom / 100);
    myCanvas.setDimensions({
        width: (canvasWidth * zoom) / 100,
        height: (canvasHeight * zoom) / 100,
    });
    HTML.zoom_info.innerHTML = zoom + '%';
}

function canvasFit() {
    while (myCanvas.wrapperEl.clientWidth > document.body.clientWidth - 500 || myCanvas.wrapperEl.clientHeight > document.body.clientHeight - 100) {
        let zoom = myCanvas.getZoom() * 100;
        let newZoom = zoom - 5;
        canvasResize(newZoom);
    }
}

function takeSnapshot() {
    if (snapshots.currentPosition !== (snapshots.data.length - 1)) {
        snapshots.data.push(snapshots.data[snapshots.currentPosition]);
    }
    snapshots.currentPosition = snapshots.data.length;
    snapshots.data.push(myCanvas.toJSON(['name']));
    updatePreviewPanel();
    HTML.button_undo.classList.remove('disabled');
    HTML.button_redo.classList.add('disabled');
    myCanvas.renderAll();
}

function updateLayersList() {
    let activeObject = myCanvas.getActiveObject();
    HTML.layersList.innerHTML = '';
    myCanvas.getObjects().forEach(object => {
        let layerItem = document.createElement('div');
        let layerHandle = document.createElement('div');
        let layerName = document.createElement('input');
        layerItem.setAttribute('class', 'layerItem');
        if (activeObject) {
            if (activeObject.type === 'activeSelection') {
                if (activeObject.contains(object)) {
                    layerItem.classList.add('active');
                }
            } else if (object.id === activeObject.id) {
                layerItem.classList.add('active');
            }
        }
        layerItem.setAttribute('id', object.id);
        layerHandle.setAttribute('class', 'layerHandle flexCenter');
        layerHandle.setAttribute('title', 'Drag vertically to reorder');
        layerName.setAttribute('class', 'layerName');
        layerName.setAttribute('type', 'text');
        layerName.value = object.name;
        layerName.setAttribute('title', 'double click to edit name');
        layerName.addEventListener('change', function() {
            let id = this.parentElement.getAttribute('id');
            let object = getObjectById(id);
            object.name = this.value;
            takeSnapshot();
        });
        layerName.addEventListener('focus', deselectAll);
        layerName.addEventListener('keydown', preventKeydownConflict);
        switch (object.type) {
            case 'line':
                layerHandle.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><line x1="4.3" y1="19.5" x2="15.7" y2="0.5" stroke="#777"></line></svg>';
                break;
            case 'circle':
            case 'triangle':
            case 'rect':
            case 'polygon':
                layerHandle.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10.39 12"><path d="M5.2,1.44,9.14,3.72V8.28L5.2,10.56,1.25,8.28V3.72l4-2.28M5.2,0,0,3V9l5.2,3,5.19-3V3L5.2,0Z"/></svg>';
                break;
            case 'image':
                layerHandle.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 15.6"><path d="M9,13a4.21,4.21,0,1,1,4.21-4.21A4.22,4.22,0,0,1,9,13ZM9,5.81a3,3,0,1,0,3,3A3,3,0,0,0,9,5.81Z"/><path d="M15.47,3.17a1.28,1.28,0,0,1,1.28,1.28v8.62a1.28,1.28,0,0,1-1.28,1.28H2.53a1.28,1.28,0,0,1-1.28-1.28V4.45A1.28,1.28,0,0,1,2.53,3.17H15.47m0-1.25H2.53A2.53,2.53,0,0,0,0,4.45v8.62A2.53,2.53,0,0,0,2.53,15.6H15.47A2.53,2.53,0,0,0,18,13.07V4.45a2.53,2.53,0,0,0-2.53-2.53Z"/><path d="M11.64,1.25H6.36A.63.63,0,1,1,6.36,0h5.28a.63.63,0,0,1,0,1.25Z"/><path d="M14.52,5.81a.69.69,0,0,1-.24,0,.86.86,0,0,1-.2-.13.77.77,0,0,1-.14-.21.59.59,0,0,1,0-.23.61.61,0,0,1,0-.24.77.77,0,0,1,.14-.21.86.86,0,0,1,.2-.13.66.66,0,0,1,.48,0,.58.58,0,0,1,.2.13A.6.6,0,0,1,15.1,5a.6.6,0,0,1,.05.24.66.66,0,0,1-.19.44.58.58,0,0,1-.2.13A.69.69,0,0,1,14.52,5.81Z"/></svg>';
                break;
            case 'text':
            case 'i-text':
            case 'textbox':
                layerHandle.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16.917 16.917"><path d="M14.191,15.817l-1.936-5.851c-0.001-0.002-0.001-0.003-0.002-0.005L9.13,0.527 c-0.003-0.008-0.01-0.014-0.012-0.022C9.092,0.434,9.051,0.373,9.009,0.313C8.992,0.289,8.982,0.26,8.963,0.238 C8.895,0.161,8.814,0.097,8.72,0.056C8.551-0.018,8.365-0.019,8.199,0.055C8.103,0.097,8.022,0.161,7.954,0.239 c-0.017,0.019-0.025,0.044-0.04,0.065C7.868,0.367,7.826,0.433,7.799,0.509C7.796,0.516,7.79,0.521,7.788,0.528L4.665,9.963 c0,0,0,0.001-0.001,0.001l-1.938,5.855c-0.137,0.417,0.049,0.885,0.413,1.044c0.083,0.036,0.17,0.054,0.259,0.054 c0.301,0,0.565-0.206,0.672-0.526l1.754-5.298h5.27l1.754,5.298c0.104,0.315,0.374,0.527,0.672,0.527 c0.088,0,0.177-0.019,0.26-0.055C14.143,16.702,14.328,16.233,14.191,15.817z M8.458,3.131l2.097,6.335H6.362L8.458,3.131z"/></svg>';
                break;
            case 'group':
                layerHandle.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 6.88 16.25"><path d="M3.44,10a.63.63,0,0,1-.63-.63V6.9a.63.63,0,1,1,1.25,0V9.35A.63.63,0,0,1,3.44,10Z"/><path d="M6.25,7.27a.63.63,0,0,1-.62-.63V3.44a2.19,2.19,0,0,0-4.38,0v3.2A.63.63,0,1,1,0,6.64V3.44a3.44,3.44,0,0,1,6.88,0v3.2A.63.63,0,0,1,6.25,7.27Z"/><path d="M3.44,16.25A3.44,3.44,0,0,1,0,12.81V9.61a.63.63,0,0,1,1.25,0v3.2a2.19,2.19,0,0,0,4.38,0V9.61a.63.63,0,0,1,1.25,0v3.2A3.45,3.45,0,0,1,3.44,16.25Z"/></svg>';
                break;
            default:
                layerHandle.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10.39 12"><path d="M5.2,1.44,9.14,3.72V8.28L5.2,10.56,1.25,8.28V3.72l4-2.28M5.2,0,0,3V9l5.2,3,5.19-3V3L5.2,0Z"/></svg>';
        }
        layerHandle.addEventListener('click', function(event) {
            let previousObject = myCanvas.getActiveObject();
            let clickedObject = getObjectById(this.parentElement.getAttribute('id'));
            let remainingObject;
            if (event.ctrlKey) {
                if (previousObject) {
                    if (clickedObject === previousObject) {
                        myCanvas.discardActiveObject().renderAll();
                    } else if (previousObject.type === 'activeSelection') {
                        if (previousObject.contains(clickedObject)) {
                            previousObject.removeWithUpdate(clickedObject);
                            if (previousObject.size() === 1) {
                                remainingObject = previousObject.getObjects()[0];
                                myCanvas.discardActiveObject().renderAll();
                                myCanvas.setActiveObject(remainingObject);
                            }
                        } else {
                            previousObject.addWithUpdate(clickedObject);
                        }
                        updateLayersList();
                        updateObjectsOptions();
                    } else {
                        myCanvas.discardActiveObject().renderAll();
                        let activeSelection = new fabric.ActiveSelection([previousObject, clickedObject], {
                            canvas: myCanvas,
                        });
                        myCanvas.setActiveObject(activeSelection);
                    }
                } else {
                    myCanvas.setActiveObject(clickedObject);
                }
            } else {
                myCanvas.setActiveObject(clickedObject);
            }
            myCanvas.renderAll();
        });
        layerItem.append(layerHandle, layerName);
        HTML.layersList.insertBefore(layerItem, HTML.layersList.firstChild);
    });
    perfectScrollbar_layers.update();
}

function updateObjectsOptions() {
    let object = myCanvas.getActiveObject();
    let options = HTML.objectsOptionsPanel.children;
    for (let option of options) {
        HTML.setDisplay(option, 'none');
    }
    HTML.input_width.value = object.getScaledWidth().toFixed(2);
    HTML.input_height.value = object.getScaledHeight().toFixed(2);
    HTML.button_linkDimensions.classList.add('linked');
    HTML.input_top.value = object.top.toFixed(2);
    HTML.input_left.value = object.left.toFixed(2);
    jQuery(HTML.objectsOptionsPanel).find('.alwaysOption').show();
    if (object.type === 'activeSelection') {
        HTML.setDisplay(HTML.input_width.parentElement, 'none');
        HTML.setDisplay(HTML.input_height.parentElement, 'none');
        HTML.setDisplay(HTML.button_linkDimensions, 'none');
        let selection = object.getObjects();
        let selectionType = selection[0].type;
        let isSameType = true;
        let isSameColor = true;
        let firstColor = selection[0].fill;
        let hasImage = false;
        let hasGroup = false;
        HTML.input_opacity.value = 100 + '%';
        jQuery(HTML.objectsOptionsPanel).find('.activeSelectionOption').show();
        for (let counter = 0; counter < selection.length; counter++) {
            if (selection[counter].type !== selectionType) {
                isSameType = false;
            }
            if (selection[counter].fill !== firstColor) {
                isSameColor = false;
            }
            if (selection[counter].type === 'image') {
                hasImage = true;
            }
            if (selection[counter].type === 'group') {
                hasGroup = true;
            }
        }
        if (isSameType) {
            switch (selectionType) {
                case 'text':
                case 'i-text':
                case 'textbox':
                    let isSameFontFamily = true;
                    let firstFontFamily = selection[0].fontFamily;
                    for (let counter = 0; counter < selection.length; counter++) {
                        if (selection[counter].fontFamily !== firstFontFamily) {
                            isSameFontFamily = false;
                        }
                    }
                    HTML.div_fontSelected.innerHTML = isSameFontFamily ? firstFontFamily : '';
                    HTML.input_fontSize.value = '';
                    HTML.input_lineHeight.value = '';
                    HTML.input_charSpacing.value = '';
                    jQuery(HTML.objectsOptionsPanel).find('.textOption').show();
                    HTML.setDisplay(HTML.input_height.parentElement, 'none');
                    break;
            }
        }
        if (!hasGroup && !hasImage) {
            HTML.setDisplay(HTML.spectrumTrigger_singleColor, 'flex');
            HTML.spectrumTrigger_singleColor.style.background = isSameColor ? firstColor : 'transparent';
        }
    } else if (object.type !== 'activeSelection') {
        HTML.input_opacity.value = (Math.round(object.opacity * 100)) + '%';
        HTML.input_rotate.value = (object.angle).toFixed() + 'º';
        jQuery(HTML.objectsOptionsPanel).find('.singleObjectOption').show();
        // specific options:
        switch (object.type) {
            case 'image':
                jQuery(HTML.objectsOptionsPanel).find('.imageOption').show();
                if (object.filters.length) {
                    HTML.button_imageFx.classList.add('hasActiveFilter');
                } else {
                    HTML.button_imageFx.classList.remove('hasActiveFilter');
                }
                if (object.clipPath) {
                    HTML.button_clip.classList.add('hasClip');
                } else {
                    HTML.button_clip.classList.remove('hasClip');
                }
                HTML.setDisplay(HTML.spectrumTrigger_singleColor, 'none');
                if (object.width > 4000 || object.height > 4000) {
                    HTML.setDisplay(HTML.button_imageFx, 'none');
                } else {
                    let filters = myCanvas.getActiveObject().filters;
                    emptyImageFiltersDropdown();
                    if (filters.length) {
                        fillImageFiltersDropdown(filters);
                        HTML.setDisplay(HTML.button_resetImageFx, 'flex');
                    } else {
                        HTML.setDisplay(HTML.button_resetImageFx, 'none');
                    }
                }
                break;
            case 'line':
            case 'polygon':
            case 'rect':
            case 'circle':
            case 'triangle':
                if (object.fill.colorStops) {
                    HTML.spectrumTrigger_singleColor.style.background = getCssGradientFromObject('fill');
                } else {
                    HTML.spectrumTrigger_singleColor.style.background = object.fill;
                }
                jQuery(HTML.objectsOptionsPanel).find('.shapeOption').show();
                if (object.type === 'rect') {
                    let max;
                    let value;
                    if (object.width < object.height) {
                        max = object.width / 2;
                        value = object.rx;
                    } else {
                        max = object.height / 2;
                        value = object.ry;
                    }
                    HTML.range_rectangleBorderRadius.setAttribute('max', max);
                    HTML.range_rectangleBorderRadius.setAttribute('value', value);
                    jQuery(HTML.objectsOptionsPanel).find('.rectOption').show();
                }
                break;
            case 'text':
            case 'i-text':
            case 'textbox':
                if (object.fill.colorStops) {
                    HTML.spectrumTrigger_singleColor.style.background = getCssGradientFromObject('fill');
                } else {
                    HTML.spectrumTrigger_singleColor.style.background = object.fill;
                }
                HTML.div_fontSelected.innerHTML = object.fontFamily;
                HTML.input_fontSize.value = Number(object.fontSize).toFixed(1);
                HTML.input_lineHeight.value = object.lineHeight.toFixed(2);
                HTML.input_charSpacing.value = object.charSpacing.toFixed();
                HTML.objectsOptionsPanel.querySelectorAll('.alignText').forEach(element => {
                    if (element.getAttribute('data-textalign') === object.textAlign) {
                        element.classList.add('active');
                    } else {
                        element.classList.remove('active');
                    }
                });
                jQuery(HTML.objectsOptionsPanel).find('.textOption').show();
                HTML.setDisplay(HTML.input_width.parentElement, 'none');
                HTML.setDisplay(HTML.input_height.parentElement, 'none');
                HTML.setDisplay(HTML.button_linkDimensions, 'none');
                break;
            case 'path':
                if (object.fill.colorStops) {
                    HTML.spectrumTrigger_singleColor.style.background = getCssGradientFromObject('fill');
                } else {
                    HTML.spectrumTrigger_singleColor.style.background = object.fill;
                }
                jQuery(HTML.objectsOptionsPanel).find('.pathOption').show();
                break;
            case 'group':
                HTML.setDisplay(HTML.spectrumTrigger_singleColor, 'none');
                jQuery(HTML.objectsOptionsPanel).find('.groupOption').show();
                break;
        }
    }
}

function deselectAll() {
    myCanvas.discardActiveObject().renderAll();
}

function allowObjectSelection(_boolean) {
    myCanvas.forEachObject(object => {
        object.selectable = _boolean;
        object.hoverCursor = _boolean ? 'move' : 'default';
    });
}

function getObjectById(_id) {
    let array = myCanvas.getObjects();
    for (let counter = 0; counter < array.length; counter++) {
        if (array[counter].id === _id) {
            return array[counter];
        }
    }
}

function removeObject() {
    let activeObject = myCanvas.getActiveObject();
    if (activeObject) {
        if (activeObject.type === 'activeSelection') {
            activeObject.forEachObject(object => {
                myCanvas.remove(object);
            });
            myCanvas.discardActiveObject().renderAll();
        } else {
            myCanvas.remove(activeObject);
        }
        updateLayersList();
        takeSnapshot();
    } else {
        popupAlert('No object selected', 'Please, select a object to perform this action');
    }
}

function copyObject() {
    let activeObject = myCanvas.getActiveObject();
    if (activeObject) {
        if (activeObject.type === 'activeSelection') {
            popupAlert('Unable to copy', 'This is a selection of objects.');
        } else {
            activeObject.clone(function(cloned) {
                clipboard = cloned;
            });
        }
    } else {
        popupAlert('No object selected', 'Please, select a object to perform this action');
    }
}

function pasteObject() {
    if (clipboard) {
        clipboard.clone(function(cloned) {
            cloned.name = 'copy';
            myCanvas.add(cloned).viewportCenterObject(cloned).setActiveObject(cloned);
            updateLayersList();
            takeSnapshot();
        });
    } else {
        popupAlert('No object in clipboard', 'Please, copy a object to perform this action');
    }
}

function cloneObject() {
    let activeObject = myCanvas.getActiveObject();
    if (activeObject) {
        if (activeObject.type === 'activeSelection') {
            popupAlert('Unable to clone', 'This a selection of objects.');
        } else {
            activeObject.clone(function(cloned) {
                cloned.top += 10;
                cloned.left += 10;
                cloned.name = 'clone';
                myCanvas.add(cloned).setActiveObject(cloned);
                updateLayersList();
                takeSnapshot();
            });
        }
    } else {
        popupAlert('No object selected', 'Please, select a object to perform this action');
    }
}

function sendObjectToBack() {
    let activeObject = myCanvas.getActiveObject();
    if (activeObject) {
        if (activeObject.type === 'activeSelection') {
            let objectsOrdered = [];
            myCanvas.forEachObject(object => {
                if (activeObject.contains(object)) {
                    objectsOrdered.push(object);
                }
            });
            myCanvas.discardActiveObject();
            let fixedSelection = new fabric.ActiveSelection(objectsOrdered, {
                canvas: myCanvas,
            });
            myCanvas.setActiveObject(fixedSelection);
            myCanvas.getActiveObject().sendToBack();
        } else {
            activeObject.sendToBack();
        }
        myCanvas.renderAll();
        updateLayersList();
        takeSnapshot();
    } else {
        popupAlert('No object selected', 'Please, select a object to perform this action');
    }
}

function bringObjectToFront() {
    let activeObject = myCanvas.getActiveObject();
    if (activeObject) {
        if (activeObject.type === 'activeSelection') {
            let objectsOrdered = [];
            myCanvas.forEachObject(object => {
                if (activeObject.contains(object)) {
                    objectsOrdered.push(object);
                }
            });
            myCanvas.discardActiveObject();
            let fixedSelection = new fabric.ActiveSelection(objectsOrdered, {
                canvas: myCanvas,
            });
            myCanvas.setActiveObject(fixedSelection);
            myCanvas.getActiveObject().bringToFront();
        } else {
            activeObject.bringToFront();
        }
        myCanvas.renderAll();
        updateLayersList();
        takeSnapshot();
    } else {
        popupAlert('No object selected', 'Please, select a object to perform this action');
    }
}

function objectOpacity(_value) {
    let activeObject = myCanvas.getActiveObject();
    if (activeObject.type === 'activeSelection') {
        activeObject.forEachObject(object => {
            object.set('opacity', _value / 100);
        });
    } else {
        activeObject.set('opacity', _value / 100);
    }
    HTML.input_opacity.value = _value + '%';
    myCanvas.renderAll();
    takeSnapshot();
}

function objectAlignTo(_string) {
    let activeObject = myCanvas.getActiveObject();
    let boundingRect = activeObject.getBoundingRect(true, false);
    switch (_string) {
        case 'left':
            activeObject.setPositionByOrigin({
                x: (boundingRect.width / 2),
                y: (boundingRect.top + (boundingRect.height / 2)),
            }, 'center', 'center');
            break;
        case 'right':
            activeObject.setPositionByOrigin({
                x: ((myCanvas.getWidth() / myCanvas.getZoom()) - (boundingRect.width / 2)),
                y: (boundingRect.top + (boundingRect.height / 2)),
            }, 'center', 'center');
            break;
        case 'top':
            activeObject.setPositionByOrigin({
                x: (boundingRect.left + (boundingRect.width / 2)),
                y: (boundingRect.height / 2),
            }, 'center', 'center');
            break;
        case 'bottom':
            activeObject.setPositionByOrigin({
                x: (boundingRect.left + (boundingRect.width / 2)),
                y: ((myCanvas.getHeight() / myCanvas.getZoom()) - (boundingRect.height / 2)),
            }, 'center', 'center');
            break;
        case 'centerH':
            myCanvas.viewportCenterObjectH(activeObject);
            break;
        case 'centerV':
            myCanvas.viewportCenterObjectV(activeObject);
            break;
    }
    activeObject.setCoords();
    myCanvas.renderAll();
    HTML.input_top.value = activeObject.top.toFixed(2);
    HTML.input_left.value = activeObject.left.toFixed(2);
    takeSnapshot();
}

function objectFlip(_string) {
    let activeObject = myCanvas.getActiveObject();
    if (activeObject) {
        activeObject.toggle(_string);
        myCanvas.renderAll();
        takeSnapshot();
    } else {
        popupAlert('No object selected', 'Please, select a object to perform this action');
    }
}

function objectAbsoluteRotation(_angle) {
    let activeObject = myCanvas.getActiveObject();
    activeObject.rotate(Number(_angle));
    activeObject.setCoords();
    myCanvas.renderAll();
    HTML.input_top.value = activeObject.top.toFixed(2);
    HTML.input_left.value = activeObject.left.toFixed(2);
    HTML.setValue(HTML.input_rotate, (activeObject.angle).toFixed() + 'º');
    takeSnapshot();
}

function objectRelativeRotation(_value) {
    let newAngle = myCanvas.getActiveObject().angle + Number(_value);
    if (newAngle < 0) {
        newAngle += 360;
    } else if (newAngle > 360) {
        newAngle -= 360;
    } else if (newAngle === 360) {
        newAngle = 0;
    }
    objectAbsoluteRotation(newAngle);
    HTML.input_rotate.value = newAngle.toFixed() + 'º';
}

function updateImageFilters() {
    myCanvas.getActiveObject().filters = [];
    HTML.imageFx_section_imageFilters.querySelectorAll('.imageFx_option').forEach(element => {
        if (element.classList.contains('checked')) {
            switch (element.getAttribute('data-filter-name')) {
                case 'Invert':
                    myCanvas.getActiveObject().filters.push(new fabric.Image.filters.Invert());
                    break;
                case 'Sepia':
                    myCanvas.getActiveObject().filters.push(new fabric.Image.filters.Sepia());
                    break;
                case 'BlackWhite':
                    myCanvas.getActiveObject().filters.push(new fabric.Image.filters.BlackWhite());
                    break;
                case 'Brownie':
                    myCanvas.getActiveObject().filters.push(new fabric.Image.filters.Brownie());
                    break;
                case 'Vintage':
                    myCanvas.getActiveObject().filters.push(new fabric.Image.filters.Vintage());
                    break;
                case 'Kodachrome':
                    myCanvas.getActiveObject().filters.push(new fabric.Image.filters.Kodachrome());
                    break;
                case 'Technicolor':
                    myCanvas.getActiveObject().filters.push(new fabric.Image.filters.Technicolor());
                    break;
                case 'Polaroid':
                    myCanvas.getActiveObject().filters.push(new fabric.Image.filters.Polaroid());
                    break;
            }
        }
    });
    if (HTML.imageFx_grayscaleSwitch.classList.contains('active')) {
        myCanvas.getActiveObject().filters.push(new fabric.Image.filters.Grayscale({
            mode: HTML.imageFx_section_grayscale.querySelector('.checked').getAttribute('data-filter-mode'),
        }));
    }
    HTML.imageFx_section_fxEffects.querySelectorAll('.imageFx_option').forEach(element => {
        if (element.classList.contains('checked')) {
            switch (element.getAttribute('data-filter-name')) {
                case 'Blur':
                    myCanvas.getActiveObject().filters.push(new fabric.Image.filters.Blur({
                        blur: parseFloat(element.querySelector('.inputRange').value),
                    }));
                    break;
                case 'Pixelate':
                    myCanvas.getActiveObject().filters.push(new fabric.Image.filters.Pixelate({
                        blocksize: parseInt(element.querySelector('.inputRange').value),
                    }));
                    break;
                case 'Noise':
                    myCanvas.getActiveObject().filters.push(new fabric.Image.filters.Noise({
                        noise: parseInt(element.querySelector('.inputRange').value),
                    }));
                    break;
                case 'HueRotation':
                    myCanvas.getActiveObject().filters.push(new fabric.Image.filters.HueRotation({
                        rotation: parseFloat(element.querySelector('.inputRange').value),
                    }));
                    break;
                case 'Saturation':
                    myCanvas.getActiveObject().filters.push(new fabric.Image.filters.Saturation({
                        saturation: parseFloat(element.querySelector('.inputRange').value),
                    }));
                    break;
                case 'Contrast':
                    myCanvas.getActiveObject().filters.push(new fabric.Image.filters.Contrast({
                        contrast: parseFloat(element.querySelector('.inputRange').value),
                    }));
                    break;
                case 'Brightness':
                    myCanvas.getActiveObject().filters.push(new fabric.Image.filters.Brightness({
                        brightness: parseFloat(element.querySelector('.inputRange').value),
                    }));
                    break;
            }
        }
    });
    if (HTML.imageFx_gammaSwitch.classList.contains('active')) {
        myCanvas.getActiveObject().filters.push(new fabric.Image.filters.Gamma({
            gamma: [
                parseFloat(HTML.gammaRedRange.value),
                parseFloat(HTML.gammaGreenRange.value),
                parseFloat(HTML.gammaBlueRange.value),
            ],
        }));
    }
    myCanvas.getActiveObject().applyFilters();
    myCanvas.renderAll();
    if (myCanvas.getActiveObject().filters.length) {
        HTML.button_imageFx.classList.add('hasActiveFilter');
        HTML.setDisplay(HTML.button_resetImageFx, 'flex');
    } else {
        HTML.button_imageFx.classList.remove('hasActiveFilter');
        HTML.setDisplay(HTML.button_resetImageFx, 'none');
    }
}

function emptyImageFiltersDropdown() {
    HTML.imageFx_section_imageFilters.querySelectorAll('.imageFx_option').forEach(checkbox => {
        checkbox.classList.remove('checked');
    });
    HTML.imageFx_grayscaleSwitch.classList.remove('active');
    HTML.imageFx_section_grayscale.querySelectorAll('.imageFx_radioContainer').forEach(radio => {
        radio.classList.remove('checked');
    });
    HTML.imageFx_section_fxEffects.querySelectorAll('.imageFx_option').forEach(checkbox => {
        checkbox.classList.remove('checked');
        if (checkbox.querySelector('.imageFx_type').innerHTML === 'Pixelate') {
            HTML.setValue(checkbox.querySelector('.inputValue'), '');
            HTML.setValue(checkbox.querySelector('.inputRange'), 1);
        } else {
            HTML.setValue(checkbox.querySelector('.inputValue'), '');
            HTML.setValue(checkbox.querySelector('.inputRange'), 0);
        }
    });
    HTML.imageFx_gammaSwitch.classList.remove('active');
}

function fillImageFiltersDropdown(_filters) {
    _filters.forEach(filter => {
        switch (filter.type) {
            case 'Gamma':
                HTML.imageFx_gammaSwitch.classList.add('active');
                HTML.imageFx_section_gamma.querySelectorAll('.imageFx_option').forEach(channel => {
                    channel.classList.add('active');
                    switch (channel.getAttribute('data-filter-channel')) {
                        case 'red':
                            HTML.setValue(channel.querySelector('.inputValue'), filter.gamma[0].toString());
                            HTML.setValue(channel.querySelector('.inputRange'), filter.gamma[0]);
                            break;
                        case 'green':
                            HTML.setValue(channel.querySelector('.inputValue'), filter.gamma[1].toString());
                            HTML.setValue(channel.querySelector('.inputRange'), filter.gamma[1]);
                            break;
                        case 'blue':
                            HTML.setValue(channel.querySelector('.inputValue'), filter.gamma[2].toString());
                            HTML.setValue(channel.querySelector('.inputRange'), filter.gamma[2]);
                            break;
                    }
                });
                break;
            case 'Grayscale':
                HTML.imageFx_grayscaleSwitch.classList.add('active');
                HTML.imageFx_section_grayscale.querySelectorAll('.imageFx_radioContainer').forEach(radio => {
                    if (radio.getAttribute('data-filter-mode') === filter.mode) {
                        radio.classList.add('checked');
                    }
                });
                break;
            case 'Blur':
            case 'Pixelate':
            case 'Noise':
            case 'HueRotation':
            case 'Saturation':
            case 'Contrast':
            case 'Brightness':
                HTML.imageFx_section_fxEffects.querySelectorAll('.imageFx_checkboxContainer').forEach(checkbox => {
                    if (checkbox.getAttribute('data-filter-name') === filter.type) {
                        checkbox.classList.add('checked');
                        HTML.setValue(checkbox.querySelector('.inputValue'), filter[filter.mainParameter].toString());
                        HTML.setValue(checkbox.querySelector('.inputRange'), filter[filter.mainParameter]);
                    }
                });
                break;
            default:
                HTML.imageFx_section_imageFilters.querySelectorAll('.imageFx_checkboxContainer').forEach(checkbox => {
                    if (checkbox.getAttribute('data-filter-name') === filter.type) {
                        checkbox.classList.add('checked');
                    }
                });
        }
    });
}

function fontSelection() {
    let fontSelected = this.getAttribute('data-family');
    let activeObject = myCanvas.getActiveObject();
    let myFont = new FontFaceObserver(fontSelected);
    myFont.load().then(function() {
        if (activeObject.type === 'activeSelection') {
            activeObject.forEachObject(object => {
                object.set('fontFamily', fontSelected);
            });
        } else {
            activeObject.set('fontFamily', fontSelected);
        }
        HTML.div_fontSelected.innerHTML = fontSelected;
        myCanvas.renderAll();
        takeSnapshot();
    });
    HTML.fontFamilyContainer.click();
}

function startEyedropperMode() {
    isEyedropperMode = true;
    allowObjectSelection(false);
    myCanvas.upperCanvasEl.classList.add('eyeDropping');
}

function endEyedropperMode() {
    isEyedropperMode = false;
    allowObjectSelection(true);
    myCanvas.upperCanvasEl.classList.remove('eyeDropping');
}

function startClippingMode(_action) {
    HTML.setDisplay(HTML.startClippingContainer, 'none');
    HTML.setDisplay(HTML.finishClippingContainer, 'flex');
    myCanvas.selection = false;
    allowObjectSelection(false);
    if (HTML.button_toggleLayers.classList.contains('active')) {
        HTML.button_toggleLayers.click();
    }
    imageToClip = myCanvas.getActiveObject();
    if (imageToClip.clipPath) {
        imageToClip.clipPath = null;
        myCanvas.renderAll();
    }
    switch (_action) {
        case 'clipEllipse':
            isClippingEllipseMode = true;
            clipZone = clipEllipse;
            let radius = imageToClip.width > imageToClip.height ? (imageToClip.height * 0.8 / 2) : (imageToClip.width * 0.8 / 2);
            clipZone.set({
                angle: imageToClip.angle,
                top: imageToClip.getCenterPoint().y,
                left: imageToClip.getCenterPoint().x,
                rx: radius,
                ry: radius,
                scaleX: imageToClip.scaleX,
                scaleY: imageToClip.scaleY,
            });
            break;
        case 'clipRectangle':
            isClippingRectangleMode = true;
            clipZone = clipRectangle;
            let minorSide = imageToClip.width > imageToClip.height ? (imageToClip.height * 0.8) : (imageToClip.width * 0.8);
            clipZone.set({
                angle: imageToClip.angle,
                top: imageToClip.getCenterPoint().y,
                left: imageToClip.getCenterPoint().x,
                width: minorSide,
                height: minorSide,
                scaleX: imageToClip.scaleX,
                scaleY: imageToClip.scaleY,
            });
            break;
    }
    HTML.input_clipWidth.value = clipZone.getScaledWidth().toFixed();
    HTML.input_clipHeight.value = clipZone.getScaledHeight().toFixed();
    myCanvas.add(clipZone).setActiveObject(clipZone).bringToFront(clipZone).renderAll();
    HTML.objectsOptionsPanel.classList.remove('active');
}

function endClippingMode(_action) {
    let clipZoneCenterCoords = clipZone.getCenterPoint();
    let imageToClipCenterCoords = imageToClip.getCenterPoint();
    let imageToClipCenterLocalPointer = imageToClip.getLocalPointer(null, {
        x: imageToClipCenterCoords.x,
        y: imageToClipCenterCoords.y,
    });
    let imageToClipTargetLocalPointer = imageToClip.getLocalPointer(null, {
        x: clipZoneCenterCoords.x,
        y: clipZoneCenterCoords.y,
    });
    if (_action === 'apply') {
        if (isClippingEllipseMode) {
            isClippingEllipseMode = false;
            imageToClip.clipPath = new fabric.Ellipse({
                angle: clipZone.angle - imageToClip.angle,
                scaleX: clipZone.scaleX / imageToClip.scaleX,
                scaleY: clipZone.scaleY / imageToClip.scaleY,
                top: (imageToClipTargetLocalPointer.y - imageToClipCenterLocalPointer.y) / imageToClip.scaleY,
                left: (imageToClipTargetLocalPointer.x - imageToClipCenterLocalPointer.x) / imageToClip.scaleX,
                rx: clipZone.rx,
                ry: clipZone.ry,
                originX: 'center',
                originY: 'center',
            });
        } else if (isClippingRectangleMode) {
            isClippingRectangleMode = false;
            imageToClip.clipPath = new fabric.Rect({
                angle: clipZone.angle - imageToClip.angle,
                scaleX: clipZone.scaleX / imageToClip.scaleX,
                scaleY: clipZone.scaleY / imageToClip.scaleY,
                top: (imageToClipTargetLocalPointer.y - imageToClipCenterLocalPointer.y) / imageToClip.scaleY,
                left: (imageToClipTargetLocalPointer.x - imageToClipCenterLocalPointer.x) / imageToClip.scaleX,
                width: clipZone.width,
                height: clipZone.height,
                originX: 'center',
                originY: 'center',
            });
        }
    } else if (_action === 'cancel') {
        isClippingEllipseMode = false;
        isClippingRectangleMode = false;
    }
    myCanvas.selection = true;
    allowObjectSelection(true);
    myCanvas.remove(clipZone).setActiveObject(imageToClip).renderAll();
    if (_action === 'apply') {
        takeSnapshot();
    }
    HTML.clipPanel.classList.remove('active');
}

function toggleEyedropperMode() {
    isEyedropperMode ? endEyedropperMode() : startEyedropperMode();
    return isEyedropperMode;
}

function spectrumToClipboard(_trigger) {
    let input = _trigger.parentElement.querySelector('.sp-input');
    input.select();
    document.execCommand('copy');
}

function toggleSpectrumType(_trigger) {
    let spectrumContainer = _trigger.closest('.sp-container');
    spectrumContainer.querySelectorAll('.type-option').forEach(option => {
        if (option === _trigger) {
            option.classList.add('active');
        } else {
            option.classList.remove('active');
        }
    });
    switch (_trigger.innerHTML) {
        case 'Solid':
            spectrumContainer.querySelector('.sp-button-container').style.display = 'flex';
            spectrumContainer.querySelector('.sp-palette').style.display = 'block';
            spectrumContainer.querySelector('.gradient-container').style.display = 'none';
            spectrumContainer.querySelector('.gradients-palette').style.display = 'none';
            break;
        case 'Gradient':
            if (isEyedropperMode) {
                spectrumContainer.querySelector('.eyedropperContainer').click();
            }
            spectrumContainer.querySelector('.sp-button-container').style.display = 'none';
            spectrumContainer.querySelector('.sp-palette').style.display = 'none';
            spectrumContainer.querySelector('.gradient-container').style.display = 'flex';
            spectrumContainer.querySelector('.gradients-palette').style.display = 'flex';
            break;
    }
}

function spectrumGradient(_type, _colorStops, _degrees) {
    let radians = fabric.util.degreesToRadians(_degrees);
    let start;
    let finish;
    if (_type === 'background') {
        let colorStopsToArray = [];
        let canvasCenterPoint = new fabric.Point(myCanvas.getWidth() / myCanvas.getZoom() / 2, myCanvas.getHeight() / myCanvas.getZoom() / 2);
        start = fabric.util.rotatePoint(new fabric.Point(0, myCanvas.getHeight() / myCanvas.getZoom() / 2), canvasCenterPoint, radians);
        finish = fabric.util.rotatePoint(new fabric.Point(myCanvas.getWidth() / myCanvas.getZoom(), (myCanvas.getHeight() / myCanvas.getZoom()) / 2), canvasCenterPoint, radians);
        Object.entries(_colorStops).forEach(([key, value], index) => {
            colorStopsToArray.push({
                offset: key,
                color: value,
            });
        });
        let gradient = new fabric.Gradient({
            type: 'linear',
            coords: {
                x1: start.x,
                y1: start.y,
                x2: finish.x,
                y2: finish.y,
            },
            colorStops: colorStopsToArray,
        });
        myCanvas.backgroundColor = gradient;
        myCanvas.backgroundImage = false;
        //
        let arcTangent = Math.atan2((start.y - finish.y), (start.x - finish.x));
        let degrees = fabric.util.radiansToDegrees(Math.PI - arcTangent);
        if (degrees < 0) {
            degrees = -degrees;
        } else if (degrees > 0) {
            degrees = 360 - degrees;
        }
        let linearGradientCss = 'linear-gradient(' + (Math.round(degrees) + 90) + 'deg, ';
        let dataGradient = Math.round(degrees);
        let orderedStops = colorStopsToArray.sort((a, b) => {
            return (a.offset > b.offset) ? 1 : ((b.offset > a.offset) ? -1 : 0);
        });
        for (let counter = 0; counter < orderedStops.length; counter++) {
            if (counter < orderedStops.length - 1) {
                linearGradientCss += orderedStops[counter].color + ' ' + (orderedStops[counter].offset * 100) + '%, ';
            } else {
                linearGradientCss += orderedStops[counter].color + ' ' + (orderedStops[counter].offset * 100) + '%';
            }
            dataGradient += '/' + orderedStops[counter].offset + ':' + orderedStops[counter].color;
        }
        linearGradientCss += ')';
        HTML.spectrumTrigger_backgroundColor.style.background = linearGradientCss;
    } else {
        let object = myCanvas.getActiveObject();
        let objectCenterPoint = new fabric.Point(object.width / 2, object.height / 2);
        start = fabric.util.rotatePoint(new fabric.Point(0, object.height / 2), objectCenterPoint, radians);
        finish = fabric.util.rotatePoint(new fabric.Point(object.width, object.height / 2), objectCenterPoint, radians);
        object.setGradient(_type, {
            type: 'linear',
            x1: start.x,
            y1: start.y,
            x2: finish.x,
            y2: finish.y,
            colorStops: _colorStops,
        });
    }
    myCanvas.renderAll();
    takeSnapshot();
    if (_type !== 'background') {
        updateObjectsOptions();
    }
}

function getGradientAngle(_type) {
    let gradientLine;
    switch (_type) {
        case 'fill':
            gradientLine = myCanvas.getActiveObject().fill.coords;
            break;
        case 'stroke':
            gradientLine = myCanvas.getActiveObject().stroke.coords;
            break;
        case 'background':
            gradientLine = myCanvas.backgroundColor.coords;
            break;
    }
    let arcTangent = Math.atan2((gradientLine.y1 - gradientLine.y2), (gradientLine.x1 - gradientLine.x2));
    let degrees = fabric.util.radiansToDegrees(Math.PI - arcTangent);
    if (degrees < 0) {
        degrees = -degrees;
    } else if (degrees > 0) {
        degrees = 360 - degrees;
    }
    return Math.round(degrees);
}

function setColorpickerAngleInfo(_container, _degrees) {
    let angleRow = _container.querySelector('.angle-row');
    let angleHandler = _container.querySelector('.angle-handler');
    angleHandler.style.left = (_degrees / (360 / angleRow.clientWidth)) - (angleHandler.clientWidth / 2) + 'px';
    HTML.setValue(_container.querySelector('.angle-value'), _degrees);
    _container.querySelector('.angle-svg').style.transform = 'rotate(' + _degrees + 'deg)';
}

function createGradientHandler(_colorpickerId, _gradientRow, _color, _offset) {
    let newDiv = document.createElement('div');
    newDiv.setAttribute('class', 'gradient-stop');
    newDiv.setAttribute('data-colorpicker', _colorpickerId);
    newDiv.style.background = _color;
    newDiv.addEventListener('dblclick', function(event) {
        event.stopPropagation();
        let colorpicker = jQuery(event.target.getAttribute('data-colorpicker'));
        let gradientRow = this.parentElement;
        if (gradientRow.children.length > 2) {
            this.remove();
            colorpicker.spectrum('updateGradientRow');
        }
    });
    _gradientRow.appendChild(newDiv);
    newDiv.style.left = (jQuery(_gradientRow).width() * _offset) - (jQuery(newDiv).width() / 2) + 'px';
}

function getCssGradientFromObject(_type) {
    let object = myCanvas.getActiveObject();
    let linearGradient = 'linear-gradient(' + (getGradientAngle(_type) + 90) + 'deg, ';
    let colorStops =
        (_type === 'fill') ? object.fill.colorStops :
            (_type === 'stroke') ? object.stroke.colorStops :
                (_type === 'background') ? myCanvas.backgroundColor.colorStops : null;
    let orderedStops = colorStops.sort((a, b) => {
        return (a.offset > b.offset) ? 1 : ((b.offset > a.offset) ? -1 : 0);
    });
    for (let counter = 0; counter < orderedStops.length; counter++) {
        if (counter < orderedStops.length - 1) {
            linearGradient += orderedStops[counter].color + ' ' + (orderedStops[counter].offset * 100) + '%, ';
        } else {
            linearGradient += orderedStops[counter].color + ' ' + (orderedStops[counter].offset * 100) + '%';
        }
    }
    linearGradient += ')';
    return linearGradient;
}

function changeCharSpacing(_value) {
    let activeObject = myCanvas.getActiveObject();
    if (activeObject.type === 'activeSelection') {
        activeObject.forEachObject(object => {
            object.charSpacing = _value;
        });
    } else {
        activeObject.charSpacing = _value;
    }
    HTML.input_charSpacing.value = _value.toString();
}

function changeLineHeight(_value) {
    let activeObject = myCanvas.getActiveObject();
    if (activeObject.type === 'activeSelection') {
        activeObject.forEachObject(object => {
            object.lineHeight = _value;
        });
    } else {
        activeObject.lineHeight = _value;
    }
    HTML.input_lineHeight.value = _value.toFixed(2);
}

function alignTextOption() {
    let activeObject = myCanvas.getActiveObject();
    let newValue = this.getAttribute('data-textAlign');
    if (activeObject.type === 'activeSelection') {
        activeObject.forEachObject(object => {
            object.textAlign = newValue;
        });
    } else {
        activeObject.textAlign = newValue;
    }
    myCanvas.renderAll();
    updateObjectsOptions();
    takeSnapshot();
}

function scrollToLayer(_id) {
    let selector = '#' + _id;
    let layer = HTML.layersList.querySelector(selector);
    let layerPosition = jQuery(layer).position().top;
    let scrollPosition = HTML.layersScroll.scrollTop;
    if (layerPosition < 0 || layerPosition > HTML.layersScroll.clientHeight - layer.clientHeight) {
        jQuery(HTML.layersScroll).animate({
            scrollTop: scrollPosition + layerPosition,
        });
    }
}

function highlightInputContent() {
    this.select();
}

function preventKeydownConflict(event) {
    event.stopPropagation();
}

function dataURLtoBlob(dataurl) {
    let parts = dataurl.split(',');
    let mime = parts[0].match(/:(.*?);/)[1];
    if (parts[0].indexOf('base64') !== -1) {
        let bstr = atob(parts[1]), n = bstr.length, u8arr = new Uint8Array(n);
        while (n--) {
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new Blob([u8arr], {type: mime});
    } else {
        let raw = decodeURIComponent(parts[1]);
        return new Blob([raw], {type: mime});
    }
}

function getRandomString() {
    let charSet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let setLength = charSet.length;
    let string = '';
    for (let counter = 0; counter < 6; counter++) {
        string += charSet.charAt(Math.floor(Math.random() * setLength));
    }
    return string;
}

function getUniqueId() {
    let id = getRandomString();
    while (idsArray.includes(id)) {
        id = getRandomString();
    }
    idsArray.push(id);
    return id;
}

function getIconfinderItems() {
    let query = (HTML.iconfinderSearchQuery.value === '') ? 'animal' : HTML.iconfinderSearchQuery.value;
    let parameters = [
        'query=' + query,
        'count=20',
        'offset=0',
        'premium=0',
        'vector=1',
        'license=commercial-nonattribution',
    ];
    jQuery.ajax({
        url: 'https://api.iconfinder.com/v3/icons/search?' + parameters.join('&'),
        type: 'GET',
        dataType: 'json',
        headers: {
            'Authorization': 'JWT ' + Cookies.get('iconfinderToken'),
        },
        success: function(response) {
            if (response['total_count'] === 0) {
                HTML.iconfinderList.innerHTML = 'Icons not found.';
            } else {
                HTML.iconfinderList.innerHTML = '';
                response.icons.forEach(item => {
                    let assetContainer = document.createElement('div');
                    let asset = document.createElement('img');
                    assetContainer.setAttribute('class', 'assetContainer');
                    assetContainer.addEventListener('click', function() {
                        let path = this.querySelector('.asset').getAttribute('data-url');
                        if (!Cookies.get('iconfinderToken')) {
                            getIconfinderToken().then(function() {
                                addIconfinderItem(path);
                            }).catch(function() {
                                popupAlert('Failed to connect with Iconfinder', 'Unable to get token');
                            });
                        } else {
                            addIconfinderItem(path);
                        }
                    });
                    asset.setAttribute('class', 'asset');
                    asset.src = item['raster_sizes'][6]['formats'][0]['preview_url'];
                    asset.setAttribute('data-url', item['vector_sizes'][0]['formats'][0]['download_url']);
                    assetContainer.appendChild(asset);
                    HTML.iconfinderList.appendChild(assetContainer);
                });
            }
        },
        error: function() {
            console.log('error');
        },
    });
}

function addIconfinderItem(_path) {
    jQuery.ajax({
        url: 'https://api.iconfinder.com/v3' + _path,
        type: 'GET',
        headers: {
            'Authorization': 'JWT ' + Cookies.get('iconfinderToken'),
        },
        success: function(response) {
            let svgString = response.querySelector('svg').outerHTML;
            fabric.loadSVGFromString(svgString, function(objects) {
                let svg = fabric.util.groupSVGElements(objects);
                svg.name = 'icon';
                svg.scaleToWidth(canvasWidth / 2, true);
                if (svg.getScaledHeight() > canvasHeight) {
                    svg.scaleToHeight(canvasHeight / 2, true);
                }
                myCanvas.add(svg).viewportCenterObject(svg).setActiveObject(svg);
                updateLayersList();
                takeSnapshot();
                HTML.iconfinderOverlay.classList.remove('active');
            });
        },
        error: function() {
            console.log('error');
        },
    });
}

function popupAlert(_title, _description) {
    HTML.alertTitle.innerHTML = _title;
    HTML.alertDescription.innerHTML = _description;
    HTML.alertOverlay.classList.add('active');
}

(function registerListeners() {
    // TODO - check bellow:
    document.addEventListener('keydown', function(event) {
        let object = myCanvas.getActiveObject();
        if (object && object.isEditing) {
            return;
        }
        switch (event.key) {
            case 'c':
                if (event.ctrlKey) {
                    if (!jQuery(event.target).is('.sp-input')) {
                        event.preventDefault();
                        copyObject();
                    }
                }
                break;
            case 'C':
                if (event.ctrlKey) {
                    event.preventDefault();
                    if (object) {
                        if (event.shiftKey) {
                            if (!jQuery(event.target).is('.sp-input')) {
                                event.preventDefault();
                                cloneObject();
                            }
                        } else {
                            if (!jQuery(event.target).is('.sp-input')) {
                                event.preventDefault();
                                copyObject();
                            }
                        }
                    } else {
                        popupAlert('No object selected', 'Please, select a object to perform this action');
                    }
                }
                break;
            case 'v':
                if (event.ctrlKey) {
                    if (!jQuery(event.target).is('.sp-input')) {
                        event.preventDefault();
                        pasteObject();
                    }
                }
                break;
            case 'V':
                if (event.ctrlKey) {
                    if (!jQuery(event.target).is('.sp-input')) {
                        event.preventDefault();
                        if (event.shiftKey) {
                            if (clipboard) {
                                clipboard.clone(function(cloned) {
                                    cloned.name = 'copy';
                                    myCanvas.add(cloned).setActiveObject(cloned);
                                    updateLayersList();
                                    takeSnapshot();
                                });
                            } else {
                                popupAlert('No object in clipboard', 'Please, copy a object to perform this action');
                            }
                        } else {
                            pasteObject();
                        }
                    }
                }
                break;
            case 'ArrowLeft':
                event.preventDefault();
                if (object && !object.isLocked) {
                    if (event.shiftKey) {
                        object.left -= 10;
                    } else {
                        object.left -= 1;
                    }
                    object.setCoords();
                    myCanvas.renderAll();
                    HTML.input_top.value = object.top.toFixed(2);
                    HTML.input_left.value = object.left.toFixed(2);
                    takeSnapshot();
                }
                break;
            case 'ArrowUp':
                event.preventDefault();
                if (object && !object.isLocked && !(HTML.fontFamilyContainer.parentElement.classList.contains('dropdownOpen'))) {
                    if (event.shiftKey) {
                        object.top -= 10;
                    } else {
                        object.top -= 1;
                    }
                    object.setCoords();
                    myCanvas.renderAll();
                    HTML.input_top.value = object.top.toFixed(2);
                    HTML.input_left.value = object.left.toFixed(2);
                    takeSnapshot();
                }
                break;
            case 'ArrowRight':
                event.preventDefault();
                if (object && !object.isLocked) {
                    if (event.shiftKey) {
                        object.left += 10;
                    } else {
                        object.left += 1;
                    }
                    object.setCoords();
                    myCanvas.renderAll();
                    HTML.input_top.value = object.top.toFixed(2);
                    HTML.input_left.value = object.left.toFixed(2);
                    takeSnapshot();
                }
                break;
            case 'ArrowDown':
                event.preventDefault();
                if (object && !object.isLocked && !(HTML.fontFamilyContainer.parentElement.classList.contains('dropdownOpen'))) {
                    if (event.shiftKey) {
                        object.top += 10;
                    } else {
                        object.top += 1;
                    }
                    object.setCoords();
                    myCanvas.renderAll();
                    HTML.input_top.value = object.top.toFixed(2);
                    HTML.input_left.value = object.left.toFixed(2);
                    takeSnapshot();
                }
                break;
            case 'Delete':
                if (!jQuery(event.target).is('.sp-input')) {
                    event.preventDefault();
                    if (object) {
                        removeObject();
                    } else {
                        popupAlert('No object selected', 'Please, select a object to perform this action');
                    }
                }
                break;
        }
    }, false);
    document.addEventListener('click', function(event) {
        let elementClicked = event.target;
        document.querySelectorAll('.dropdownOpen').forEach(dropdown => {
            if (!dropdown.contains(elementClicked)) {
                dropdown.classList.remove('dropdownOpen');
            }
        });
    });
    document.getElementById('artboard').addEventListener('click', function(event) {
        if (!jQuery(event.target).is('canvas')) {
            deselectAll();
        }
    });
    document.getElementById('button_closeAlert').addEventListener('click', function() {
        HTML.alertOverlay.classList.remove('active');
    });
    document.getElementById('button_cancelSettings').addEventListener('click', function() {
        HTML.button_settings.classList.remove('active');
        HTML.setDisplay(HTML.settingsOverlay, 'none');
    });
    document.getElementById('button_applySettings').addEventListener('click', function() {
        let zoom = myCanvas.getZoom();
        canvasWidth = HTML.input_canvasWidth.value;
        canvasHeight = HTML.input_canvasHeight.value;
        myCanvas.setDimensions({
            width: (canvasWidth * zoom),
            height: (canvasHeight * zoom),
        });
        myCanvas.renderAll();
        updatePreviewPanel();
        HTML.button_settings.classList.remove('active');
        HTML.setDisplay(HTML.settingsOverlay, 'none');
    });
    HTML.iconfinderSearchQuery.addEventListener('keydown', function(_event) {
        if (_event.key === 'Enter') {
            if (!Cookies.get('iconfinderToken')) {
                getIconfinderToken().then(function() {
                    getIconfinderItems();
                }).catch(function() {
                    popupAlert('Failed to connect with Iconfinder', 'Unable to get token');
                });
            } else {
                getIconfinderItems();
            }
        }
        preventKeydownConflict(_event);
    });
    document.getElementById('button_iconfinderSearch').addEventListener('click', function() {
        if (!Cookies.get('iconfinderToken')) {
            getIconfinderToken().then(function() {
                getIconfinderItems();
            }).catch(function() {
                popupAlert('Failed to connect with Iconfinder', 'Unable to get token');
            });
        } else {
            getIconfinderItems();
        }
    });
    document.getElementById('button_closeIconfinder').addEventListener('click', function() {
        HTML.iconfinderOverlay.classList.remove('active');
    });
    HTML.button_undo.addEventListener('click', function() {
        if (HTML.button_undo.classList.contains('disabled')) {
            return;
        }
        HTML.button_redo.classList.remove('disabled');
        if (snapshots.currentPosition === 1) {
            HTML.button_undo.classList.add('disabled');
        }
        snapshots.currentPosition--;
        myCanvas.loadFromJSON(snapshots.data[snapshots.currentPosition], function() {
            updatePreviewPanel();
            updateLayersList();
        });
    });
    HTML.button_redo.addEventListener('click', function() {
        if (HTML.button_redo.classList.contains('disabled')) {
            return;
        }
        HTML.button_undo.classList.remove('disabled');
        if (snapshots.currentPosition === (snapshots.data.length - 2)) {
            HTML.button_redo.classList.add('disabled');
        }
        snapshots.currentPosition++;
        myCanvas.loadFromJSON(snapshots.data[snapshots.currentPosition], function() {
            updatePreviewPanel();
            updateLayersList();
        });
    });
    document.getElementById('button_grab').addEventListener('click', function() {
        this.classList.toggle('active');
        if (this.classList.contains('active')) {
            isGrabMode = true;
            myCanvas.upperCanvasEl.classList.add('grab');
            myCanvas.selection = false;
            myCanvas.discardActiveObject().renderAll();
            allowObjectSelection(false);
        } else {
            isGrabMode = false;
            myCanvas.upperCanvasEl.classList.remove('grab');
            myCanvas.selection = true;
            allowObjectSelection(true);
        }
    });
    document.getElementById('button_zoomIn').addEventListener('click', function() {
        let newZoom = myCanvas.getZoom() * 100 + 5;
        canvasResize(newZoom);
    });
    document.getElementById('button_zoomOut').addEventListener('click', function() {
        let newZoom = myCanvas.getZoom() * 100 - 5;
        canvasResize(newZoom);
    });
    HTML.button_toggleLayers.addEventListener('click', function() {
        this.classList.toggle('active');
        if (this.classList.contains('active')) {
            jQuery(HTML.layersPanel).fadeIn(400);
        } else {
            jQuery(HTML.layersPanel).fadeOut(400);
        }
    });
    HTML.button_settings.addEventListener('click', function() {
        this.classList.add('active');
        HTML.setDisplay(HTML.settingsOverlay, 'flex');
        HTML.input_canvasWidth.value = canvasWidth;
        HTML.input_canvasHeight.value = canvasHeight;
    });
    document.getElementById('button_fileExport').addEventListener('click', function() {
        let content = JSON.stringify({
            'dimensions': {
                'width': canvasWidth,
                'height': canvasHeight
            },
            'fabric': myCanvas.toObject(['name'])
        });
        let anchor = document.createElement('a');
        let myBlob = new Blob([content], {type: 'text/plain'});
        anchor.href = URL.createObjectURL(myBlob);
        anchor.download = 'rsc.rsc';
        anchor.click();
    });
    document.getElementById('button_fileImport').addEventListener('click', function() {
        HTML.hiddenFilePicker2.click();
    });
    HTML.hiddenFilePicker2.addEventListener('change', function() {
        HTML.blockOverlay.classList.add('active');
        let file = this.files[0];
        let reader = new FileReader();
        reader.onload = function(event) {
            let data = JSON.parse(event.target['result']);
            canvasWidth = data['dimensions']['width'];
            canvasHeight = data['dimensions']['height'];
            myCanvas.setDimensions({
                width: canvasWidth,
                height: canvasHeight
            });
            myCanvas.loadFromJSON(data['fabric'], function() {
                HTML.blockOverlay.classList.remove('active');
            });
            HTML.hiddenFilePicker.value = null;
        };
        reader.readAsText(file);
    }, false);
    document.getElementById('previewImage').addEventListener('click', exportImage);
    HTML.hiddenFilePicker.addEventListener('change', function() {
        HTML.blockOverlay.classList.add('active');
        let file = this.files[0];
        let reader = new FileReader();
        let fileAllowed = true;
        reader.onload = function(event) {
            if (fileAllowed === true) {
                let data = event.target['result'];
                switch (file.type) {
                    case 'image/png':
                    case 'image/jpeg':
                        fabric.Image.fromURL(data, function(imgData) {
                            imgData.name = file.name;
                            imgData.scaleToWidth(canvasWidth / 2, true);
                            if (imgData.getScaledHeight() > canvasHeight) {
                                imgData.scaleToHeight(canvasHeight / 2, true);
                            }
                            myCanvas.add(imgData).viewportCenterObject(imgData).setActiveObject(imgData);
                            updateLayersList();
                            takeSnapshot();
                            HTML.blockOverlay.classList.remove('active');
                        });
                        break;
                    case 'image/svg+xml':
                        fabric.loadSVGFromString(data, function(fabricObjects) {
                            let fonts = [];
                            let fontsStrings = [];
                            fabricObjects.forEach(object => {
                                if (object.type === 'text') {
                                    object.fontFamily = object.fontFamily.replace(/'/g, '');
                                    if (!fontsStrings.includes(object.fontFamily)) {
                                        let font = new FontFaceObserver(object.fontFamily);
                                        fontsStrings.push(object.fontFamily);
                                        fonts.push(font.load());
                                    }
                                }
                            });
                            Promise.all(fonts).then(function() {
                                for (let counter = 0; fabricObjects.length > counter; counter++) {
                                    let object = fabricObjects[counter];
                                    if (object.type === 'text') {
                                        let text = object.text;
                                        let optionsopt = object.toObject();
                                        delete optionsopt.text;
                                        delete optionsopt.type;
                                        optionsopt.strokeWidth = 0;
                                        let newTextbox = new fabric.Textbox(text, optionsopt);
                                        newTextbox.name = 'text';
                                        fabricObjects[counter] = newTextbox;
                                    }
                                }
                                let svg = fabric.util.groupSVGElements(fabricObjects);
                                svg.name = file.name;
                                svg.scaleToWidth(canvasWidth / 2, true);
                                if (svg.getScaledHeight() > canvasHeight) {
                                    svg.scaleToHeight(canvasHeight / 2, true);
                                }
                                myCanvas.add(svg).viewportCenterObject(svg).setActiveObject(svg);
                                updateLayersList();
                                takeSnapshot();
                                HTML.blockOverlay.classList.remove('active');
                            }).catch(function(error) {
                                for (let counter = 0; fabricObjects.length > counter; counter++) {
                                    let object = fabricObjects[counter];
                                    if (object.type === 'text') {
                                        let text = object.text;
                                        let optionsopt = object.toObject();
                                        delete optionsopt.text;
                                        delete optionsopt.type;
                                        optionsopt.strokeWidth = 0;
                                        let newTextbox = new fabric.Textbox(text, optionsopt);
                                        newTextbox.name = 'text';
                                        fabricObjects[counter] = newTextbox;
                                    }
                                }
                                let svg = fabric.util.groupSVGElements(fabricObjects);
                                svg.name = file.name;
                                if (svg.width > (myCanvas.getWidth() / myCanvas.getZoom())) {
                                    svg.scaleToWidth((myCanvas.getWidth() / myCanvas.getZoom()) / 2, true);
                                }
                                if (svg.height > (myCanvas.getHeight() / myCanvas.getZoom())) {
                                    svg.scaleToHeight((myCanvas.getHeight() / myCanvas.getZoom()) / 2, true);
                                }
                                myCanvas.add(svg).viewportCenterObject(svg).setActiveObject(svg);
                                updateLayersList();
                                takeSnapshot();
                                HTML.blockOverlay.classList.remove('active');
                                popupAlert('Error', 'Failed to load font ' + error.family);
                            });
                        });
                        break;
                }
                HTML.hiddenFilePicker.value = null;
            }
        };
        if (file.size > 10000000) {
            HTML.blockOverlay.classList.remove('active');
            popupAlert('Size not allowed', 'Only files up to 10Mb are allowed');
            fileAllowed = false;
        } else {
            switch (file.type) {
                case 'image/png':
                case 'image/jpeg':
                    reader.readAsDataURL(file);
                    break;
                case 'image/svg+xml':
                    reader.readAsText(file);
                    break;
                default:
                    HTML.blockOverlay.classList.remove('active');
                    popupAlert('Type not allowed', 'Only jpeg, png or svg files are allowed');
                    fileAllowed = false;
            }
        }
    }, false);
    document.getElementById('button_addImage').addEventListener('click', function() {
        HTML.hiddenFilePicker.click();
    });
    document.getElementById('button_addRectangle').addEventListener('click', function() {
        let newElement = new fabric.Rect({
            name: 'rectangle',
            width: (canvasWidth / 2),
            height: (canvasHeight / 2),
            fill: '#000000'
        });
        myCanvas.add(newElement).viewportCenterObject(newElement).setActiveObject(newElement);
        updateLayersList();
        takeSnapshot();
    });
    document.getElementById('button_addCircle').addEventListener('click', function() {
        let newElement = new fabric.Circle({
            name: 'circle',
            radius: (canvasHeight / 4),
            fill: '#000000'
        });
        myCanvas.add(newElement).viewportCenterObject(newElement).setActiveObject(newElement);
        updateLayersList();
        takeSnapshot();
    });
    document.getElementById('button_addTriangle').addEventListener('click', function() {
        let newElement = new fabric.Triangle({
            name: 'triangle',
            width: (canvasWidth / 2),
            height: (canvasHeight / 2),
            fill: '#000000'
        });
        myCanvas.add(newElement).viewportCenterObject(newElement).setActiveObject(newElement);
        updateLayersList();
        takeSnapshot();
    });
    document.getElementById('button_addText').addEventListener('click', function() {
        let newElement = new fabric.Textbox('Double click to edit', {
            name: 'text',
            textAlign: 'center',
            width: (canvasWidth / 2),
            fill: '#000000',
            fontFamily: 'Raleway',
            fontSize: 32,
            strokeWidth: 0,
        });
        myCanvas.add(newElement).viewportCenterObject(newElement).setActiveObject(newElement);
        updateLayersList();
        takeSnapshot();
    });
    document.getElementById('button_openIconfinder').addEventListener('click', function() {
        HTML.iconfinderOverlay.classList.add('active');
    });
    // clip panel - listeners
    HTML.button_startClipEllipse.addEventListener('click', function() {startClippingMode('clipEllipse');});
    HTML.button_startClipRectangle.addEventListener('click', function() {startClippingMode('clipRectangle');});
    HTML.button_cancelClipping.addEventListener('click', function() {endClippingMode('cancel');});
    HTML.button_applyClipping.addEventListener('click', function() {endClippingMode('apply');});
    HTML.input_clipWidth.addEventListener('keydown', preventKeydownConflict);
    HTML.input_clipWidth.addEventListener('change', function() {
        let newValue = Number(this.value);
        if (HTML.button_linkClipDimensions.classList.contains('linked')) {
            let oldWidth = clipZone.getScaledWidth();
            clipZone.scaleX = newValue / clipZone.width;
            clipZone.scaleY = clipZone.getScaledHeight() * (newValue / oldWidth) / clipZone.height;
            HTML.input_clipHeight.value = Math.floor(clipZone.getScaledHeight()).toFixed(0);
        } else {
            clipZone.scaleX = newValue / clipZone.width;
        }
        clipZone.setCoords();
        myCanvas.renderAll();
    });
    HTML.input_clipWidth.addEventListener('focus', highlightInputContent);
    HTML.input_clipHeight.addEventListener('keydown', preventKeydownConflict);
    HTML.input_clipHeight.addEventListener('change', function() {
        let newValue = Number(this.value);
        if (HTML.button_linkClipDimensions.classList.contains('linked')) {
            let oldHeight = clipZone.getScaledHeight();
            clipZone.scaleY = newValue / clipZone.height;
            clipZone.scaleX = clipZone.getScaledWidth() * (newValue / oldHeight) / clipZone.width;
            HTML.input_clipWidth.value = Math.floor(clipZone.getScaledWidth()).toFixed(0);
        } else {
            clipZone.scaleY = newValue / clipZone.height;
        }
        clipZone.setCoords();
        myCanvas.renderAll();
    });
    HTML.input_clipHeight.addEventListener('focus', highlightInputContent);
    // objects options panel - image
    HTML.button_resetImageFx.addEventListener('click', function() {
        emptyImageFiltersDropdown();
        updateImageFilters();
        takeSnapshot();
    });
    jQuery(HTML.imageFx_section_imageFilters).find('.imageFx_checkbox').on('click', function() {
        this.parentElement.classList.toggle('checked');
        updateImageFilters();
        takeSnapshot();
    });
    HTML.imageFx_grayscaleSwitch.addEventListener('click', function() {
        let radios = HTML.imageFx_section_grayscale.querySelectorAll('.imageFx_radioContainer');
        this.classList.toggle('active');
        if (this.classList.contains('active')) {
            radios[0].classList.add('checked');
        } else {
            radios.forEach(radio => {
                radio.classList.remove('checked');
            });
        }
        updateImageFilters();
        takeSnapshot();
    });
    jQuery(HTML.imageFx_section_grayscale).find('.imageFx_radio').on('click', function() {
        let radios = HTML.imageFx_section_grayscale.querySelectorAll('.imageFx_radioContainer');
        if (HTML.imageFx_grayscaleSwitch.classList.contains('active')) {
            if (this.parentElement.classList.contains('checked')) {
                return;
            } else {
                radios.forEach(radio => {
                    radio.classList.remove('checked');
                });
                this.parentElement.classList.add('checked');
            }
            updateImageFilters();
            takeSnapshot();
        }
    });
    jQuery(HTML.imageFx_section_fxEffects).find('.imageFx_checkbox').on('click', function() {
        let container = this.parentElement;
        let isChecked;
        container.classList.toggle('checked');
        isChecked = container.classList.contains('checked');
        switch (container.querySelector('.imageFx_type').innerHTML) {
            case 'Pixelate':
                if (isChecked) {
                    HTML.setValue(container.querySelector('.inputValue'), '1');
                    HTML.setValue(container.querySelector('.inputRange'), 1);
                } else {
                    HTML.setValue(container.querySelector('.inputValue'), '');
                    HTML.setValue(container.querySelector('.inputRange'), 1);
                }
                break;
            default:
                if (isChecked) {
                    HTML.setValue(container.querySelector('.inputValue'), '0');
                    HTML.setValue(container.querySelector('.inputRange'), 0);
                } else {
                    HTML.setValue(container.querySelector('.inputValue'), '');
                    HTML.setValue(container.querySelector('.inputRange'), 0);
                }
        }
        updateImageFilters();
        takeSnapshot();
    });
    jQuery(HTML.imageFx_section_fxEffects).find('.inputValue').on({
        keydown: preventKeydownConflict,
        change: function() {
            let container = this.parentElement;
            let isChecked = container.classList.contains('checked');
            let newValue = Number(this.value);
            let min;
            let max;
            if (isChecked) {
                switch (container.getAttribute('data-filter-name')) {
                    case 'Blur':
                        min = 0;
                        max = 1;
                        break;
                    case 'Pixelate':
                        min = 1;
                        max = 100;
                        break;
                    case 'Noise':
                        min = 0;
                        max = 1000;
                        break;
                    case 'HueRotation':
                    case 'Saturation':
                    case 'Contrast':
                    case 'Brightness':
                        min = -1;
                        max = 1;
                        break;
                }
                if (isNaN(newValue)) {
                    popupAlert('invalid value', '');
                    this.value = container.querySelector('.inputRange').value.toString();
                    return;
                } else if (newValue < min) {
                    popupAlert('minimum value is ' + min, '');
                    this.value = min.toString();
                    newValue = min;
                } else if (newValue > max) {
                    popupAlert('maximum value is ' + max, '');
                    this.value = max.toString();
                    newValue = max;
                }
                HTML.setValue(container.querySelector('.inputRange'), newValue);
                updateImageFilters();
                takeSnapshot();
            }
        },
    });
    jQuery(HTML.imageFx_section_fxEffects).find('.inputRange').on({
        input: function() {
            let container = this.parentElement;
            let isChecked = container.classList.contains('checked');
            if (isChecked) {
                HTML.setValue(container.querySelector('.inputValue'), this.value.toString());
                updateImageFilters();
            }
        },
        change: function() {
            let container = this.parentElement;
            let isChecked = container.classList.contains('checked');
            if (isChecked) {
                takeSnapshot();
            }
        },
    });
    HTML.imageFx_gammaSwitch.addEventListener('click', function() {
        let options = HTML.imageFx_section_gamma.querySelectorAll('.imageFx_option');
        let isGammaActive;
        this.classList.toggle('active');
        isGammaActive = this.classList.contains('active');
        options.forEach(option => {
            option.classList.toggle('active', isGammaActive);
            HTML.setValue(option.querySelector('.inputValue'), isGammaActive ? '1' : '');
            HTML.setValue(option.querySelector('.inputRange'), 1);
        });
        updateImageFilters();
        takeSnapshot();
    });
    jQuery(HTML.imageFx_section_gamma).find('.inputValue').on({
        keydown: preventKeydownConflict,
        change: function() {
            let container = this.parentElement;
            let isActive = container.classList.contains('active');
            let newValue = Number(this.value);
            if (isActive) {
                if (isNaN(newValue)) {
                    popupAlert('invalid value', '');
                    this.value = container.querySelector('.inputRange').value.toString();
                    return;
                } else if (newValue < 0.1) {
                    popupAlert('minimum value is 0.1', '');
                    this.value = '0.1';
                    newValue = 0.1;
                } else if (newValue > 2.2) {
                    popupAlert('maximum value is 2.2', '');
                    this.value = '2.2';
                    newValue = 2.2;
                }
                HTML.setValue(container.querySelector('.inputRange'), newValue);
                updateImageFilters();
                takeSnapshot();
            }
        },
    });
    jQuery(HTML.imageFx_section_gamma).find('.inputRange').on({
        input: function() {
            let container = this.parentElement;
            let isActive = container.classList.contains('active');
            if (isActive) {
                HTML.setValue(container.querySelector('.inputValue'), this.value.toString());
                updateImageFilters();
            }
        },
        change: function() {
            let container = this.parentElement;
            let isActive = container.classList.contains('active');
            if (isActive) {
                takeSnapshot();
            }
        },
    });
    // objects options panel - clip
    HTML.button_clip.addEventListener('click', function() {
        if (this.classList.contains('hasClip')) {
            myCanvas.getActiveObject().clipPath = null;
            myCanvas.renderAll();
            takeSnapshot();
            updateObjectsOptions();
        } else {
            HTML.objectsOptionsPanel.classList.remove('active');
            HTML.clipPanel.classList.add('active');
            HTML.setDisplay(HTML.startClippingContainer, 'flex');
            HTML.setDisplay(HTML.finishClippingContainer, 'none');
        }
    });
    // objects options panel - font
    HTML.fontFamilyContainer.addEventListener('click', function() {
        this.parentElement.classList.toggle('dropdownOpen');
        if (this.parentElement.classList.contains('dropdownOpen')) {
            let currentFont = myCanvas.getActiveObject().fontFamily;
            this.parentElement.querySelectorAll('.fontOption').forEach(element => {
                if (element.getAttribute('data-family') === currentFont) {
                    element.classList.add('selected');
                    jQuery(HTML.fontsContainer).animate({
                        scrollTop: HTML.fontsContainer.scrollTop + jQuery(element).position().top - 5,
                    });
                } else {
                    element.classList.remove('selected');
                }
            });
        }
    });
    HTML.fontsContainer.querySelectorAll('.fontOption').forEach(element => {
        element.addEventListener('click', fontSelection);
    });
    HTML.input_fontSize.addEventListener('keydown', preventKeydownConflict);
    HTML.input_fontSize.addEventListener('change', function() {
        let activeObject = myCanvas.getActiveObject();
        let newValue = Number(Number(this.value).toFixed(1));
        if (Number.isNaN(newValue)) {
            popupAlert('Wrong type of value', '');
            this.value = activeObject.fontSize.toFixed(1);
        } else {
            if (activeObject.type === 'activeSelection') {
                activeObject.forEachObject(object => {
                    object.fontSize = newValue;
                });
            } else {
                activeObject.fontSize = newValue;
            }
            myCanvas.renderAll();
            takeSnapshot();
        }
    });
    HTML.input_fontSize.addEventListener('wheel', function(event) {
        let activeObject = myCanvas.getActiveObject();
        let targetValue = Number(this.value) ? Number(this.value) : '30';
        if (event.deltaY < 0) {
            targetValue++;
        } else if (event.deltaY > 0) {
            targetValue--;
        }
        if (targetValue < 0) {
            targetValue = 0;
        }
        if (targetValue !== Number(this.value)) {
            if (activeObject.type === 'activeSelection') {
                activeObject.forEachObject(object => {
                    object.fontSize = targetValue;
                });
            } else {
                activeObject.fontSize = targetValue;
            }
            myCanvas.renderAll();
            takeSnapshot();
            this.value = targetValue.toFixed(1);
        }
    });
    HTML.input_fontSize.addEventListener('focus', highlightInputContent);
    HTML.input_charSpacing.addEventListener('keydown', preventKeydownConflict);
    HTML.input_charSpacing.addEventListener('change', function() {
        let activeObject = myCanvas.getActiveObject();
        let newValue = Number(Number(this.value).toFixed());
        if (Number.isNaN(newValue)) {
            popupAlert('Wrong type of value', '');
            this.value = activeObject.charSpacing.toFixed();
        } else {
            if (newValue > 800) {
                popupAlert('Maximum value allowed is 800', '');
                newValue = 800;
                this.value = '800';
            }
            if (newValue < -200) {
                popupAlert('Minimum value allowed is -200', '');
                newValue = -200;
                this.value = '-200';
            }
            changeCharSpacing(newValue);
            myCanvas.renderAll();
            takeSnapshot();
        }
    });
    HTML.input_charSpacing.addEventListener('wheel', function(event) {
        let targetValue = Number(this.value);
        if (event.deltaY < 0) {
            targetValue += 10;
        } else if (event.deltaY > 0) {
            targetValue -= 10;
        }
        if (targetValue > 800) {
            popupAlert('Maximum value allowed is 800', '');
            targetValue = 800;
        } else if (targetValue < -200) {
            popupAlert('Minimum value allowed is -200', '');
            targetValue = -200;
        }
        changeCharSpacing(targetValue);
        myCanvas.renderAll();
        takeSnapshot();
    });
    HTML.input_charSpacing.addEventListener('focus', highlightInputContent);
    HTML.input_lineHeight.addEventListener('keydown', preventKeydownConflict);
    HTML.input_lineHeight.addEventListener('change', function() {
        let activeObject = myCanvas.getActiveObject();
        let newValue = Number(Number(this.value).toFixed(2));
        if (Number.isNaN(newValue)) {
            popupAlert('Wrong type of value', '');
            this.value = activeObject.lineHeight.toFixed(2);
        } else {
            if (newValue > 5) {
                popupAlert('Maximum value allowed is 5', '');
                newValue = 5;
                this.value = '5';
            }
            if (newValue < 0.5) {
                popupAlert('Minimum value allowed is 0.5', '');
                newValue = 0.5;
                this.value = '0.5';
            }
            changeLineHeight(newValue);
            myCanvas.renderAll();
            takeSnapshot();
        }
    });
    HTML.input_lineHeight.addEventListener('wheel', function() {
        let targetValue = Number(this.value);
        if (event.deltaY < 0) {
            targetValue = targetValue + 0.1;
        } else if (event.deltaY > 0) {
            targetValue = targetValue - 0.1;
        }
        if (targetValue > 5) {
            popupAlert('Maximum value allowed is 5', '');
            targetValue = 5;
        } else if (targetValue < 0.5) {
            popupAlert('Minimum value allowed is 0.5', '');
            targetValue = 0.5;
        }
        changeLineHeight(targetValue);
        myCanvas.renderAll();
        takeSnapshot();
    });
    HTML.input_lineHeight.addEventListener('focus', highlightInputContent);
    HTML.objectsOptionsPanel.querySelectorAll('.alignText').forEach(element => {
        element.addEventListener('click', alignTextOption);
    });
    // objects options panel - opacity
    HTML.input_opacity.addEventListener('keydown', preventKeydownConflict);
    HTML.input_opacity.addEventListener('change', function() {
        let newValue = Number(this.value.replace('%', ''));
        if (newValue > 100) {
            newValue = 100;
        } else if (newValue < 0) {
            newValue = 0;
        }
        objectOpacity(newValue);
    });
    HTML.input_opacity.addEventListener('wheel', function(event) {
        let targetValue = Number(this.value.replace('%', ''));
        if (event.deltaY < 0) {
            targetValue += 10;
        } else if (event.deltaY > 0) {
            targetValue -= 10;
        }
        if (targetValue > 100) {
            targetValue = 100;
        } else if (targetValue < 0) {
            targetValue = 0;
        }
        objectOpacity(targetValue);
    });
    HTML.input_opacity.addEventListener('focus', highlightInputContent);
    // objects options panel - align
    jQuery(HTML.objectsOptionsPanel).find('.alignObject').on('click', function() {
        objectAlignTo(this.getAttribute('data-objectAlign'));
    });
    // objects options panel - position
    HTML.input_top.addEventListener('keydown', preventKeydownConflict);
    HTML.input_top.addEventListener('change', function() {
        let object = myCanvas.getActiveObject();
        if (Number(this.value) !== object.top) {
            object.top = Number(this.value);
            object.setCoords();
            myCanvas.renderAll();
            takeSnapshot();
        }
    });
    HTML.input_top.addEventListener('wheel', function(event) {
        let object = myCanvas.getActiveObject();
        let targetValue = Number(this.value);
        if (event.deltaY < 0) {
            targetValue += 1;
        } else if (event.deltaY > 0) {
            targetValue -= 1;
        }
        object.top = Number(targetValue);
        this.value = targetValue.toString();
        object.setCoords();
        myCanvas.renderAll();
        takeSnapshot();
    });
    HTML.input_top.addEventListener('focus', highlightInputContent);
    HTML.input_left.addEventListener('keydown', preventKeydownConflict);
    HTML.input_left.addEventListener('change', function() {
        let object = myCanvas.getActiveObject();
        if (Number(this.value) !== object.left) {
            object.left = Number(this.value);
            object.setCoords();
            myCanvas.renderAll();
            takeSnapshot();
        }
    });
    HTML.input_left.addEventListener('wheel', function(event) {
        let object = myCanvas.getActiveObject();
        let targetValue = Number(this.value);
        if (event.deltaY < 0) {
            targetValue += 1;
        } else if (event.deltaY > 0) {
            targetValue -= 1;
        }
        object.left = Number(targetValue);
        this.value = targetValue.toString();
        object.setCoords();
        myCanvas.renderAll();
        takeSnapshot();
    });
    HTML.input_left.addEventListener('focus', highlightInputContent);
    // objects options panel - size
    HTML.input_width.addEventListener('keydown', preventKeydownConflict);
    HTML.input_width.addEventListener('change', function() {
        let object = myCanvas.getActiveObject();
        let newValue = Number(this.value);
        if (newValue !== object.getScaledWidth().toFixed(2)) {
            if (object.type === 'textbox') {
                object.width = newValue / object.scaleX;
            } else {
                if (HTML.button_linkDimensions.classList.contains('linked')) {
                    let oldWidth = object.getScaledWidth();
                    object.scaleX = newValue / object.width;
                    object.scaleY = object.getScaledHeight() * (newValue / oldWidth) / object.height;
                    HTML.input_height.value = object.getScaledHeight().toFixed(2);
                } else {
                    object.scaleX = newValue / object.width;
                }
            }
            object.setCoords();
            myCanvas.renderAll();
            takeSnapshot();
        }
    });
    HTML.input_width.addEventListener('wheel', function(event) {
        let object = myCanvas.getActiveObject();
        let targetValue = Number(this.value);
        if (event.deltaY < 0) {
            targetValue += 1;
        } else if (event.deltaY > 0) {
            targetValue -= 1;
        }
        if (object.type === 'textbox') {
            object.width = targetValue / object.scaleX;
        } else {
            if (HTML.button_linkDimensions.classList.contains('linked')) {
                let oldWidth = object.getScaledWidth();
                object.scaleX = targetValue / object.width;
                object.scaleY = object.getScaledHeight() * (targetValue / oldWidth) / object.height;
                HTML.input_height.value = object.getScaledHeight().toFixed(2);
            } else {
                object.scaleX = targetValue / object.width;
            }
        }
        HTML.input_width.value = targetValue.toFixed(2);
        object.setCoords();
        myCanvas.renderAll();
        takeSnapshot();
    });
    HTML.input_width.addEventListener('focus', highlightInputContent);
    HTML.input_height.addEventListener('keydown', preventKeydownConflict);
    HTML.input_height.addEventListener('change', function() {
        let object = myCanvas.getActiveObject();
        let newValue = Number(this.value);
        if (newValue !== object.getScaledHeight().toFixed(2)) {
            if (HTML.button_linkDimensions.classList.contains('linked')) {
                let oldHeight = object.getScaledHeight();
                object.scaleY = newValue / object.height;
                object.scaleX = object.getScaledWidth() * (newValue / oldHeight) / object.width;
                HTML.input_width.value = object.getScaledWidth().toFixed(2);
            } else {
                object.scaleY = newValue / object.height;
            }
            object.setCoords();
            myCanvas.renderAll();
            takeSnapshot();
        }
    });
    HTML.input_height.addEventListener('wheel', function(event) {
        let object = myCanvas.getActiveObject();
        let targetValue = Number(this.value);
        if (event.deltaY < 0) {
            targetValue += 1;
        } else if (event.deltaY > 0) {
            targetValue -= 1;
        }
        if (HTML.button_linkDimensions.classList.contains('linked')) {
            let oldHeight = object.getScaledHeight();
            object.scaleY = targetValue / object.height;
            object.scaleX = object.getScaledWidth() * (targetValue / oldHeight) / object.width;
            HTML.input_width.value = object.getScaledWidth().toFixed(2);
        } else {
            object.scaleY = targetValue / object.height;
        }
        HTML.input_height.value = targetValue.toFixed(2);
        object.setCoords();
        myCanvas.renderAll();
        takeSnapshot();
    });
    HTML.input_height.addEventListener('focus', highlightInputContent);
    // objects options panel - rotation
    jQuery(HTML.objectsOptionsPanel).find('.rotateObject').on('click', function() {
        objectRelativeRotation(this.getAttribute('data-objectRotate'));
    });
    HTML.input_rotate.addEventListener('keydown', preventKeydownConflict);
    HTML.input_rotate.addEventListener('change', function() {
        let newValue = Number(this.value.replace('º', ''));
        if (newValue > 360) {
            popupAlert('Maximum value allowed is 360º', '');
            newValue = 360;
            this.value = '360';
        } else if (newValue < -360) {
            popupAlert('Minimum value allowed is -360º', '');
            newValue = -360;
            this.value = '-360';
        }
        objectAbsoluteRotation(newValue);
    });
    HTML.input_rotate.addEventListener('wheel', function(event) {
        let targetValue = Number(this.value.replace('º', ''));
        if (event.deltaY < 0) {
            targetValue += 1;
        } else if (event.deltaY > 0) {
            targetValue -= 1;
        }
        if (targetValue > 360) {
            popupAlert('Maximum value allowed is 360º', '');
            targetValue = 360;
        } else if (targetValue < -360) {
            popupAlert('Minimum value allowed is -360º', '');
            targetValue = -360;
        }
        objectAbsoluteRotation(targetValue);
    });
    HTML.input_rotate.addEventListener('focus', highlightInputContent);
    // objects options panel - flip
    jQuery(HTML.objectsOptionsPanel).find('.flipObject').on('click', function() {
        objectFlip(this.getAttribute('data-objectFlip'));
    });
    // objects options panel - border radius
    HTML.range_rectangleBorderRadius.addEventListener('input', function() {
        myCanvas.getActiveObject().setOptions({
            rx: Number(this.value),
            ry: Number(this.value),
        });
        myCanvas.renderAll();
        takeSnapshot();
    });
    // objects options panel - others
    document.getElementById('button_bringToFront').addEventListener('click', bringObjectToFront);
    document.getElementById('button_sendToBack').addEventListener('click', sendObjectToBack);
    document.getElementById('button_fitToCanvas').addEventListener('click', function() {
        let object = myCanvas.getActiveObject();
        object.scaleToWidth(canvasWidth, true);
        if (object.getScaledHeight() > canvasHeight) {
            object.scaleToHeight(canvasHeight, true);
        }
        object.viewportCenter();
        object.setCoords();
        myCanvas.renderAll();
        HTML.input_width.value = object.getScaledWidth().toFixed();
        HTML.input_height.value = object.getScaledHeight().toFixed();
        HTML.input_top.value = object.top.toFixed(2);
        HTML.input_left.value = object.left.toFixed(2);
        takeSnapshot();
    });
    document.getElementById('button_clone').addEventListener('click', cloneObject);
    document.getElementById('button_delete').addEventListener('click', removeObject);
})();

jQuery(HTML.spectrumTrigger_singleColor).spectrum({
    containerClassName: 'singleColor',
    preferredFormat: 'hex',
    showPalette: true,
    showSelectionPalette: false,
    palette: paletteColors,
    showInitial: true,
    showInput: true,
    showAlpha: false,
    clickoutFiresChange: false,
    beforeShow: function() {
        let activeObject = myCanvas.getActiveObject();
        if (activeObject.type === 'activeSelection') {
            let selection = activeObject.getObjects();
            let isSameColor = true;
            let firstColor = selection[0].fill;
            for (let counter = 0; counter < selection.length; counter++) {
                if (selection[counter].fill !== firstColor) {
                    isSameColor = false;
                }
            }
            if (isSameColor) {
                jQuery(this).spectrum('set', firstColor);
            } else {
                jQuery(this).spectrum('set', 'transparent');
            }
        } else if (activeObject.fill.colorStops) {
            jQuery(this).spectrum('set', 'transparent');
        } else {
            jQuery(this).spectrum('set', activeObject.fill);
        }
    },
    show: function(_color) {
        let container = jQuery(this).spectrum('container')[0];
        let activeObject = myCanvas.getActiveObject();
        let colorpickerId = '#' + this.getAttribute('id');
        let typeOptions = container.querySelectorAll('.type-option');
        let gradientRow = container.querySelector('.gradient-row');
        if (_color.toName() === 'transparent') {
            HTML.setValue(container.querySelector('.sp-input'), '');
        }
        HTML.setDisplay(container.querySelector('.title'), 'none');
        HTML.setDisplay(container.querySelector('.type-container'), 'flex');
        gradientRow.innerHTML = '';
        if (activeObject.fill.colorStops) {
            typeOptions[0].classList.remove('active');
            typeOptions[1].classList.add('active');
            HTML.setDisplay(container.querySelector('.sp-button-container'), 'none');
            HTML.setDisplay(container.querySelector('.sp-palette'), 'none');
            HTML.setDisplay(container.querySelector('.gradient-container'), 'flex');
            HTML.setDisplay(container.querySelector('.gradients-palette'), 'flex');
            (activeObject.fill.colorStops).forEach(colorStop => {
                createGradientHandler(colorpickerId, gradientRow, colorStop.color, colorStop.offset);
            });
            jQuery(this).spectrum('updateGradientRow');
            setColorpickerAngleInfo(container, getGradientAngle('fill'));
        } else {
            typeOptions[0].classList.add('active');
            typeOptions[1].classList.remove('active');
            HTML.setDisplay(container.querySelector('.sp-button-container'), 'flex');
            HTML.setDisplay(container.querySelector('.sp-palette'), 'block');
            HTML.setDisplay(container.querySelector('.gradient-container'), 'none');
            HTML.setDisplay(container.querySelector('.gradients-palette'), 'none');
            createGradientHandler(colorpickerId, gradientRow, '#00FF00', 0);
            createGradientHandler(colorpickerId, gradientRow, '#0000FF', 1);
            jQuery(this).spectrum('updateGradientRow');
            setColorpickerAngleInfo(container, 0);
        }
    },
    change: function(_color) {
        let activeObject = myCanvas.getActiveObject();
        if (activeObject) {
            HTML.spectrumTrigger_singleColor.style.background = _color.toHexString();
            if (activeObject.setSelectionStyles && activeObject.isEditing) {
                activeObject.setSelectionStyles({
                    'fill': _color.toHexString(),
                });
            } else {
                if (activeObject.type === 'activeSelection') {
                    activeObject.forEachObject(object => {
                        object.setColor(_color.toHexString());
                    });
                } else {
                    activeObject.setColor(_color.toHexString());
                    activeObject.styles = {};
                }
            }
            myCanvas.renderAll();
            takeSnapshot();
            updateObjectsOptions();
        }
    },
    hide: function() {
        if (isEyedropperMode) {
            let eyedropper = jQuery(this).spectrum('container')[0].querySelector('.eyedropperContainer');
            eyedropper.click();
        }
    },
});
jQuery(HTML.spectrumTrigger_backgroundColor).spectrum({
    containerClassName: 'backgroundColor',
    preferredFormat: 'hex',
    showPalette: true,
    showSelectionPalette: false,
    palette: paletteColors,
    showInitial: true,
    showInput: true,
    showAlpha: false,
    clickoutFiresChange: false,
    beforeShow: function() {
        if (myCanvas.backgroundColor) {
            if (myCanvas.backgroundColor.colorStops) {
                jQuery(this).spectrum('set', 'transparent');
            } else {
                jQuery(this).spectrum('set', myCanvas.backgroundColor);
            }
        } else {
            jQuery(this).spectrum('set', 'transparent');
        }
    },
    show: function(_color) {
        let container = jQuery(this).spectrum('container')[0];
        let colorpickerId = '#' + this.getAttribute('id');
        let typeOptions = container.querySelectorAll('.type-option');
        let gradientRow = container.querySelector('.gradient-row');
        if (_color.toName() === 'transparent') {
            HTML.setValue(container.querySelector('.sp-input'), '');
        }
        HTML.setDisplay(container.querySelector('.title'), 'none');
        HTML.setDisplay(container.querySelector('.type-container'), 'flex');
        gradientRow.innerHTML = '';
        if (myCanvas.backgroundColor.colorStops) {
            typeOptions[0].classList.remove('active');
            typeOptions[1].classList.add('active');
            HTML.setDisplay(container.querySelector('.sp-button-container'), 'none');
            HTML.setDisplay(container.querySelector('.sp-palette'), 'none');
            HTML.setDisplay(container.querySelector('.gradient-container'), 'flex');
            HTML.setDisplay(container.querySelector('.gradients-palette'), 'flex');
            (myCanvas.backgroundColor.colorStops).forEach(colorStop => {
                createGradientHandler(colorpickerId, gradientRow, colorStop.color, colorStop.offset);
            });
            jQuery(this).spectrum('updateGradientRow');
            setColorpickerAngleInfo(container, getGradientAngle('background'));
        } else {
            typeOptions[0].classList.add('active');
            typeOptions[1].classList.remove('active');
            HTML.setDisplay(container.querySelector('.sp-button-container'), 'flex');
            HTML.setDisplay(container.querySelector('.sp-palette'), 'block');
            HTML.setDisplay(container.querySelector('.gradient-container'), 'none');
            HTML.setDisplay(container.querySelector('.gradients-palette'), 'none');
            createGradientHandler(colorpickerId, gradientRow, '#00FF00', 0);
            createGradientHandler(colorpickerId, gradientRow, '#0000FF', 1);
            jQuery(this).spectrum('updateGradientRow');
            setColorpickerAngleInfo(container, 0);
        }
    },
    change: function(_color) {
        myCanvas.backgroundImage = false;
        myCanvas.setBackgroundColor(_color.toRgbString(), function() {
            myCanvas.renderAll();
            takeSnapshot();
        });
        this.style.background = _color.toRgbString();
    },
    hide: function() {
        if (isEyedropperMode) {
            let eyedropper = jQuery(this).spectrum('container')[0].querySelector('.eyedropperContainer');
            eyedropper.click();
        }
    },
});

fabric.Object.NUM_FRACTION_DIGITS = 8;
fabric.textureSize = 4096;
fabric.Object.prototype.set({
    borderColor: '#777777',
    cornerColor: '#ffffff',
    cornerStrokeColor: '#777777',
    cornerSize: 10,
    cornerStyle: 'circle',
    rotatingPointOffset: 30,
    transparentCorners: false,
    strokeWidth: 0,
    onDeselect: function() {
        if (isClippingEllipseMode && myCanvas.getActiveObject().name === 'clipEllipse') {
            return true;
        }
        if (isClippingRectangleMode && myCanvas.getActiveObject().name === 'clipRectangle') {
            return true;
        }
        if (isEyedropperMode) {
            return true;
        }
    },
});
fabric.Textbox.prototype.set({
    _controlsVisibility: {
        mtr: true,
        tl: false,
        tr: false,
        bl: false,
        br: false,
        mt: false,
        mb: false,
        ml: true,
        mr: true,
    },
});

myCanvas.on({
    'object:modified': function() {
        if (!isClippingEllipseMode && !isClippingRectangleMode) {
            takeSnapshot();
        }
    },
    'object:added': function(arg) {
        let object = arg.target;
        if (!object.name) {
            object.name = 'object';
        }
        object.id = getUniqueId();
        updateLayersList();
    },
    'object:removed': function() {
        updateLayersList();
    },
    'object:rotating': function(arg) {
        HTML.input_rotate.value = (arg.target.angle).toFixed() + 'º';
        HTML.input_top.value = (arg.target.top).toFixed(2);
        HTML.input_left.value = (arg.target.left).toFixed(2);
    },
    'object:scaling': function(arg) {
        if (isClippingEllipseMode || isClippingRectangleMode) {
            HTML.input_clipWidth.value = clipZone.getScaledWidth().toFixed();
            HTML.input_clipHeight.value = clipZone.getScaledHeight().toFixed();
        } else {
            HTML.input_width.value = arg.target.getScaledWidth().toFixed(2);
            HTML.input_height.value = arg.target.getScaledHeight().toFixed(2);
            HTML.input_top.value = arg.target.top.toFixed(2);
            HTML.input_left.value = arg.target.left.toFixed(2);
        }
    },
    'object:moving': function(arg) {
        HTML.input_top.value = arg.target.top.toFixed(2);
        HTML.input_left.value = arg.target.left.toFixed(2);
    },
    'selection:created': function(_event) {
        let activeObject = _event.target;
        updateLayersList();
        updateObjectsOptions();
        HTML.objectsOptionsPanel.classList.add('active');
        if (activeObject.type !== 'activeSelection' && HTML.layersList.clientHeight > HTML.layersScroll.clientHeight) {
            scrollToLayer(activeObject.id);
        }
    },
    'selection:updated': function(_event) {
        let activeObject = _event.target;
        updateLayersList();
        updateObjectsOptions();
        HTML.objectsOptionsPanel.classList.add('active');
        if (activeObject.type !== 'activeSelection' && HTML.layersList.clientHeight > HTML.layersScroll.clientHeight) {
            scrollToLayer(activeObject.id);
        }
    },
    'selection:cleared': function() {
        HTML.objectsOptionsPanel.classList.remove('active');
        updateLayersList();
        HTML.clipPanel.classList.remove('active');
    },
    'mouse:down': function() {
        if (isGrabMode) {
            isGrabbing = true;
            myCanvas.upperCanvasEl.classList.add('grabbing');
        }
    },
    'mouse:move': function(opt) {
        let mouseEvent = opt.e;
        if (isGrabbing) {
            let canvasContainer = this.wrapperEl;
            let previousTop = canvasContainer.style.top.replace('px', '');
            let previousLeft = canvasContainer.style.left.replace('px', '');
            let newTop = Number(previousTop) + mouseEvent.movementY;
            let newLeft = Number(previousLeft) + mouseEvent.movementX;
            canvasContainer.style.top = newTop + 'px';
            canvasContainer.style.left = newLeft + 'px';
        }
    },
    'mouse:up': function() {
        if (isGrabMode) {
            isGrabbing = false;
            myCanvas.upperCanvasEl.classList.remove('grabbing');
        }
    },
    'mouse:over': function(_event) {
        if (_event.target && _event.target !== myCanvas.getActiveObject() && !isEyedropperMode && !isClippingRectangleMode && !isClippingEllipseMode && !isGrabMode) {
            let object = _event.target;
            object._renderControls(myCanvas.contextContainer, {
                borderColor: '#000',
                hasControls: false,
            });
        }
    },
    'mouse:out': function(_event) {
        if (_event.target) {
            myCanvas.renderAll();
        }
    },
});

canvasFit();