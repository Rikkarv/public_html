const DRAWINGS_HTML = {
    overlay: document.getElementById('drawingsOverlay'),
    folders: document.getElementById('drawingsFolders').querySelectorAll('.drawingsFolder'),
    screen: document.getElementById('drawingsList'),
    pagination: document.getElementById('drawingsPagination'),
    previousPage: document.getElementById('drawingsPreviousPage'),
    infoPagination: document.getElementById('drawingsInfoPagination'),
    nextPage: document.getElementById('drawingsNextPage'),
};
let folder;
let pageIndex;

function selectFolder(_element) {
    folder = _element.dataset.folder;
    pageIndex = 0;
    DRAWINGS_HTML.folders.forEach(_folder => {
        _folder.classList.remove('selected');
    });
    _element.classList.add('selected');
    loadPage();
}

function loadPage() {
    DRAWINGS_HTML.screen.innerHTML = '';
    jQuery.getJSON(
        'private/drawings.php',
        {
            folder: folder,
            page: pageIndex
        },
        function(_data) {
            _data['pageFiles'].forEach(file => {
                let drawingContainer = document.createElement('div');
                let drawing = document.createElement('img');
                drawingContainer.setAttribute('class', 'assetContainer');
                drawingContainer.addEventListener('click', function() {
                    let path = this.querySelector('.asset').getAttribute('src');
                    fabric.loadSVGFromURL(path, function(objects) {
                        let svg = fabric.util.groupSVGElements(objects);
                        svg.name = 'drawing';
                        svg.scaleToWidth(canvasWidth / 2, true);
                        if (svg.getScaledHeight() > canvasHeight) {
                            svg.scaleToHeight(canvasHeight / 2, true);
                        }
                        myCanvas.add(svg).viewportCenterObject(svg).setActiveObject(svg);
                        updateLayersList();
                        takeSnapshot();
                        DRAWINGS_HTML.overlay.classList.remove('active');
                    });
                });
                drawing.setAttribute('class', 'asset');
                drawing.src = 'private/' + file;
                drawingContainer.appendChild(drawing);
                DRAWINGS_HTML.screen.appendChild(drawingContainer);
            });
            DRAWINGS_HTML.previousPage.classList.toggle('disabled', pageIndex === 0);
            DRAWINGS_HTML.nextPage.classList.toggle('disabled', pageIndex + 1 >= _data['totalPages']);
            DRAWINGS_HTML.infoPagination.innerHTML = 'Page ' + (pageIndex + 1) + ' of ' + _data['totalPages'] + ' (' + _data['totalFiles'] + ' files)';
        }
        );
}

document.getElementById('button_openDrawings').addEventListener('click', function() {
    DRAWINGS_HTML.overlay.classList.add('active');
});
document.getElementById('button_closeDrawings').addEventListener('click', function() {
    DRAWINGS_HTML.overlay.classList.remove('active');
});
DRAWINGS_HTML.previousPage.addEventListener('click', function() {
    if (this.classList.contains('disabled')) {
        return;
    }
    pageIndex--;
    loadPage(folder);
});
DRAWINGS_HTML.nextPage.addEventListener('click', function() {
    if (this.classList.contains('disabled')) {
        return;
    }
    pageIndex++;
    loadPage(folder);
});