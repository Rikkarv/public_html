<?php
$folderFiles = glob('drawings/' . $_GET['folder'] . '/*');
$itemsPerPage = 40;
$pages = array_chunk($folderFiles, $itemsPerPage);
echo json_encode([
    'pageFiles' => $pages[$_GET['page']],
    'totalFiles' => count($folderFiles),
    'totalPages' => count($pages),
    'test' => $_GET['folder']
]);