<div id="drawingsOverlay" class="overlay">
    <div id="drawingsContainer" class="flexCenter">
        <div id="drawingsFolders">
            <div class="drawingsFolder" data-folder="Animals" onclick="selectFolder(this)">Animals</div>
            <div class="drawingsFolder" data-folder="Characters" onclick="selectFolder(this)">Characters</div>
            <div class="drawingsFolder" data-folder="Fashion" onclick="selectFolder(this)">Fashion</div>
            <div class="drawingsFolder" data-folder="Food" onclick="selectFolder(this)">Food</div>
            <div class="drawingsFolder" data-folder="Frames" onclick="selectFolder(this)">Frames</div>
            <div class="drawingsFolder" data-folder="Home" onclick="selectFolder(this)">Home</div>
            <div class="drawingsFolder" data-folder="Kids" onclick="selectFolder(this)">Kids</div>
            <div class="drawingsFolder" data-folder="Landscape" onclick="selectFolder(this)">Landscape</div>
            <div class="drawingsFolder" data-folder="MagicalCreatures" onclick="selectFolder(this)">MagicalCreatures</div>
            <div class="drawingsFolder" data-folder="Marine" onclick="selectFolder(this)">Marine</div>
            <div class="drawingsFolder" data-folder="Mascots" onclick="selectFolder(this)">Mascots</div>
            <div class="drawingsFolder" data-folder="Misc" onclick="selectFolder(this)">Misc</div>
            <div class="drawingsFolder" data-folder="Nature" onclick="selectFolder(this)">Nature</div>
            <div class="drawingsFolder" data-folder="Transportation" onclick="selectFolder(this)">Transportation</div>
            <div class="drawingsFolder" data-folder="Wedding" onclick="selectFolder(this)">Wedding</div>
        </div>
        <div id="drawingsList" class="flexCenter"></div>
        <div id="drawingsPagination">
            <div id="drawingsPreviousPage" class="disabled drawingsPageButton">
                <svg enable-background="new 0 0 48 48" height="36px" version="1.1" viewBox="0 0 48 48" width="36px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <path d="M24,0C10.745,0,0,10.745,0,24c0,13.254,10.745,24,24,24s24-10.746,24-24C48,10.745,37.255,0,24,0z M23.905,44.008c-11.046,0-20-8.955-20-20c0-11.045,8.954-20,20-20s20,8.955,20,20C43.905,35.053,34.951,44.008,23.905,44.008z"/>
                    <path d="M11.962,23.125l8.904-7.339c1.969-1.622,1.985,0.929,1.985,0.929v1.525 c0.078,0.572,0.491,1.471,2.326,1.471h8.896h0.847c0,0,1.883-0.076,1.883,1.914v0.613v3.034v0.479 c-0.05,0.689-0.435,2.15-2.812,2.15h-9.459c-1.744,0-1.682-1.309-1.682-1.309v-0.076c0-1.037,1.039-1.219,1.561-1.244h7.912 c1.405,0,1.699-0.684,1.75-1.118v-0.539c0-1.135-0.846-1.346-1.32-1.377h-9.902h-0.29c-1.607,0-2.113-0.866-2.27-1.506 l-0.002,0.001c0,0-0.158-0.896-1.043-0.167l-3.426,2.822c0,0-0.009,0.009-0.012,0.01l-0.05,0.041 c-0.244,0.209-1.094,1.054,0.06,1.868l2.797,1.969l0.02,0.014l1.587,1.117v-0.033l1.32,0.902c1.319,0.904,1.309,2.396,1.309,2.396 v0.342c0,0-0.161,1.332-1.458,0.422l-9.431-6.639c0,0,0,0-0.001,0C10.241,24.586,11.958,23.128,11.962,23.125z"/>
                </svg>
            </div>
            <div id="drawingsInfoPagination"></div>
            <div id="drawingsNextPage" class="disabled drawingsPageButton">
                <svg enable-background="new 0 0 48 48" height="36px" version="1.1" viewBox="0 0 48 48" width="36px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <path d="M24.097,0c-13.255,0-24,10.745-24,24s10.745,24,24,24s24-10.745,24-24S37.352,0,24.097,0z M24.002,44.008 c-11.046,0-20-8.954-20-20c0-11.046,8.954-20,20-20s20,8.954,20,20C44.002,35.054,35.048,44.008,24.002,44.008z"/>
                    <path d="M36.135,23.125l-8.904-7.339c-1.969-1.622-1.985,0.929-1.985,0.929v1.525 c-0.078,0.572-0.491,1.47-2.326,1.47h-8.896h-0.847c0,0-1.883-0.076-1.883,1.914v0.614v3.033v0.479 c0.05,0.689,0.435,2.15,2.812,2.15h9.459c1.744,0,1.682-1.309,1.682-1.309v-0.076c0-1.037-1.039-1.218-1.561-1.244h-7.912 c-1.405,0-1.699-0.684-1.75-1.118v-0.539c0-1.135,0.846-1.346,1.32-1.376h9.902h0.29c1.607,0,2.113-0.867,2.27-1.506l0.002,0 c0,0,0.158-0.895,1.043-0.167l3.426,2.823c0,0,0.009,0.008,0.012,0.009l0.05,0.042c0.244,0.208,1.094,1.053-0.06,1.867 l-2.797,1.969l-0.02,0.014l-1.587,1.117v-0.033l-1.32,0.902c-1.319,0.904-1.309,2.396-1.309,2.396v0.342 c0,0,0.161,1.332,1.458,0.422l9.431-6.638c0,0,0-0.001,0.001-0.001C37.855,24.586,36.139,23.128,36.135,23.125z"/>
                </svg>
            </div>
        </div>
        <div id="button_closeDrawings" class="textButton flexCenter">close</div>
    </div>
</div>